import './mocks/googleauth';
import './mocks/rnDraggableFlatList';
import './mocks/rnGestureHandler';

import { shallow } from 'enzyme';
import React from 'react';

import MainNavigator from '../src/navigation/MainNavigator';

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');

describe('MainNavigator', () => {
  it('should have 8 <Screen /> for "Home", "EditTask", "CompletedTasks", "TaskDetails", "CreateTask", "SearchFriends", "FriendsDetail" and "Settings" routes', () => {
    const wrapper = shallow(<MainNavigator />);
    expect(wrapper.find('Screen')).toHaveLength(8);
    expect(
      wrapper
        .find('Screen')
        .filterWhere(screen => screen.prop('name') === 'Home')
    ).toHaveLength(1);
    expect(
      wrapper
        .find('Screen')
        .filterWhere(screen => screen.prop('name') === 'EditTask')
    ).toHaveLength(1);
    expect(
      wrapper
        .find('Screen')
        .filterWhere(screen => screen.prop('name') === 'CompletedTasks')
    ).toHaveLength(1);
    expect(
      wrapper
        .find('Screen')
        .filterWhere(screen => screen.prop('name') === 'TaskDetails')
    ).toHaveLength(1);
    expect(
      wrapper
        .find('Screen')
        .filterWhere(screen => screen.prop('name') === 'CreateTask')
    ).toHaveLength(1);
    expect(
      wrapper
        .find('Screen')
        .filterWhere(screen => screen.prop('name') === 'SearchFriends')
    ).toHaveLength(1);
    expect(
      wrapper
        .find('Screen')
        .filterWhere(screen => screen.prop('name') === 'FriendsDetail')
    ).toHaveLength(1);
    expect(
      wrapper
        .find('Screen')
        .filterWhere(screen => screen.prop('name') === 'Settings')
    ).toHaveLength(1);
  });
});
