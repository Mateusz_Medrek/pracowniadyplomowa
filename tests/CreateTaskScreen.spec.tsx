import './mocks/eventEmitter';
import './mocks/googleauth';
import './mocks/rncDateTimePicker';
import './mocks/rnDraggableFlatList';
import './mocks/rnGestureHandler';
import './mocks/rnSafeAreaContext';
import './mocks/rnScrollView';
import './mocks/rnVectorIcons';

import { useNavigation, useRoute } from '@react-navigation/native';
import { mount, shallow } from 'enzyme';
import i18n from 'i18n-js';
import moxios from 'moxios';
import React from 'react';
import { act } from 'react-test-renderer';
import wait from 'waait';

import { CREATE_TASK, EDIT_TASK } from '../src/api/routes';
import server, { baseURL } from '../src/api/server';
import CreateTaskScreen from '../src/screens/CreateTaskScreen';
import { TokenProviders, IUser, ITask } from '../src/store';
import { TranslationsKeys } from '../translations/keys';

const startDate = new Date();
startDate.setSeconds(0);
const finishDate = new Date();
finishDate.setSeconds(0);
const mockGoBack = jest.fn();
const mockNavigate = jest.fn();
const mockTask: ITask & { id: string } = {
  id: '12345678',
  title: 'ABCD',
  description: 'abcdefghijklmnop',
  startingTime: startDate,
  finishingTime: finishDate,
  subTasks: [{ title: 'A', assigneeId: '0987654', category: 1, id: 1 }],
};
jest.mock('@react-navigation/native', () => ({
  useNavigation: jest.fn(() => ({
    goBack: mockGoBack,
    navigate: mockNavigate,
  })),
  useRoute: jest.fn(() => ({
    params: {},
  })),
}));

const mockSaveUser = jest.fn();
const mockTokenType = TokenProviders.server;
const mockUserId = '01234567';
const mockState = {
  locale: 'en-GB',
  token: {
    isLoading: false,
    token: 'IAmValidToken',
    tokenType: mockTokenType,
  },
  user: {
    id: mockUserId,
    accountType: mockTokenType,
    email: 'se@gmail.com',
    name: 'XYZ',
    tasksIds: [],
    friends: [{ id: '0987654', name: 'CBA' }],
  },
};
jest.mock('react-redux', () => ({
  useDispatch: jest.fn(() => ({
    user: {
      saveUser: mockSaveUser,
    },
  })),
  useSelector: jest.fn((callback: (state: Record<string, any>) => any) =>
    callback(mockState)
  ),
}));

jest.mock('../src/hooks/useCurrentUser', () => {
  return (): [() => Promise<IUser>] => [
    () =>
      Promise.resolve<IUser>({
        id: '01234',
        name: 'XYZ',
        accountType: mockTokenType,
        email: 'se@gmail.com',
        tasksIds: [],
        friends: [],
      }),
  ];
});

describe('CreateTaskScreen', () => {
  beforeEach(() => {
    mockGoBack.mockClear();
    mockNavigate.mockClear();
    mockSaveUser.mockClear();
    (useNavigation as any).mockClear();
    (useRoute as any).mockImplementation(() => ({
      params: {},
    }));
  });
  it('should have <ReanimatedDrawerLayout /> when there is no taskData route param and should not have <ReanimatedDrawerLayout /> when there is taskData', () => {
    let wrapper = shallow(<CreateTaskScreen />);
    expect(wrapper.find('ReanimatedDrawerLayout')).toHaveLength(1);
    (useRoute as any).mockImplementation(() => ({
      params: {
        taskData: mockTask,
      },
    }));
    wrapper = shallow(<CreateTaskScreen />);
    expect(wrapper.find('ReanimatedDrawerLayout')).toHaveLength(0);
  });
  it('should have <Header />', () => {
    const wrapper = shallow(<CreateTaskScreen />);
    expect(wrapper.find('Header')).toHaveLength(1);
  });
  it('should have 2 <TextInput />', () => {
    const wrapper = shallow(<CreateTaskScreen />);
    expect(wrapper.find('TextInput')).toHaveLength(2);
  });
  it('should have <SubTasksList /> on default', () => {
    const wrapper = shallow(<CreateTaskScreen />);
    expect(wrapper.find('SubTasksList')).toHaveLength(1);
  });
  it('should have 2 <DateTimePicker />', () => {
    const wrapper = shallow(<CreateTaskScreen />);
    expect(wrapper.find('DateTimePicker')).toHaveLength(2);
  });
  it('should have "Save task" <TouchableOpacity />', () => {
    const wrapper = shallow(<CreateTaskScreen />);
    expect(
      wrapper
        .find('TouchableOpacity')
        .first()
        .contains(i18n.t(TranslationsKeys.CREATE_TASK_SCREEN__SAVE_TASK))
    ).toBe(true);
  });
  it('should display error <Text />, when title is not provided', async () => {
    const wrapper = shallow(<CreateTaskScreen />);
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CREATE_TASK_SCREEN__SAVE_TASK))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('Text')
        .filterWhere(txt =>
          txt.contains(
            i18n.t(TranslationsKeys.CREATE_TASK_SCREEN__TITLE_NOT_PROVIDED)
          )
        )
    ).toHaveLength(1);
  });
  it('should display error <Text />, when description is not provided', async () => {
    const wrapper = shallow(<CreateTaskScreen />);
    wrapper
      .find('TextInput')
      .first()
      .invoke<any>('onChangeText')('ABC');
    await wait(0);
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CREATE_TASK_SCREEN__SAVE_TASK))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('Text')
        .filterWhere(txt =>
          txt.contains(
            i18n.t(
              TranslationsKeys.CREATE_TASK_SCREEN__DESCRIPTION_NOT_PROVIDED
            )
          )
        )
    ).toHaveLength(1);
  });
  it('should call server on submit, and navigate method with "Home" when route params object is undefined, should have <ActivityIndicator /> until request is not completed', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${CREATE_TASK}`, {
      status: 200,
    });
    const wrapper = mount(<CreateTaskScreen />);
    wrapper.find('TextInput').map(textInput => {
      textInput.invoke<any>('onChangeText')('ABC');
    });
    wrapper
      .find('DateTimePicker')
      .first()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    const date = new Date();
    wrapper
      .find('DateTimePicker')
      .first()
      .find('RNCDateTimePicker')
      .last()
      .invoke<any>('onChange')({}, date);
    await wait(0);
    wrapper.update();
    //
    wrapper
      .find('SubTasksList')
      .first()
      .find('RNGHTouchableWithoutFeedback')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.SUBTASKSLIST__ADD_SUBTASK))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('SubTask')
      .first()
      .find('TextInput')
      .first()
      .invoke<any>('onChangeText')('XYZ');
    wrapper
      .find('SubTask')
      .first()
      .find('Picker')
      .first()
      .invoke<any>('onValueChange')(3, 1);
    wrapper
      .find('SubTask')
      .first()
      .find('Picker')
      .last()
      .invoke<any>('onValueChange')(mockUserId, 2);
    await wait(0);
    wrapper.update();
    wrapper
      .find('SubTask')
      .first()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    //
    wrapper
      .find('SubTasksList')
      .first()
      .find('RNGHTouchableWithoutFeedback')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.SUBTASKSLIST__ADD_SUBTASK))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('SubTask')
      .first()
      .find('TextInput')
      .first()
      .invoke<any>('onChangeText')('ABC');
    wrapper
      .find('SubTask')
      .first()
      .find('Picker')
      .first()
      .invoke<any>('onValueChange')(2, 1);
    wrapper
      .find('SubTask')
      .first()
      .find('Picker')
      .last()
      .invoke<any>('onValueChange')(mockUserId, 2);
    await wait(0);
    wrapper.update();
    wrapper
      .find('SubTask')
      .first()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    //
    expect(
      wrapper
        .find('ReanimatedModal')
        .first()
        .prop('isVisible')
    ).toBe(0);
    // Saving task must be wrapped in act() - without it, wrapper does not update showModal state;
    await act(() => {
      wrapper
        .find('TouchableOpacity')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.CREATE_TASK_SCREEN__SAVE_TASK))
        )
        .invoke<any>('onPress')();
    });
    expect(wrapper.find('ActivityIndicator')).toHaveLength(2);
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ReanimatedModal')
        .first()
        .prop('isVisible')
    ).toBe(1);
    wrapper
      .find('ReanimatedModal')
      .first()
      .find('TouchableOpacity')
      .last()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(mockNavigate).toBeCalledWith('Home');
    wrapper.unmount();
    moxios.uninstall(server as any);
  });
  it('should already have values when route params object has taskData', async () => {
    (useRoute as any).mockImplementation(() => ({
      params: {
        taskData: mockTask,
      },
    }));
    const wrapper = mount(<CreateTaskScreen />);
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('TextInput')
        .filterWhere(txtinput => txtinput.prop('value') === mockTask.title)
    ).toHaveLength(2);
    expect(
      wrapper
        .find('TextInput')
        .filterWhere(
          txtinput => txtinput.prop('value') === mockTask.description
        )
    ).toHaveLength(2);
    expect(
      wrapper.contains(
        mockTask.startingTime.toLocaleDateString(i18n.locale) +
          ' ' +
          mockTask.startingTime.toLocaleTimeString(i18n.locale)
      )
    ).toBe(true);
    mockTask.subTasks.map(subtask => {
      expect(wrapper.contains(subtask.title)).toBe(true);
    });
    wrapper.unmount();
  });
  it('should call goBack method when route params has taskData, should have <ActivityIndicator /> until request is not completed', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${EDIT_TASK}`, {
      status: 200,
    });
    (useRoute as any).mockImplementation(() => ({
      params: {
        taskData: mockTask,
      },
    }));
    const wrapper = mount(<CreateTaskScreen />);
    wrapper
      .find('DateTimePicker')
      .first()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    const date = new Date();
    wrapper
      .find('DateTimePicker')
      .first()
      .find('RNCDateTimePicker')
      .last()
      .invoke<any>('onChange')({}, date);
    expect(
      wrapper
        .find('ReanimatedModal')
        .first()
        .prop('isVisible')
    ).toBe(0);
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CREATE_TASK_SCREEN__SAVE_TASK))
      )
      .invoke<any>('onPress')();
    expect(wrapper.find('ActivityIndicator')).toHaveLength(2);
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ReanimatedModal')
        .first()
        .prop('isVisible')
    ).toBe(1);
    //
    wrapper
      .find('TouchableOpacity')
      .last()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(mockGoBack).toBeCalledTimes(1);
    wrapper.unmount();
    moxios.uninstall(server as any);
  });
});
