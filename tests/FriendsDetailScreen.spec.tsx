import './mocks/rnVectorIcons';

import { useRoute } from '@react-navigation/native';
import { mount, shallow } from 'enzyme';
import i18n from 'i18n-js';
import moxios from 'moxios';
import React from 'react';
import wait from 'waait';

import { GET_USER } from '../src/api/routes';
import server, { baseURL } from '../src/api/server';
import FriendsDetailScreen, {
  FlatListEmptyComponent,
  FlatListRenderItemComponent,
} from '../src/screens/FriendsDetailScreen';
import { TokenProviders } from '../src/store';
import { TranslationsKeys } from '../translations/keys';

const mockId = '123456';
jest.mock('@react-navigation/native', () => ({
  useRoute: jest.fn(() => ({
    params: {
      id: mockId,
    },
  })),
}));

const mockTokenType = TokenProviders.server;
const mockState = {
  token: {
    isLoading: false,
    token: '',
    tokenType: mockTokenType,
  },
};
jest.mock('react-redux', () => ({
  useSelector: jest.fn((callback: (state: Record<string, any>) => any) =>
    callback(mockState)
  ),
}));

const userResponse = {
  id: mockId,
  name: 'ABCDEFGH',
  email: 'someemail@gmail.com',
  accountType: '0',
  friends: [{ id: '98765', name: 'XYZ' }],
};

describe('FriendsDetailScreen', () => {
  beforeEach(() => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${GET_USER}/${mockId}`, {
      response: userResponse,
    });
    (useRoute as any).mockClear();
  });
  afterEach(() => {
    moxios.uninstall(server as any);
  });
  it('should have 2 <Text /> for name and email', async () => {
    const wrapper = mount(<FriendsDetailScreen />);
    await wait(0);
    wrapper.update();
    expect(wrapper.find('Text').length).toBeGreaterThanOrEqual(2);
    wrapper.unmount();
  });
  it('should have <FlatList /> for rendering friends', async () => {
    const wrapper = mount(<FriendsDetailScreen />);
    await wait(0);
    wrapper.update();
    expect(wrapper.find('FlatList')).toHaveLength(1);
    wrapper.unmount();
  });
  it('should render friend details', async () => {
    const wrapper = mount(<FriendsDetailScreen />);
    await wait(0);
    wrapper.update();
    expect(wrapper.contains(userResponse.name)).toBe(true);
    expect(wrapper.contains(userResponse.email)).toBe(true);
    expect(wrapper.contains(userResponse.friends[0].name)).toBe(true);
    wrapper.unmount();
  });
});

describe('FlatListEmptyComponent', () => {
  it('should have "No friends" <Text />', () => {
    const wrapper = shallow(<FlatListEmptyComponent />);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.FRIENDS_DETAIL_SCREEN__NO_FRIENDS)
      )
    ).toBe(true);
  });
});

describe('FlatListRenderItemComponent', () => {
  it('should have <Text /> with name prop as content', () => {
    const props = {
      item: {
        id: '1',
        name: 'XYZ',
      },
      index: 0,
      separators: {
        highlight: jest.fn(),
        unhighlight: jest.fn(),
        updateProps: jest.fn(),
      },
    };
    const wrapper = shallow(<FlatListRenderItemComponent {...props} />);
    expect(
      wrapper
        .find('Text')
        .first()
        .contains(props.item.name)
    ).toBe(true);
  });
});
