import './mocks/rnGestureHandler';

import { shallow } from 'enzyme';
import React from 'react';

import ChangeLocale from '../src/components/ChangeLocale';

const mockChangeLocale = jest.fn();
jest.mock('react-redux', () => ({
  useDispatch: jest.fn(() => ({
    locale: {
      changeLocale: mockChangeLocale,
    },
  })),
}));

describe('ChangeLocale', () => {
  it('should have 2 <TouchableOpacity /> each with 1 <Image />, they should call onChangeToXLocale after press', () => {
    const wrapper = shallow(<ChangeLocale />);
    expect(
      wrapper
        .find('TouchableOpacity')
        .filterWhere(btn => btn.find('Image').length === 1)
    ).toHaveLength(2);
  });
  it('should call onChangeXLocale props after clicking on <TouchableOpacity />', () => {
    const onChangeToGBLocale = jest.fn();
    const onChangeToPLLocale = jest.fn();
    const wrapper = shallow(
      <ChangeLocale
        onChangeToGBLocale={onChangeToGBLocale}
        onChangeToPLLocale={onChangeToPLLocale}
      />
    );
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn => btn.find('Image').length === 1)
      .map(btn => {
        btn.invoke<any>('onPress')();
      });
    expect(onChangeToGBLocale).toBeCalledTimes(1);
    expect(onChangeToPLLocale).toBeCalledTimes(1);
  });
});
