import './mocks/rnGestureHandler';
import './mocks/rnVectorIcons';

import { useRoute } from '@react-navigation/native';
import { mount } from 'enzyme';
import i18n from 'i18n-js';
import moxios from 'moxios';
import React from 'react';
import wait from 'waait';

import { CHECK_SUBTASK, GET_TASK, UNCHECK_SUBTASK } from '../src/api/routes';
import server, { baseURL } from '../src/api/server';
import TaskDetailsScreen from '../src/screens/TaskDetailsScreen';
import { IUser, TokenProviders, ISubTask } from '../src/store';
import { TranslationsKeys } from '../translations/keys';

const mockId = '1234';
jest.mock('@react-navigation/native', () => ({
  useRoute: jest.fn(() => ({
    params: {
      id: mockId,
    },
  })),
}));

const mockUserId = '01234567';
const mockFriendId = '0987654';
const mockTokenType = TokenProviders.server;
const mockUser: IUser = {
  id: mockUserId,
  accountType: mockTokenType,
  email: 'se@gmail.com',
  name: 'XYZ',
  tasksIds: [],
  friends: [{ id: mockFriendId, name: 'CBA' }],
};
const mockState = {
  token: {
    isLoading: false,
    token: 'IAmToken',
    tokenType: mockTokenType,
  },
  user: mockUser,
};
jest.mock('react-redux', () => ({
  useSelector: jest.fn((callback: (state: Record<string, any>) => any) =>
    callback(mockState)
  ),
}));

const response = {
  _id: mockId,
  title: 'ABC',
  description: 'XYZ',
  startingTime: new Date(),
  finishingTime: new Date(),
  subTasks: [
    { assigneeId: mockUserId, category: 1, id: 1, title: 'DEF' },
    { assigneeId: mockFriendId, category: 2, id: 2, title: 'PLPLPL' },
  ] as ISubTask[],
};

describe('TaskDetailsScreen', () => {
  beforeEach(() => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${GET_TASK}/${mockId}`, { response });
    moxios.stubRequest(`${baseURL}/${CHECK_SUBTASK}`, { status: 200 });
    moxios.stubRequest(`${baseURL}/${UNCHECK_SUBTASK}`, { status: 200 });
    (useRoute as any).mockClear();
  });
  afterEach(() => {
    moxios.uninstall(server as any);
  });
  it('should have 4 <Text /> for title, description, startingTime and finishingTime', async () => {
    const wrapper = mount(<TaskDetailsScreen />);
    await wait(0);
    wrapper.update();
    expect(wrapper.find('Text').length).toBeGreaterThanOrEqual(4);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.TASK_DETAILS_SCREEN__TITLE_HEADER)
      )
    ).toBe(true);
    expect(wrapper.contains(response.title)).toBe(true);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.TASK_DETAILS_SCREEN__DESCRIPTION_HEADER)
      )
    ).toBe(true);
    expect(wrapper.contains(response.description)).toBe(true);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.TASK_DETAILS_SCREEN__STARTING_DATETIME_HEADER)
      )
    ).toBe(true);
    expect(
      wrapper.contains(
        response.startingTime.toLocaleDateString(i18n.locale) +
          ' ' +
          response.startingTime.toLocaleTimeString(i18n.locale)
      )
    ).toBe(true);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.TASK_DETAILS_SCREEN__FINISHING_DATETIME_HEADER)
      )
    ).toBe(true);
    expect(
      wrapper.contains(
        response.finishingTime.toLocaleDateString(i18n.locale) +
          ' ' +
          response.finishingTime.toLocaleTimeString(i18n.locale)
      )
    ).toBe(true);
    wrapper.unmount();
  });
  it('should render subTasks items', async () => {
    const wrapper = mount(<TaskDetailsScreen />);
    await wait(0);
    wrapper.update();
    expect(wrapper.find('FlatList')).toHaveLength(1);
    expect(
      wrapper
        .find('FlatList')
        .first()
        .find('CellRenderer')
    ).toHaveLength(2);
    const firstSubTask = wrapper
      .find('FlatList')
      .first()
      .find('CellRenderer')
      .first();
    const secondSubTask = wrapper
      .find('FlatList')
      .first()
      .find('CellRenderer')
      .last();
    expect(firstSubTask.contains(response.subTasks[0].title)).toBe(true);
    expect(
      firstSubTask.contains(
        i18n.t(TranslationsKeys.TASK_DETAILS_SCREEN__EXECUTOR) +
          i18n.t(TranslationsKeys.TASK_DETAILS_SCREEN__ME)
      )
    ).toBe(true);
    expect(secondSubTask.contains(response.subTasks[1].title)).toBe(true);
    expect(
      secondSubTask.contains(
        i18n.t(TranslationsKeys.TASK_DETAILS_SCREEN__EXECUTOR) +
          mockUser.friends[0].name
      )
    ).toBe(true);
    wrapper.unmount();
  });
  it('should change <Icon /> to "restore" and should show additional "check-circle" <Icon /> when user completes subtask and again back to "checkbox-blank-circle-outline" when user uncheck subtask', async () => {
    const wrapper = mount(<TaskDetailsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('FlatList')
      .first()
      .find('CellRenderer')
      .first()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('FlatList')
        .first()
        .find('CellRenderer')
        .first()
        .find('Icon')
        .filterWhere(icn => icn.prop('name') === 'check-circle')
    ).toHaveLength(1);
    expect(
      wrapper
        .find('FlatList')
        .first()
        .find('CellRenderer')
        .first()
        .find('TouchableOpacity')
        .filterWhere(
          btn =>
            btn
              .find('Icon')
              .first()
              .prop('name') === 'restore'
        )
    ).toHaveLength(1);
    wrapper
      .find('FlatList')
      .first()
      .find('CellRenderer')
      .first()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('FlatList')
        .first()
        .find('CellRenderer')
        .first()
        .find('TouchableOpacity')
        .filterWhere(
          btn =>
            btn
              .find('Icon')
              .first()
              .prop('name') === 'checkbox-blank-circle-outline'
        )
    ).toHaveLength(1);
    wrapper.unmount();
  });
});
