import './mocks/rnDraggableFlatList';
import './mocks/rnGestureHandler';
import './mocks/rnVectorIcons';

import { shallow } from 'enzyme';
import i18n from 'i18n-js';
import React from 'react';
import wait from 'waait';

import SubTasksList from '../src/components/SubTasksList';
import { IUser, TokenProviders } from '../src/store';
import { TranslationsKeys } from '../translations/keys';

const user: IUser = {
  id: '01234',
  name: 'XYZ',
  email: 'se@gmail.com',
  accountType: TokenProviders.server,
  tasksIds: [],
  friends: [],
};

const userWithFriends: IUser = {
  ...user,
};
const friend = {
  id: '13',
  name: 'ABC',
};
const subTasks = {
  assigneeId: '13',
  category: 1,
  id: 987,
  title: 'FFF',
};
userWithFriends.friends.push(friend);

describe('SubTasksList', () => {
  it('should have <DraggableFlatList />', () => {
    const wrapper = shallow(
      <SubTasksList dispatch={jest.fn()} subTasks={[]} user={user} />
    );
    expect(wrapper.find('DraggableFlatList')).toHaveLength(1);
  });
  it('should have "Add subtask" btn and should not have <SubTask /> by default', () => {
    const wrapper = shallow(
      <SubTasksList dispatch={jest.fn()} subTasks={[]} user={user} />
    );
    expect(
      wrapper
        .find('RNGHTouchableWithoutFeedback')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.SUBTASKSLIST__ADD_SUBTASK))
        )
    ).toHaveLength(1);
    expect(wrapper.find('SubTask')).toHaveLength(0);
  });
  it('should have <SubTask /> and "Cancel" btn and should not have "Add subtask" btn after clicking it', async () => {
    const wrapper = shallow(
      <SubTasksList dispatch={jest.fn()} subTasks={[]} user={user} />
    );
    wrapper
      .find('RNGHTouchableWithoutFeedback')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.SUBTASKSLIST__ADD_SUBTASK))
      )
      .invoke<any>('onPress')();
    await wait(0);
    expect(
      wrapper
        .find('RNGHTouchableWithoutFeedback')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.SUBTASKSLIST__ADD_SUBTASK))
        )
    ).toHaveLength(0);
    expect(
      wrapper
        .find('RNGHTouchableWithoutFeedback')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.SUBTASKSLIST__CANCEL_SUBTASK))
        )
    ).toHaveLength(1);
    expect(wrapper.find('SubTask')).toHaveLength(1);
  });
  it('should have <SubTask /> after clicking on <FlatList /> item', async () => {
    const wrapper = shallow(
      <SubTasksList
        dispatch={jest.fn()}
        subTasks={[subTasks]}
        user={userWithFriends}
      />
    );
    wrapper
      .find('DraggableFlatList')
      .dive()
      .dive()
      .dive()
      .dive()
      .find('CellRenderer')
      .first()
      .dive()
      .find('RNGHTouchableWithoutFeedback')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(
      wrapper
        .find('RNGHTouchableWithoutFeedback')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.SUBTASKSLIST__ADD_SUBTASK))
        )
    ).toHaveLength(0);
    expect(
      wrapper
        .find('RNGHTouchableWithoutFeedback')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.SUBTASKSLIST__CANCEL_SUBTASK))
        )
    ).toHaveLength(1);
    expect(wrapper.find('SubTask')).toHaveLength(1);
  });
  it('should call dispatch after editing existing subtask', async () => {
    const dispatch = jest.fn();
    const wrapper = shallow(
      <SubTasksList
        dispatch={dispatch}
        subTasks={[subTasks]}
        user={userWithFriends}
      />
    );
    wrapper
      .find('DraggableFlatList')
      .dive()
      .dive()
      .dive()
      .dive()
      .find('CellRenderer')
      .first()
      .dive()
      .find('RNGHTouchableWithoutFeedback')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(
      wrapper
        .find('RNGHTouchableWithoutFeedback')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.SUBTASKSLIST__ADD_SUBTASK))
        )
    ).toHaveLength(0);
    expect(
      wrapper
        .find('RNGHTouchableWithoutFeedback')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.SUBTASKSLIST__CANCEL_SUBTASK))
        )
    ).toHaveLength(1);
    expect(wrapper.find('SubTask')).toHaveLength(1);
    wrapper
      .find('SubTask')
      .first()
      .dive()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(dispatch).toBeCalledTimes(1);
  });
});
