import './mocks/rnGestureHandler';

import { shallow } from 'enzyme';
import React from 'react';
import wait from 'waait';

import ReanimatedModal from '../src/components/ReanimatedModal';

describe('ReanimatedModal', () => {
  it('should contain 2 <Text />', () => {
    const modalButtonText = 'ABC';
    const modalText = 'XYZ';
    const wrapper = shallow(
      <ReanimatedModal
        isVisible={0}
        modalButtonText={modalButtonText}
        modalText={modalText}
        onModalClose={jest.fn()}
      />
    );
    expect(
      wrapper
        .find('Text')
        .first()
        .contains(modalText)
    ).toBe(true);
    expect(
      wrapper
        .find('Text')
        .last()
        .contains(modalButtonText)
    ).toBe(true);
  });
  it('should call onModalClose prop after closing modal', async () => {
    const onModalClose = jest.fn();
    const wrapper = shallow(
      <ReanimatedModal
        isVisible={1}
        modalButtonText=""
        modalText=""
        onModalClose={onModalClose}
      />
    );
    wrapper
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(onModalClose).toBeCalledTimes(1);
  });
});
