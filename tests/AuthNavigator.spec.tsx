import './mocks/googleauth';
import './mocks/rnGestureHandler';

import { shallow } from 'enzyme';
import React from 'react';

import AuthNavigator from '../src/navigation/AuthNavigator';

describe('AuthNavigator', () => {
  it('should have <NativeStackNavigator />', () => {
    const wrapper = shallow(<AuthNavigator />);
    expect(wrapper.find('NativeStackNavigator')).toHaveLength(1);
  });
  it('should have 2 <Screen /> for "Auth" and "Registration" routes', () => {
    const wrapper = shallow(<AuthNavigator />);
    expect(wrapper.find('Screen')).toHaveLength(2);
    expect(
      wrapper
        .find('Screen')
        .filterWhere(screen => screen.prop('name') === 'Auth')
    ).toHaveLength(1);
    expect(
      wrapper
        .find('Screen')
        .filterWhere(screen => screen.prop('name') === 'Registration')
    ).toHaveLength(1);
  });
});
