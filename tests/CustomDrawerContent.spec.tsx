import './mocks/rnGestureHandler';
import './mocks/rnSafeAreaContext';
import './mocks/rnScrollView';
import './mocks/rnVectorIcons';

import { useNavigation } from '@react-navigation/native';
import { shallow } from 'enzyme';
import i18n from 'i18n-js';
import React from 'react';
import { useSelector } from 'react-redux';
import wait from 'waait';

import CustomDrawerContent from '../src/components/CustomDrawerContent';
import { TokenProviders } from '../src/store';
import { TranslationsKeys } from '../translations/keys';

const mockNavigate = jest.fn();
jest.mock('@react-navigation/native', () => ({
  useFocusEffect: jest.fn(),
  useNavigation: jest.fn(() => ({
    navigate: mockNavigate,
  })),
}));

const mockUser = {
  id: '01234',
  email: 'se@gmail.com',
  name: 'ABC',
  accountType: TokenProviders.server,
  tasksIds: [],
  friends: [],
};
const mockState = {
  user: mockUser,
};
jest.mock('react-redux', () => ({
  useSelector: jest.fn((callback: (state: Record<string, any>) => any) =>
    callback(mockState)
  ),
}));

describe('CustomDrawerContent', () => {
  beforeEach(() => {
    mockNavigate.mockClear();
    (useNavigation as any).mockClear();
  });
  it('should contain <ScrollView />', () => {
    const wrapper = shallow(
      <CustomDrawerContent closeDrawer={jest.fn()} drawerPosition="left" />
    );
    expect(wrapper.find('ScrollView')).toHaveLength(1);
  });
  it('should call closeDrawer method after pressing "X" button', async () => {
    const closeDrawer = jest.fn();
    const wrapper = shallow(
      <CustomDrawerContent closeDrawer={closeDrawer} drawerPosition="left" />
    );
    wrapper
      .find('ScrollView')
      .first()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(closeDrawer).toBeCalledTimes(1);
  });
  it('should dispatch closeDrawer action and navigate to "Settings" after pressing <TouchableOpacity /> with "Ustawienia" <Text />', async () => {
    const closeDrawer = jest.fn();
    const wrapper = shallow(
      <CustomDrawerContent closeDrawer={closeDrawer} drawerPosition="left" />
    );
    wrapper
      .find('ScrollView')
      .first()
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__SETTINGS))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(closeDrawer).toBeCalledTimes(1);
    expect(mockNavigate).toBeCalledWith('Settings');
  });
  it('should have <TouchableOpacity /> with "Ustawienia" <Text /> only when account type is TokenProviders.server', () => {
    const closeDrawer = jest.fn();
    let wrapper = shallow(
      <CustomDrawerContent closeDrawer={closeDrawer} drawerPosition="left" />
    );
    expect(
      wrapper
        .find('ScrollView')
        .first()
        .find('TouchableOpacity')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__SETTINGS))
        )
    ).toHaveLength(1);
    // Mock for Google;
    (useSelector as any).mockImplementationOnce(
      (callback: (state: Record<string, any>) => any) =>
        callback({
          user: {
            id: '21234',
            email: 'se@gmail.com',
            name: 'ABC',
            accountType: TokenProviders.google,
            tasksIds: [],
            friends: [],
          },
        })
    );
    closeDrawer.mockClear();
    wrapper = shallow(
      <CustomDrawerContent closeDrawer={closeDrawer} drawerPosition="left" />
    );
    expect(
      wrapper
        .find('ScrollView')
        .first()
        .find('TouchableOpacity')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__SETTINGS))
        )
    ).toHaveLength(0);
  });
  it('should have <ChangeLocale />', () => {
    const wrapper = shallow(
      <CustomDrawerContent closeDrawer={jest.fn()} drawerPosition="left" />
    );
    expect(wrapper.find('ChangeLocale')).toHaveLength(1);
  });
  it('should have 3 <TouchableOpacity /> for "Home", "CreateTask", "SearchFriends" routes', () => {
    const closeDrawer = jest.fn();
    const wrapper = shallow(
      <CustomDrawerContent closeDrawer={closeDrawer} drawerPosition="left" />
    );
    expect(
      wrapper
        .find('ScrollView')
        .first()
        .find('TouchableOpacity')
        .filterWhere(btn =>
          btn.contains(
            i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__HOME_PAGE)
          )
        )
    ).toHaveLength(1);
    expect(
      wrapper
        .find('ScrollView')
        .first()
        .find('TouchableOpacity')
        .filterWhere(btn =>
          btn.contains(
            i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__CREATE_TASK)
          )
        )
    ).toHaveLength(1);
    expect(
      wrapper
        .find('ScrollView')
        .first()
        .find('TouchableOpacity')
        .filterWhere(btn =>
          btn.contains(
            i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__SEARCH_FRIENDS)
          )
        )
    ).toHaveLength(1);
  });
  it('should navigate to "Home"', async () => {
    const closeDrawer = jest.fn();
    const wrapper = shallow(
      <CustomDrawerContent closeDrawer={closeDrawer} drawerPosition="left" />
    );
    wrapper
      .find('ScrollView')
      .first()
      .find('TouchableOpacity')
      .filterWhere(
        btn =>
          btn.contains(
            i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__HOME_PAGE)
          ) &&
          btn.find('Icon').filterWhere(icn => icn.prop('name') === 'home')
            .length === 1
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(closeDrawer).toBeCalledTimes(1);
    expect(mockNavigate).toBeCalledWith('Home');
  });
  it('should navigate to "CreateTask"', async () => {
    const closeDrawer = jest.fn();
    const wrapper = shallow(
      <CustomDrawerContent closeDrawer={closeDrawer} drawerPosition="left" />
    );
    wrapper
      .find('ScrollView')
      .first()
      .find('TouchableOpacity')
      .filterWhere(
        btn =>
          btn.contains(
            i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__CREATE_TASK)
          ) &&
          btn
            .find('Icon')
            .filterWhere(icn => icn.prop('name') === 'plus-circle-outline')
            .length === 1
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(closeDrawer).toBeCalledTimes(1);
    expect(mockNavigate).toBeCalledWith('CreateTask');
  });
  it('should navigate to "SearchFriends"', async () => {
    const closeDrawer = jest.fn();
    const wrapper = shallow(
      <CustomDrawerContent closeDrawer={closeDrawer} drawerPosition="left" />
    );
    wrapper
      .find('ScrollView')
      .first()
      .find('TouchableOpacity')
      .filterWhere(
        btn =>
          btn.contains(
            i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__SEARCH_FRIENDS)
          ) &&
          btn.find('Icon').filterWhere(icn => icn.prop('name') === 'magnify')
            .length === 1
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(closeDrawer).toBeCalledTimes(1);
    expect(mockNavigate).toBeCalledWith('SearchFriends');
  });
});
