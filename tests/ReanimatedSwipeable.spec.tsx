import './mocks/rnGestureHandler';

import { useIsFocused } from '@react-navigation/native';
import { mount } from 'enzyme';
import React from 'react';
import { Text } from 'react-native';

import ReanimatedSwipeable from '../src/components/ReanimatedSwipeable';

jest.mock('@react-navigation/native', () => ({
  useIsFocused: jest.fn(() => true),
}));

const RightActions = () => {
  return (
    <>
      <Text>ABC</Text>
    </>
  );
};

describe('ReanimatedSwipeable', () => {
  beforeEach(() => {
    (useIsFocused as any).mockClear();
  });
  it('should render Swipeable', () => {
    const wrapper = mount(
      <ReanimatedSwipeable renderRightActions={RightActions} />
    );
    expect(wrapper.find('Swipeable')).toHaveLength(1);
  });
  it('should render passed children', () => {
    (useIsFocused as any).mockImplementationOnce(() => false);
    const Children = () => (
      <>
        <Text>Children</Text>
      </>
    );
    const wrapper = mount(
      <ReanimatedSwipeable renderRightActions={RightActions}>
        <Children />
      </ReanimatedSwipeable>
    );
    expect(wrapper.find('Children')).toHaveLength(1);
  });
});
