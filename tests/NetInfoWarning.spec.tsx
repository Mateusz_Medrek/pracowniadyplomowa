import { shallow } from 'enzyme';
import i18n from 'i18n-js';
import React from 'react';

import NetInfoWarning from '../src/components/NetInfoWarning';
import { TranslationsKeys } from '../translations/keys';

describe('NetInfoWarning', () => {
  it('should have "No internet connection" <Text />', () => {
    const wrapper = shallow(<NetInfoWarning />);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.NET_INFO_WARNING__NO_INTERNET_CONNECTION)
      )
    ).toBe(true);
  });
});
