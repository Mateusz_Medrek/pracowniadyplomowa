import './mocks/googleauth';
import './mocks/rnGestureHandler';
import './mocks/rnVectorIcons';

import { shallow } from 'enzyme';
import React from 'react';
import { useSelector } from 'react-redux';
import wait from 'waait';

import Header from '../src/components/Header';
import { TokenProviders } from '../src/store';

const mockRemoveToken = jest.fn();
const mockTokenType = TokenProviders.server;
const mockState = {
  token: {
    isLoading: false,
    token: 'IAmToken',
    tokenType: mockTokenType,
  },
  user: {
    id: '01234567',
    accountType: mockTokenType,
    email: 'se@gmail.com',
    name: 'XYZ',
    tasksIds: [],
    friends: [{ id: '0987654', name: 'CBA' }],
  },
};
jest.mock('react-redux', () => ({
  useDispatch: jest.fn(() => ({
    token: {
      removeToken: mockRemoveToken,
    },
  })),
  useSelector: jest.fn((callback: (state: Record<string, any>) => any) =>
    callback(mockState)
  ),
}));

describe('Header', () => {
  beforeEach(() => {
    mockRemoveToken.mockClear();
  });
  it('should have <TouchableOpacity /> for opening drawer and for logout button', () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.find('TouchableOpacity')).toHaveLength(2);
  });
  it('should have <Text /> with title prop as content', () => {
    const title = 'SomeTitle';
    const wrapper = shallow(<Header title={title} />);
    expect(
      wrapper.find('Text').filterWhere(txt => txt.contains(title))
    ).toHaveLength(1);
  });
  it('should have 2 <TouchableOpacity /> where each has an <Icon />', () => {
    const title = 'SomeTitle';
    const wrapper = shallow(<Header title={title} />);
    expect(
      wrapper
        .find('TouchableOpacity')
        .filterWhere(btn => btn.find('Icon').length === 1)
    ).toHaveLength(2);
  });
  it('should call onDrawerButtonPress prop after clicking on openDrawerButton', async () => {
    const onDrawerButtonPress = jest.fn();
    const wrapper = shallow(
      <Header onDrawerButtonPress={onDrawerButtonPress} />
    );
    wrapper
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(onDrawerButtonPress).toBeCalledTimes(1);
  });
  it('should call removeToken method after pressing logout button and enter server flow when user has server account type', async () => {
    const wrapper = shallow(<Header />);
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn => btn.find('Icon').length === 1)
      .last()
      .invoke<any>('onPress')();
    await wait(0);
    expect(mockRemoveToken).toBeCalledTimes(1);
  });
  it('should call removeToken method after pressing logout button and enter google flow when user has google account type', async () => {
    (useSelector as any).mockImplementation(
      (callback: (state: Record<string, any>) => any) =>
        callback({
          token: {
            isLoading: false,
            token: 'IAmGoogleToken',
            tokenType: TokenProviders.google,
          },
          user: {
            id: '21234567',
            accountType: TokenProviders.google,
            email: 'se@gmail.com',
            name: 'XYZ',
            tasksIds: [],
            friends: [{ id: '0987654', name: 'CBA' }],
          },
        })
    );
    const wrapper = shallow(<Header />);
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn => btn.find('Icon').length === 1)
      .last()
      .invoke<any>('onPress')();
    await wait(0);
    expect(mockRemoveToken).toBeCalledTimes(1);
    (useSelector as any).mockRestore();
    (useSelector as any).mockImplementation(
      (callback: (state: Record<string, any>) => any) => callback(mockState)
    );
  });
});
