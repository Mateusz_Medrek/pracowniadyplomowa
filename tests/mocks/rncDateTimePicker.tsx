import React, { Fragment } from 'react';
import {
  AndroidNativeProps,
  IOSNativeProps,
} from '@react-native-community/datetimepicker';

const mockDateTimePicker: React.FC<
  AndroidNativeProps | IOSNativeProps
> = () => {
  return <Fragment />;
};
mockDateTimePicker.displayName = 'RNCDateTimePicker';

jest.mock('@react-native-community/datetimepicker', () => mockDateTimePicker);
