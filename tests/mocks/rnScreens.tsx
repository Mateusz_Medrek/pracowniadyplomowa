import React from 'react';

const mockScreen: React.FC<{}> = ({ children }) => <>{children}</>;
mockScreen.displayName = 'Screen';
const mockScreenStack: React.FC<{}> = ({ children }) => <>{children}</>;
mockScreenStack.displayName = 'ScreenStack';
const mockScreenStackHeaderConfig: React.FC<{}> = ({ children }) => (
  <>{children}</>
);
mockScreenStackHeaderConfig.displayName = 'ScreenStackHeaderConfig';
const mockScreenStackHeaderRightView: React.FC<{}> = ({ children }) => (
  <>{children}</>
);
mockScreenStackHeaderRightView.displayName = 'ScreenStackHeaderRightView';
const mockScreenStackHeaderLeftView: React.FC<{}> = ({ children }) => (
  <>{children}</>
);
mockScreenStackHeaderLeftView.displayName = 'ScreenStackHeaderLeftView';
const mockScreenStackHeaderTitleView: React.FC<{}> = ({ children }) => (
  <>{children}</>
);
mockScreenStackHeaderTitleView.displayName = 'ScreenStackHeaderTitleView';
const mockScreenStackHeaderCenterView: React.FC<{}> = ({ children }) => (
  <>{children}</>
);
mockScreenStackHeaderCenterView.displayName = 'ScreenStackHeaderCenterView';
const mockScreenStackHeaderSubview: React.FC<{}> = ({ children }) => (
  <>{children}</>
);
mockScreenStackHeaderSubview.displayName = 'ScreenStackHeaderSubview';

jest.mock('react-native-screens', () => ({
  Screen: mockScreen,
  ScreenStack: mockScreenStack,
  ScreenStackHeaderConfig: mockScreenStackHeaderConfig,
  ScreenStackHeaderRightView: mockScreenStackHeaderRightView,
  ScreenStackHeaderLeftView: mockScreenStackHeaderLeftView,
  ScreenStackHeaderTitleView: mockScreenStackHeaderTitleView,
  ScreenStackHeaderCenterView: mockScreenStackHeaderCenterView,
  ScreenStackHeaderSubview: mockScreenStackHeaderSubview,
  enableScreens: jest.fn(),
  screensEnabled: jest.fn(() => true),
}));
