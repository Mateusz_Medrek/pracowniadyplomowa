// eslint-disable-next-line
// @ts-ignore
const EventEmitter = require('react-native/Libraries/vendor/emitter/EventEmitter');
const RCTDeviceEventEmitter = require('react-native/Libraries/EventEmitter/RCTDeviceEventEmitter');

/**
 * Mock the NativeEventEmitter as a normal JS EventEmitter.
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
class NativeEventEmitter extends EventEmitter {
  constructor() {
    super(RCTDeviceEventEmitter.sharedSubscriber);
  }
}
