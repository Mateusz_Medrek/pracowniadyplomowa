jest.mock('react-native-safe-area-context', () => ({
  useSafeArea: jest.fn(() => ({
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  })),
}));
