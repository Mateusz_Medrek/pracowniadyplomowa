import { AsyncStorageStatic } from '@react-native-community/async-storage';

type KeysType = string[];
type KeyValueType = string[][];
type ResultType = [string, string | null];
type CallbackType = ((error?: Error) => void) | undefined;
type MultiCallbackType = ((errors?: Error[]) => void) | undefined;
type ItemGetCallbackType =
  | ((error?: Error, result?: string) => void)
  | undefined;
type MultiResultCallbackType =
  | ((errors?: Error[], result?: ResultType[]) => void)
  | undefined;

interface AsyncStorageMock extends AsyncStorageStatic {
  __INTERNAL_MOCK_STORAGE__: Record<string, any>;
}

const asyncstorageMock: AsyncStorageMock = {
  __INTERNAL_MOCK_STORAGE__: {},
  setItem: jest.fn<Promise<void>, [string, string, CallbackType]>(
    async (key: string, value: string, callback?: CallbackType) => {
      try {
        await asyncstorageMock.multiSet([[key, value]], undefined);
        callback?.();
        return Promise.resolve();
      } catch (err) {
        callback?.(err);
        return Promise.reject(err);
      }
    }
  ),
  getItem: jest.fn<Promise<string | null>, [string, ItemGetCallbackType]>(
    async (key: string, callback?: ItemGetCallbackType) => {
      const getResult = await asyncstorageMock.multiGet([key], undefined);
      const result = getResult[0] ? getResult[0][1] : null;
      callback?.(undefined, result === null ? undefined : result);
      return result;
    }
  ),
  removeItem: jest.fn<Promise<void>, [string, CallbackType]>(
    async (key: string, callback?: CallbackType) => {
      const multiCallback = (errors?: Error[]) => {
        callback?.(errors?.[0]);
      };
      return asyncstorageMock.multiRemove([key], multiCallback);
    }
  ),
  mergeItem: jest.fn<Promise<void>, [string, string, CallbackType]>(
    async (key: string, value: string, callback?: CallbackType) => {
      const multiCallback = (errors?: Error[]) => {
        callback?.(errors?.[0]);
      };
      return asyncstorageMock.multiMerge([[key, value]], multiCallback);
    }
  ),

  clear: jest.fn<Promise<void>, [CallbackType]>(_clear),
  getAllKeys: jest.fn<Promise<string[]>, []>(_getAllKeys),

  multiGet: jest.fn<Promise<ResultType[]>, [KeysType, MultiResultCallbackType]>(
    _multiGet
  ),
  multiSet: jest.fn<Promise<void>, [KeyValueType, MultiCallbackType]>(
    _multiSet
  ),
  multiRemove: jest.fn<Promise<void>, [KeysType, MultiCallbackType]>(
    _multiRemove
  ),
  multiMerge: jest.fn<Promise<void>, [KeyValueType, MultiCallbackType]>(
    _multiMerge
  ),
};

async function _multiSet(
  keyValuePairs: KeyValueType,
  callback?: MultiResultCallbackType
) {
  keyValuePairs.forEach(keyValue => {
    const key = keyValue[0];
    const value = keyValue[1];

    asyncstorageMock.__INTERNAL_MOCK_STORAGE__[key] = value;
  });
  callback?.();
}

async function _multiGet(keys: KeysType, callback?: MultiResultCallbackType) {
  const values = keys.map<ResultType>(key => [
    key,
    asyncstorageMock.__INTERNAL_MOCK_STORAGE__[key] || null,
  ]);
  callback?.(undefined, values);
  return values;
}

async function _multiRemove(keys: KeysType, callback?: MultiCallbackType) {
  keys.forEach(key => {
    if (asyncstorageMock.__INTERNAL_MOCK_STORAGE__[key]) {
      delete asyncstorageMock.__INTERNAL_MOCK_STORAGE__[key];
    }
  });
  callback?.();
}

async function _clear(callback?: CallbackType) {
  asyncstorageMock.__INTERNAL_MOCK_STORAGE__ = {};
  callback?.();
}

async function _getAllKeys() {
  return Object.keys(asyncstorageMock.__INTERNAL_MOCK_STORAGE__);
}

async function _multiMerge(
  keyValuePairs: KeyValueType,
  callback?: MultiCallbackType
) {
  keyValuePairs.forEach(keyValue => {
    const key = keyValue[0];
    const value = JSON.parse(keyValue[1]);
    const oldValue = JSON.parse(
      asyncstorageMock.__INTERNAL_MOCK_STORAGE__[key]
    );
    const processedValue = JSON.stringify(_deepMergeInto(oldValue, value));
    asyncstorageMock.__INTERNAL_MOCK_STORAGE__[key] = processedValue;
  });
  callback?.();
}

const _isObject = (obj: any) => typeof obj === 'object' && !Array.isArray(obj);
const _deepMergeInto = (oldObject: any, newObject: any) => {
  const newKeys = Object.keys(newObject);
  const mergedObject = oldObject;
  newKeys.forEach(key => {
    const oldValue = mergedObject[key];
    const newValue = newObject[key];
    if (_isObject(oldValue) && _isObject(newValue)) {
      mergedObject[key] = _deepMergeInto(oldValue, newValue);
    } else {
      mergedObject[key] = newValue;
    }
  });
  return mergedObject;
};

export default asyncstorageMock;
