// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
jest.mock('react-native/Libraries/Components/ScrollView/ScrollView', () => {
  const RealScrollView = jest.requireActual(
    'react-native/Libraries/Components/ScrollView/ScrollView'
  );
  RealScrollView.displayName = 'ScrollView';
  return RealScrollView;
});
