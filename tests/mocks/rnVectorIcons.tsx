import React from 'react';
import {
  TabBarIOSItem,
  ToolbarAndroid,
  TouchableHighlight,
  View,
} from 'react-native';
import {
  IconButtonProps,
  IconProps,
  TabBarItemIOSProps,
  ToolbarAndroidProps,
} from 'react-native-vector-icons/Icon';

const MockButton: React.ComponentType<IconButtonProps> = props => {
  return (
    <TouchableHighlight {...props}>
      <View />
    </TouchableHighlight>
  );
};
MockButton.displayName = 'IconButton';
const MockTabBarItem: React.ComponentType<TabBarItemIOSProps> = props => {
  return <TabBarIOSItem {...props} />;
};
MockTabBarItem.displayName = 'IconTabBarItem';
const MockTabBarItemIOS: React.ComponentType<TabBarItemIOSProps> = props => {
  return <TabBarIOSItem {...props} />;
};
MockTabBarItemIOS.displayName = 'IconTabBarItemIOS';
const MockToolBarAndroid: React.ComponentType<ToolbarAndroidProps> = props => {
  return <ToolbarAndroid {...props} />;
};
MockToolBarAndroid.displayName = 'IconToolBarAndroid';
const MockIcon: React.ComponentType<IconProps> & {
  Button?: React.ComponentType<IconButtonProps>;
  TabBarItem?: React.ComponentType<TabBarItemIOSProps>;
  TabBarItemIOS?: React.ComponentType<TabBarItemIOSProps>;
  ToolBarAndroid?: React.ComponentType<ToolbarAndroidProps>;
} = class extends React.Component<IconProps> {
  render() {
    return <View>{this.props.children}</View>;
  }
};
MockIcon.Button = MockButton;
MockIcon.TabBarItem = MockTabBarItem;
MockIcon.TabBarItemIOS = MockTabBarItemIOS;
MockIcon.ToolBarAndroid = MockToolBarAndroid;
MockIcon.displayName = 'Icon';
jest.mock('react-native-vector-icons/MaterialCommunityIcons', () => MockIcon);
