import React from 'react';
import { FlatList, FlatListProps } from 'react-native';

function mockDraggableFlatList<T>(props: FlatListProps<T>) {
  return <FlatList {...props} />;
}
mockDraggableFlatList.displayName = 'DraggableFlatList';
jest.mock('react-native-draggable-flatlist', () => mockDraggableFlatList);
