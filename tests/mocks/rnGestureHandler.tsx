import React from 'react';
import {
  DrawerLayoutAndroid,
  DrawerLayoutAndroidProps,
  FlatList,
  FlatListProps,
  ScrollView,
  ScrollViewProps,
  Slider,
  SliderProps,
  Switch,
  SwitchProps,
  TextInput,
  TextInputProps,
  TouchableHighlight,
  TouchableHighlightProps,
  TouchableNativeFeedback,
  TouchableNativeFeedbackProps,
  TouchableOpacity,
  TouchableOpacityProps,
  TouchableWithoutFeedback,
  TouchableWithoutFeedbackProps,
} from 'react-native';

/** MOCK DRAWERLAYOUT, SWIPEABLE AND TOUCHABLES */
const mockDrawerLayout: React.FC<{}> = ({ children }) => <>{children}</>;
mockDrawerLayout.displayName = 'RNGHDrawerLayout';
const mockSwipeable: React.FC<{
  renderRightActions: () => React.ReactNode;
}> = ({ children, renderRightActions }) => (
  <>
    {renderRightActions()}
    {children}
  </>
);
mockSwipeable.displayName = 'RNGHSwipeable';
const mockTouchableHighlight: React.FC<TouchableHighlightProps> = props => (
  <TouchableHighlight {...props} />
);
mockTouchableHighlight.displayName = 'RNGHTouchableHighlight';
const mockTouchableNativeFeedback: React.FC<TouchableNativeFeedbackProps> = props => (
  <TouchableNativeFeedback {...props} />
);
mockTouchableNativeFeedback.displayName = 'RNGHTouchableNativeFeedback';
const mockTouchableOpacity: React.FC<TouchableOpacityProps> = props => (
  <TouchableOpacity {...props} />
);
mockTouchableOpacity.displayName = 'RNGHTouchableOpacity';
const mockTouchableWithoutFeedback: React.FC<TouchableWithoutFeedbackProps> = props => (
  <TouchableWithoutFeedback {...props} />
);
mockTouchableWithoutFeedback.displayName = 'RNGHTouchableWithoutFeedback';

/** MOCK GESTURE HANDLERS */
const mockNativeViewGestureHandler: React.FC<{}> = ({ children }) => (
  <>{children}</>
);
mockNativeViewGestureHandler.displayName = 'RNGHNativeViewGestureHandler';
const mockPanGestureHandler: React.FC<{}> = ({ children }) => <>{children}</>;
mockPanGestureHandler.displayName = 'RNGHPanGestureHandler';
const mockTapGestureHandler: React.FC<{}> = ({ children }) => <>{children}</>;
mockTapGestureHandler.displayName = 'RNGHTapGestureHandler';
const mockFlingGestureHandler: React.FC<{}> = ({ children }) => <>{children}</>;
mockFlingGestureHandler.displayName = 'RNGHFlingGestureHandler';
const mockForceTouchGestureHandler: React.FC<{}> = ({ children }) => (
  <>{children}</>
);
mockForceTouchGestureHandler.displayName = 'RNGHForceTouchGestureHandler';
const mockLongPressGestureHandler: React.FC<{}> = ({ children }) => (
  <>{children}</>
);
mockLongPressGestureHandler.displayName = 'RNGHLongPressGestureHandler';
const mockPinchGestureHandler: React.FC<{}> = ({ children }) => <>{children}</>;
mockPinchGestureHandler.displayName = 'RNGHPinchGestureHandler';
const mockRotationGestureHandler: React.FC<{}> = ({ children }) => (
  <>{children}</>
);
mockRotationGestureHandler.displayName = 'RNGHRotationGestureHandler';

/** MOCK GESTURE COMPONENTS */
const mockDrawerLayoutAndroid: React.FC<DrawerLayoutAndroidProps> = props => (
  <DrawerLayoutAndroid {...props} />
);
mockDrawerLayoutAndroid.displayName = 'RNGHDrawerLayoutAndroid';
const mockFlatList: React.FC<FlatListProps<{}>> = props => (
  <FlatList {...props} />
);
mockFlatList.displayName = 'RNGHFlatList';
const mockScrollView: React.FC<ScrollViewProps> = props => (
  <ScrollView {...props} />
);
mockScrollView.displayName = 'RNGHScrollView';
const mockSlider: React.FC<SliderProps> = props => <Slider {...props} />;
mockSlider.displayName = 'RNGHSlider';
const mockSwitch: React.FC<SwitchProps> = props => <Switch {...props} />;
mockSwitch.displayName = 'RNGHSwitch';
const mockTextInput: React.FC<TextInputProps> = props => (
  <TextInput {...props} />
);
mockTextInput.displayName = 'RNGHTextInput';

/** MOCK GESTURE BUTTONS */
const mockRawButton: React.FC<{ onPress: () => void }> = ({ children }) => (
  <>{children}</>
);
mockRawButton.displayName = 'RNGHRawButton';
const mockBaseButton: React.FC<{ onPress: () => void }> = ({ children }) => (
  <>{children}</>
);
mockBaseButton.displayName = 'RNGHBaseButton';
const mockRectButton: React.FC<{ onPress: () => void }> = ({ children }) => (
  <>{children}</>
);
mockRectButton.displayName = 'RNGHRectButton';
const mockBorderlessButton: React.FC<{ onPress: () => void }> = ({
  children,
}) => <>{children}</>;
mockBorderlessButton.displayName = 'RNGHBorderlessButton';

jest.mock('react-native-gesture-handler', () => ({
  /** DrawerLayout, Swipeable and Touchables */
  DrawerLayout: mockDrawerLayout,
  Swipeable: mockSwipeable,
  TouchableHighlight: mockTouchableHighlight,
  TouchableNativeFeedback: mockTouchableNativeFeedback,
  TouchableOpacity: mockTouchableOpacity,
  TouchableWithoutFeedback: mockTouchableWithoutFeedback,
  /** Gesture components */
  DrawerLayoutAndroid: mockDrawerLayoutAndroid,
  FlatList: mockFlatList,
  ScrollView: mockScrollView,
  Slider: mockSlider,
  Switch: mockSwitch,
  TextInput: mockTextInput,
  /** Gesture handlers */
  NativeViewGestureHandler: mockNativeViewGestureHandler,
  TapGestureHandler: mockTapGestureHandler,
  FlingGestureHandler: mockFlingGestureHandler,
  ForceTouchGestureHandler: mockForceTouchGestureHandler,
  LongPressGestureHandler: mockLongPressGestureHandler,
  PanGestureHandler: mockPanGestureHandler,
  PinchGestureHandler: mockPinchGestureHandler,
  RotationGestureHandler: mockRotationGestureHandler,
  /** Gesture buttons */
  RawButton: mockRawButton,
  BaseButton: mockBaseButton,
  RectButton: mockRectButton,
  BorderlessButton: mockBorderlessButton,
  /** Other */
  Direction: {
    RIGHT: 1,
    LEFT: 2,
    UP: 4,
    DOWN: 8,
  },
  gestureHandlerRootHOC: jest.fn(),
  State: {
    BEGAN: 'BEGAN',
    FAILED: 'FAILED',
    ACTIVE: 'ACTIVE',
    END: 'END',
    UNDETERMINED: 'UNDETERMINED',
  },
}));

jest.mock('react-native-gesture-handler/DrawerLayout', () => mockDrawerLayout);
jest.mock('react-native-gesture-handler/Swipeable', () => mockSwipeable);
