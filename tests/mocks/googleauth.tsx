import React from 'react';
import { View } from 'react-native';

const mockGoogleSigninButton: React.FC<{}> = () => <View />;
mockGoogleSigninButton.displayName = 'GoogleSigninButton';
jest.mock('@react-native-community/google-signin', () => ({
  GoogleSignin: {
    configure: jest.fn(),
    revokeAccess: jest.fn(),
    signIn: jest.fn(() => {
      return Promise.resolve({ idToken: 'IAmGoogleToken' });
    }),
    signOut: jest.fn(),
  },
  GoogleSigninButton: mockGoogleSigninButton,
}));
