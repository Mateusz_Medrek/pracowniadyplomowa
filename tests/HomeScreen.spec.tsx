import './mocks/eventEmitter';
import './mocks/googleauth';
import './mocks/rnGestureHandler';
import './mocks/rnSafeAreaContext';
import './mocks/rnScrollView';
import './mocks/rnVectorIcons';

import { useNavigation } from '@react-navigation/native';
import { mount, shallow } from 'enzyme';
import i18n from 'i18n-js';
import moxios from 'moxios';
import React from 'react';
import { useSelector } from 'react-redux';
import wait from 'waait';

import {
  CHECK_TASK,
  DELETE_TASK,
  GET_ALL_TASKS,
  GET_ALL_TODAY_TASKS,
  GET_CURRENT_USER,
  GET_TASK,
} from '../src/api/routes';
import server, { baseURL } from '../src/api/server';
import HomeScreen, {
  FlatListEmptyComponent,
  FlatListHeaderComponent,
} from '../src/screens/HomeScreen';
import { ITask, IUser, TokenProviders } from '../src/store';
import { TranslationsKeys } from '../translations/keys';

const mockNavigate = jest.fn();
jest.mock('@react-navigation/native', () => ({
  useFocusEffect: jest.fn(),
  useIsFocused: jest.fn(() => true),
  useNavigation: jest.fn(() => ({
    navigate: mockNavigate,
  })),
}));

const mockSaveUser = jest.fn();
const mockTokenType = TokenProviders.server;
const mockUser: IUser = {
  id: '1234',
  name: 'ABCDEFGH',
  email: 'se@gmail.com',
  accountType: mockTokenType,
  tasksIds: ['1234'],
  friends: [],
};
const mockState = {
  locale: 'en-GB',
  token: {
    isLoading: false,
    token: 'IAmValidToken',
    tokenType: mockTokenType,
  },
  user: mockUser,
};
jest.mock('react-redux', () => ({
  useDispatch: jest.fn(() => ({
    user: {
      saveUser: mockSaveUser,
    },
  })),
  useSelector: jest.fn((callback: (state: Record<string, any>) => any) =>
    callback(mockState)
  ),
}));

jest.mock('../src/hooks/useCurrentUser', () => {
  return (): [() => Promise<IUser>] => [
    () =>
      Promise.resolve<IUser>({
        id: '01234',
        name: 'XYZ',
        accountType: mockTokenType,
        email: 'se@gmail.com',
        tasksIds: [],
        friends: [],
      }),
  ];
});

const tasks = [{ id: '1234', title: 'ABC' }];

const task: ITask = {
  title: 'ABC',
  description: 'abcdefghijklm',
  startingTime: new Date(),
  finishingTime: new Date(),
  subTasks: [],
};

describe('HomeScreen', () => {
  beforeEach(() => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${GET_CURRENT_USER}`, {
      response: {
        user: mockUser,
      },
    });
    moxios.stubRequest(`${baseURL}/${GET_ALL_TASKS}`, {
      response: {
        tasks,
      },
    });
    moxios.stubRequest(`${baseURL}/${GET_ALL_TODAY_TASKS}`, {
      response: {
        tasks,
      },
    });
    moxios.stubRequest(`${baseURL}/${GET_TASK}/${tasks[0].id}`, {
      response: task,
    });
    mockSaveUser.mockClear();
    (useNavigation as any).mockClear();
  });
  afterEach(() => {
    moxios.uninstall(server as any);
  });
  it('should have <ReanimatedDrawerLayout />', () => {
    const wrapper = mount(<HomeScreen />);
    expect(wrapper.find('ReanimatedDrawerLayout')).toHaveLength(1);
    wrapper.unmount();
  });
  it('should have <Header />', () => {
    const wrapper = mount(<HomeScreen />);
    expect(wrapper.find('Header')).toHaveLength(1);
    wrapper.unmount();
  });
  it('should have <TouchableOpacity /> with "Show today tasks" <Text /> as default', async () => {
    const wrapper = mount(<HomeScreen />);
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('TouchableOpacity')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.HOME_SCREEN__SHOW_TODAY_TASKS))
        )
    ).toHaveLength(1);
    wrapper.unmount();
  });
  it('should have <TouchableOpacity /> with "Show all tasks" <Text /> after clicking it before', async () => {
    const wrapper = mount(<HomeScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.HOME_SCREEN__SHOW_TODAY_TASKS))
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('TouchableOpacity')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.HOME_SCREEN__SHOW_ALL_TASKS))
        )
    ).toHaveLength(1);
    wrapper.unmount();
  });
  it('should again have <TouchableOpacity /> with "Show today tasks" <Text /> after clicking it twice', async () => {
    const wrapper = mount(<HomeScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.HOME_SCREEN__SHOW_TODAY_TASKS))
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.HOME_SCREEN__SHOW_ALL_TASKS))
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('TouchableOpacity')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.HOME_SCREEN__SHOW_TODAY_TASKS))
        )
    ).toHaveLength(1);
    wrapper.unmount();
  });
  it('should have <FlatList /> for Task items', () => {
    const wrapper = mount(<HomeScreen />);
    expect(wrapper.find('FlatList')).toHaveLength(1);
    wrapper.unmount();
  });
  it('should have <Text /> title and <Icon /> as Task item', async () => {
    const wrapper = mount(<HomeScreen />);
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('FlatList')
        .first()
        .find('CellRenderer')
        .first()
        .contains(tasks[0].title)
    ).toBe(true);
    expect(
      wrapper
        .find('FlatList')
        .first()
        .find('CellRenderer')
        .first()
        .find('TouchableOpacity')
        .last()
        .find('Icon')
    ).toHaveLength(1);
    wrapper.unmount();
  });
  it('should call navigate method with "EditTask" and taskData parameter', async () => {
    const wrapper = mount(<HomeScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('FlatList')
      .first()
      .find('CellRenderer')
      .first()
      .find('TouchableOpacity')
      .filterWhere(
        btn =>
          btn
            .find('Icon')
            .filterWhere(icn => icn.prop('name') === 'circle-edit-outline')
            .length === 1
      )
      .last()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(mockNavigate).toBeCalledWith('EditTask', {
      taskData: {
        id: tasks[0].id,
        ...task,
      },
    });
    wrapper.unmount();
  });
  it('should call navigate method with "TaskDetails" and id parameters after clicking task item', async () => {
    const wrapper = mount(<HomeScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('FlatList')
      .first()
      .find('CellRenderer')
      .first()
      .find('RNGHRectButton')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(mockNavigate).toBeCalledWith('TaskDetails', {
      id: tasks[0].id,
    });
    wrapper.unmount();
  });
  it('should call navigate method with "CompletedTasks" after clicking task item', async () => {
    const wrapper = mount(<HomeScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.HOME_SCREEN__SHOW_COMPLETED_TASKS))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(mockNavigate).toBeCalledWith('CompletedTasks');
    wrapper.unmount();
  });
  it('should delete one task item from <FlatList /> when user completes task', async () => {
    moxios.stubRequest(`${baseURL}/${CHECK_TASK}`, {
      status: 200,
    });
    const wrapper = mount(<HomeScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('FlatList')
      .first()
      .find('CellRenderer')
      .first()
      .find('TouchableOpacity')
      .filterWhere(
        btn =>
          btn
            .find('Icon')
            .filterWhere(icn => icn.prop('name') === 'check-circle').length ===
          1
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('FlatList')
        .first()
        .find('CellRenderer')
    ).toHaveLength(tasks.length - 1);
    wrapper.unmount();
  });
  it('should delete one task item from <FlatList /> when user deletes task', async () => {
    moxios.stubRequest(`${baseURL}/${DELETE_TASK}`, { status: 200 });
    const wrapper = mount(<HomeScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('FlatList')
      .first()
      .find('CellRenderer')
      .first()
      .find('TouchableOpacity')
      .filterWhere(
        btn =>
          btn.find('Icon').filterWhere(icn => icn.prop('name') === 'trash-can')
            .length === 1
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('FlatList')
        .first()
        .find('CellRenderer')
    ).toHaveLength(tasks.length - 1);
    wrapper.unmount();
  });
});

describe('FlatListEmptyComponent', () => {
  it('should have "No tasks" <Text /> when user has no tasks', async () => {
    (useSelector as any).mockImplementation(
      (callback: (state: Record<string, any>) => any) =>
        callback({
          user: {
            tasksIds: [],
          },
        })
    );
    const wrapper = mount(<FlatListEmptyComponent />);
    await wait(0);
    expect(
      wrapper.contains(i18n.t(TranslationsKeys.HOME_SCREEN__NO_TASKS))
    ).toBe(true);
    wrapper.unmount();
    (useSelector as any).mockRestore();
    (useSelector as any).mockImplementation(
      (callback: (state: Record<string, any>) => any) => callback(mockState)
    );
  });
});

describe('FlatListHeaderComponent', () => {
  it('should have "Your tasks: " <Text />', () => {
    const wrapper = shallow(<FlatListHeaderComponent />);
    expect(
      wrapper
        .find('Text')
        .first()
        .contains(i18n.t(TranslationsKeys.HOME_SCREEN__YOUR_TASKS))
    ).toBe(true);
  });
});
