import './mocks/rnGestureHandler';

import { shallow } from 'enzyme';
import i18n from 'i18n-js';
import moxios from 'moxios';
import React from 'react';
import wait from 'waait';

import { SIGN_UP_EMAIL } from '../src/api/routes';
import server, { baseURL } from '../src/api/server';
import RegistrationScreen from '../src/screens/RegistrationScreen';
import { TranslationsKeys } from '../translations/keys';

const mockAddToken = jest.fn();
const mockState = {
  locale: 'en-GB',
};
jest.mock('react-redux', () => ({
  useDispatch: jest.fn(() => ({
    token: {
      addToken: mockAddToken,
    },
  })),
  useSelector: jest.fn((callback: (state: Record<string, any>) => any) =>
    callback(mockState)
  ),
}));

describe('RegistrationScreen', () => {
  beforeEach(() => {
    mockAddToken.mockClear();
  });
  it('should have 3 <TextInput />', () => {
    const wrapper = shallow(<RegistrationScreen />);
    expect(wrapper.find('TextInput')).toHaveLength(3);
  });
  it('should have <TouchableOpacity /> with "Create account" <Text />', () => {
    const wrapper = shallow(<RegistrationScreen />);
    expect(
      wrapper
        .find('TouchableOpacity')
        .first()
        .contains(i18n.t(TranslationsKeys.REGISTRATION_SCREEN__CREATE_ACCOUNT))
    ).toBe(true);
  });
  it('should call addToken method after pressing FormSubmit button, when form values are provided, should display <ActivityIndicator /> until request is not completed', async () => {
    moxios.install(server as any);
    const wrapper = shallow(<RegistrationScreen />);
    moxios.stubRequest(`${baseURL}/${SIGN_UP_EMAIL}`, {
      responseText: 'IAmToken',
      status: 200,
    });
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.REGISTRATION_SCREEN__EMAIL_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla@gmail.com');
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.REGISTRATION_SCREEN__NAME_INPUT_PLACEHOLDER
            ) ||
          textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.REGISTRATION_SCREEN__PASSWORD_INPUT_PLACEHOLDER
            )
      )
      .forEach(textinput => textinput.invoke<any>('onChangeText')('blablabla'));
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.REGISTRATION_SCREEN__CREATE_ACCOUNT)
        )
      )
      .first()
      .invoke<any>('onPress')();
    expect(wrapper.find('ActivityIndicator')).toHaveLength(1);
    await wait(0);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.REGISTRATION_SCREEN__EMAIL_NOT_PROVIDED)
      )
    ).toBe(false);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.REGISTRATION_SCREEN__EMAIL_HAS_INVALID_FORMAT)
      )
    ).toBe(false);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.REGISTRATION_SCREEN__NAME_NOT_PROVIDED)
      )
    ).toBe(false);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.REGISTRATION_SCREEN__PASSWORD_NOT_PROVIDED)
      )
    ).toBe(false);
    expect(mockAddToken).toBeCalledTimes(1);
    moxios.uninstall(server as any);
  });
  it('should not call addToken method after pressing FormSubmit button, when any of form values is not provided', async () => {
    let wrapper = shallow(<RegistrationScreen />);
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.REGISTRATION_SCREEN__NAME_INPUT_PLACEHOLDER
            ) ||
          textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.REGISTRATION_SCREEN__PASSWORD_INPUT_PLACEHOLDER
            )
      )
      .forEach(textinput => textinput.invoke<any>('onChangeText')('blablabla'));
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.REGISTRATION_SCREEN__CREATE_ACCOUNT)
        )
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.REGISTRATION_SCREEN__EMAIL_NOT_PROVIDED)
      )
    ).toBe(true);
    expect(mockAddToken).toBeCalledTimes(0);
    wrapper = shallow(<RegistrationScreen />);
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.REGISTRATION_SCREEN__EMAIL_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla@gmail.com');
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.REGISTRATION_SCREEN__PASSWORD_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('blablabla');
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.REGISTRATION_SCREEN__CREATE_ACCOUNT)
        )
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.REGISTRATION_SCREEN__NAME_NOT_PROVIDED)
      )
    ).toBe(true);
    expect(mockAddToken).toBeCalledTimes(0);
    wrapper = shallow(<RegistrationScreen />);
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.REGISTRATION_SCREEN__EMAIL_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla@gmail.com');
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.REGISTRATION_SCREEN__NAME_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla');
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.REGISTRATION_SCREEN__CREATE_ACCOUNT)
        )
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.REGISTRATION_SCREEN__PASSWORD_NOT_PROVIDED)
      )
    ).toBe(true);
    expect(mockAddToken).toBeCalledTimes(0);
  });
  it('should display error text when email has invalid format', async () => {
    const wrapper = shallow(<RegistrationScreen />);
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.REGISTRATION_SCREEN__EMAIL_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla');
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.REGISTRATION_SCREEN__NAME_INPUT_PLACEHOLDER
            ) ||
          textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.REGISTRATION_SCREEN__PASSWORD_INPUT_PLACEHOLDER
            )
      )
      .forEach(textinput => textinput.invoke<any>('onChangeText')('blablabla'));
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.REGISTRATION_SCREEN__CREATE_ACCOUNT)
        )
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.REGISTRATION_SCREEN__EMAIL_HAS_INVALID_FORMAT)
      )
    ).toBe(true);
    expect(mockAddToken).toBeCalledTimes(0);
  });
  it('should display error text after getting "There is already account with this email" error', async () => {
    moxios.install(server as any);
    const wrapper = shallow(<RegistrationScreen />);
    moxios.stubRequest(`${baseURL}/${SIGN_UP_EMAIL}`, {
      response: { error: 'There is already account with this email' },
      status: 401,
    });
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.REGISTRATION_SCREEN__EMAIL_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla@gmail.com');
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.REGISTRATION_SCREEN__NAME_INPUT_PLACEHOLDER
            ) ||
          textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.REGISTRATION_SCREEN__PASSWORD_INPUT_PLACEHOLDER
            )
      )
      .forEach(textinput => textinput.invoke<any>('onChangeText')('blablabla'));
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.REGISTRATION_SCREEN__CREATE_ACCOUNT)
        )
      )
      .first()
      .invoke<any>('onPress')();
    expect(wrapper.find('ActivityIndicator')).toHaveLength(1);
    await wait(0);
    wrapper.update();
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.REGISTRATION_SCREEN__SERVER_EMAIL_ERROR)
      )
    ).toBe(true);
    moxios.uninstall(server as any);
  });
  it('should have <ChangeLocale />', () => {
    const wrapper = shallow(<RegistrationScreen />);
    expect(wrapper.find('ChangeLocale')).toHaveLength(1);
  });
});
