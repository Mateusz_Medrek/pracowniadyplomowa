import './mocks/rnGestureHandler';
import './mocks/rnVectorIcons';

import { shallow } from 'enzyme';
import React from 'react';
import wait from 'waait';

import SubTask from '../src/components/SubTask';

const assignees = [
  { id: '0', name: 'Yourself' },
  { id: '1', name: 'XYZ' },
];

describe('SubTask', () => {
  it('should have 1 <TextInput />', () => {
    const wrapper = shallow(
      <SubTask assigneesList={assignees} onSubmit={jest.fn()} />
    );
    expect(wrapper.find('TextInput')).toHaveLength(1);
  });
  it('should have <TouchableOpacity />', () => {
    const wrapper = shallow(
      <SubTask assigneesList={assignees} onSubmit={jest.fn()} />
    );
    expect(wrapper.find('TouchableOpacity')).toHaveLength(1);
  });
  it('should have 2 <Picker />', () => {
    const wrapper = shallow(
      <SubTask assigneesList={assignees} onSubmit={jest.fn()} />
    );
    expect(wrapper.find('Picker')).toHaveLength(2);
  });
  it('should have 5 items in first <Picker />', () => {
    const wrapper = shallow(
      <SubTask assigneesList={assignees} onSubmit={jest.fn()} />
    );
    expect(
      wrapper
        .find('Picker')
        .first()
        .find('PickerItem')
    ).toHaveLength(5);
  });
  it('should have at least 1 item in last <Picker />', () => {
    const wrapper = shallow(
      <SubTask assigneesList={assignees} onSubmit={jest.fn()} />
    );
    expect(
      wrapper
        .find('Picker')
        .last()
        .find('PickerItem').length
    ).toBeGreaterThanOrEqual(1);
  });
  it('should have submit button disabled until all values are not provided', async () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(
      <SubTask assigneesList={assignees} onSubmit={onSubmit} />
    );
    expect(
      wrapper
        .find('TouchableOpacity')
        .first()
        .prop('disabled')
    ).toBe(true);
  });
  it('should call onSubmit prop, after pressing "X" button, when all values are provided', async () => {
    const onSubmit = jest.fn();
    const wrapper = shallow(
      <SubTask assigneesList={assignees} onSubmit={onSubmit} />
    );
    wrapper
      .find('TextInput')
      .first()
      .invoke<any>('onChangeText')('XYZ');
    wrapper
      .find('Picker')
      .first()
      .invoke<any>('onValueChange')(1, 1);
    wrapper
      .find('Picker')
      .last()
      .invoke<any>('onValueChange')(assignees[0].id, 2);
    expect(
      wrapper
        .find('TouchableOpacity')
        .first()
        .prop('disabled')
    ).toBe(false);
    wrapper
      .find('TouchableOpacity')
      .first()
      .simulate('press');
    await wait(0);
    expect(onSubmit).toBeCalledTimes(1);
  });
  it('should have values passed as a props', async () => {
    const onSubmit = jest.fn();
    const title = 'XYZ';
    const category = 3;
    const wrapper = shallow(
      <SubTask
        assigneesList={assignees}
        onSubmit={onSubmit}
        assigneeId={assignees[1].id}
        category={category}
        id={13}
        title={title}
      />
    );
    expect(
      wrapper
        .find('TextInput')
        .first()
        .prop('value')
    ).toEqual(title);
    expect(
      wrapper
        .find('Picker')
        .first()
        .prop('selectedValue')
    ).toEqual(category);
    expect(
      wrapper
        .find('Picker')
        .last()
        .prop('selectedValue')
    ).toEqual('1');
  });
});
