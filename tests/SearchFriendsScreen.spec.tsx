import './mocks/eventEmitter';
import './mocks/googleauth';
import './mocks/rnGestureHandler';
import './mocks/rnSafeAreaContext';
import './mocks/rnVectorIcons';

import { useNavigation } from '@react-navigation/native';
import { shallow } from 'enzyme';
import i18n from 'i18n-js';
import moxios from 'moxios';
import React from 'react';
import { useSelector } from 'react-redux';
import wait from 'waait';

import { GET_USER_BY_NAME } from '../src/api/routes';
import server, { baseURL } from '../src/api/server';
import SearchFriendsScreen, {
  FlatListEmptyComponent,
} from '../src/screens/SearchFriendsScreen';
import { TokenProviders, IUser } from '../src/store';
import { TranslationsKeys } from '../translations/keys';

const mockNavigate = jest.fn();
jest.mock('@react-navigation/native', () => ({
  useNavigation: jest.fn(() => ({
    navigate: mockNavigate,
  })),
}));

const mockAddFriend = jest.fn();
const mockRemoveFriend = jest.fn();
const mockSaveUser = jest.fn();
const mockSomeUser = { id: '90123', name: 'abcdefgh' };
const mockTokenType = TokenProviders.server;
const mockUser: IUser = {
  id: '45678',
  name: 'ABCDEFGH',
  email: 'se@gmail.com',
  accountType: mockTokenType,
  tasksIds: [],
  friends: [mockSomeUser],
};
const mockState = {
  locale: 'en-GB',
  token: {
    isLoading: false,
    token: 'IAmToken',
    tokenType: mockTokenType,
  },
  user: mockUser,
};
jest.mock('react-redux', () => ({
  useDispatch: jest.fn(() => ({
    user: {
      addFriend: mockAddFriend,
      removeFriend: mockRemoveFriend,
      saveUser: mockSaveUser,
    },
  })),
  useSelector: jest.fn((callback: (state: Record<string, any>) => any) =>
    callback(mockState)
  ),
}));

jest.mock('../src/hooks/useCurrentUser', () => {
  return (): [() => Promise<IUser>] => [
    () =>
      Promise.resolve<IUser>({
        id: '01234',
        name: 'XYZ',
        accountType: mockTokenType,
        email: 'se@gmail.com',
        tasksIds: [],
        friends: [],
      }),
  ];
});

describe('SearchFriendsScreen', () => {
  beforeEach(() => {
    mockAddFriend.mockClear();
    mockRemoveFriend.mockClear();
    mockSaveUser.mockClear();
    (useNavigation as any).mockClear();
  });
  it('should have <ReanimatedDrawerLayout />', () => {
    const wrapper = shallow(<SearchFriendsScreen />);
    expect(wrapper.find('ReanimatedDrawerLayout')).toHaveLength(1);
  });
  it('should have <TextInput />', () => {
    const wrapper = shallow(<SearchFriendsScreen />);
    expect(wrapper.find('TextInput')).toHaveLength(1);
  });
  it('should have <TouchableOpacity /> with "Search" <Text />', () => {
    const wrapper = shallow(<SearchFriendsScreen />);
    expect(wrapper.find('TouchableOpacity').length).toBeGreaterThanOrEqual(1);
    expect(
      wrapper
        .find('TouchableOpacity')
        .last()
        .contains(i18n.t(TranslationsKeys.SEARCH_FRIENDS_SCREEN__SEARCH_BUTTON))
    ).toBe(true);
  });
  it('should have <FlatList />', () => {
    const wrapper = shallow(<SearchFriendsScreen />);
    expect(wrapper.find('FlatList')).toHaveLength(1);
  });
  it('should render search results after typing value, should not render current logged user', async () => {
    const inputValue = 'ABCDEFGH';
    const response = {
      users: [
        { id: '45678', name: 'ABCDEFGH' },
        { id: '90123', name: 'abcdefgh' },
      ],
    };
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${GET_USER_BY_NAME}?name=${inputValue}`, {
      response,
    });
    const wrapper = shallow(<SearchFriendsScreen />);
    wrapper
      .find('TextInput')
      .first()
      .invoke<any>('onChangeText')(inputValue);
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SEARCH_FRIENDS_SCREEN__SEARCH_BUTTON)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    const cellRenderers = wrapper
      .find('FlatList')
      .first()
      .dive()
      .dive()
      .dive()
      .find('CellRenderer');
    expect(cellRenderers).toHaveLength(response.users.length - 1);
    cellRenderers.map((cell, index) => {
      if (response.users[index].id !== mockUser.id) {
        expect(cell.contains(response.users[index].name)).toBe(true);
      }
    });
    moxios.uninstall(server as any);
  });
  it('should call navigate method with "FriendsDetail", after pressing on FlatList rendered element', async () => {
    const inputValue = 'ABCDEFGH';
    const response = {
      users: [
        { id: '87654', name: 'ABCDEFGH' },
        { id: '90123', name: 'abcdefgh' },
      ],
    };
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${GET_USER_BY_NAME}?name=${inputValue}`, {
      response,
    });
    const wrapper = shallow(<SearchFriendsScreen />);
    wrapper
      .find('TextInput')
      .first()
      .invoke<any>('onChangeText')(inputValue);
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SEARCH_FRIENDS_SCREEN__SEARCH_BUTTON)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    const cellRenderers = wrapper
      .find('FlatList')
      .first()
      .dive()
      .dive()
      .dive()
      .find('CellRenderer');
    expect(cellRenderers).toHaveLength(response.users.length);
    cellRenderers
      .first()
      .dive()
      .dive()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    expect(mockNavigate).toBeCalledWith('FriendsDetail', {
      id: response.users[0].id,
    });
    moxios.uninstall(server as any);
  });
  it('should call removeFriend action creator after long button press, when pressed user is friend', async () => {
    const inputValue = 'ABCDEFGH';
    const response = {
      users: [{ id: '45678', name: 'ABCDEFGH' }, mockSomeUser],
    };
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${GET_USER_BY_NAME}?name=${inputValue}`, {
      response,
    });
    const wrapper = shallow(<SearchFriendsScreen />);
    wrapper
      .find('TextInput')
      .first()
      .invoke<any>('onChangeText')(inputValue);
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SEARCH_FRIENDS_SCREEN__SEARCH_BUTTON)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('FlatList')
      .first()
      .dive()
      .dive()
      .dive()
      .find('CellRenderer')
      .first()
      .dive()
      .dive()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onLongPress')();
    await wait(0);
    expect(mockRemoveFriend).toBeCalledWith(
      mockSomeUser,
      'Bearer ' + TokenProviders.server + 'IAmToken'
    );
    moxios.uninstall(server as any);
  });
  it('should call addFriend action creator after long button press, when pressed user is not friend', async () => {
    (useSelector as any).mockImplementation(
      (callback: (state: Record<string, any>) => any) =>
        callback({
          token: {
            isLoading: false,
            token: 'IAmToken',
            tokenType: mockTokenType,
          },
          user: {
            id: '45678',
            name: 'ABCDEFGH',
            email: 'se@gmail.com',
            accountType: TokenProviders.server,
            tasksIds: [],
            friends: [],
          },
        })
    );
    const inputValue = 'ABCDEFGH';
    const response = {
      users: [{ id: '45678', name: 'ABCDEFGH' }, mockSomeUser],
    };
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${GET_USER_BY_NAME}?name=${inputValue}`, {
      response,
    });
    const wrapper = shallow(<SearchFriendsScreen />);
    wrapper
      .find('TextInput')
      .first()
      .invoke<any>('onChangeText')(inputValue);
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SEARCH_FRIENDS_SCREEN__SEARCH_BUTTON)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper
      .find('FlatList')
      .first()
      .dive()
      .dive()
      .dive()
      .find('CellRenderer')
      .first()
      .dive()
      .dive()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onLongPress')();
    await wait(0);
    expect(mockAddFriend).toBeCalledWith(
      mockSomeUser,
      'Bearer ' + TokenProviders.server + 'IAmToken'
    );
    (useSelector as any).mockRestore();
    (useSelector as any).mockImplementation(
      (callback: (state: Record<string, any>) => any) => callback(mockState)
    );
    moxios.uninstall(server as any);
  });
});

describe('FlatListEmptyComponent', () => {
  it('should have <Text /> with instruction', () => {
    const wrapper = shallow(<FlatListEmptyComponent />);
    expect(
      wrapper
        .find('Text')
        .first()
        .contains(i18n.t(TranslationsKeys.SEARCH_FRIENDS_SCREEN__INSTRUCTION))
    ).toBe(true);
  });
});
