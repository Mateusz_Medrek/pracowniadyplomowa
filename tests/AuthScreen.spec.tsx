import './mocks/googleauth';
import './mocks/rnGestureHandler';
import './mocks/rnScrollView';
import './mocks/rnVectorIcons';

import { useNavigation } from '@react-navigation/native';
import { shallow } from 'enzyme';
import i18n from 'i18n-js';
import React from 'react';
import { act } from 'react-test-renderer';
import moxios from 'moxios';
import wait from 'waait';

import { SIGN_IN, SIGN_UP_GOOGLE } from '../src/api/routes';
import server, { baseURL } from '../src/api/server';
import AuthScreen from '../src/screens/AuthScreen';
import { TokenProviders } from '../src/store';
import { TranslationsKeys } from '../translations/keys';

const mockNavigate = jest.fn();
jest.mock('@react-navigation/native', () => ({
  useNavigation: jest.fn(() => ({
    navigate: mockNavigate,
  })),
}));

const mockAddToken = jest.fn();
const mockTokenType = TokenProviders.server;
const mockState = {
  locale: 'en-GB',
  token: {
    isLoading: false,
    token: '',
    tokenType: mockTokenType,
  },
};
jest.mock('react-redux', () => ({
  useDispatch: jest.fn(() => ({
    token: {
      addToken: mockAddToken,
    },
  })),
  useSelector: jest.fn((callback: (state: Record<string, any>) => any) =>
    callback(mockState)
  ),
}));

describe('AuthScreen', () => {
  beforeEach(() => {
    mockAddToken.mockClear();
    (useNavigation as any).mockClear();
  });
  it('should have 2 <TextInput />', () => {
    const wrapper = shallow(<AuthScreen />);
    expect(wrapper.find('TextInput')).toHaveLength(2);
  });
  it('should have <TouchableOpacity /> with "Sign in" <Text />', () => {
    const wrapper = shallow(<AuthScreen />);
    expect(
      wrapper
        .find('TouchableOpacity')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_IN))
        )
    ).toHaveLength(1);
  });
  it('should have "OR" <Text />', () => {
    const wrapper = shallow(<AuthScreen />);
    expect(wrapper.contains(i18n.t(TranslationsKeys.AUTH_SCREEN__OR))).toBe(
      true
    );
  });
  it('should have 1 <TouchableOpacity /> with "Continue with" <Text /> and 1 <Icon />', () => {
    const wrapper = shallow(<AuthScreen />);
    expect(
      wrapper
        .find('TouchableOpacity')
        .filterWhere(
          btn =>
            btn.contains(
              i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_IN_GOOGLE)
            ) &&
            btn.find('Icon').filterWhere(icn => icn.prop('name') === 'google')
              .length === 1
        )
    ).toHaveLength(1);
  });
  it('should call addToken method after pressing FormSubmit button, when form values are provided, should display <ActivityIndicator /> until user is not authorized', async () => {
    moxios.install(server as any);
    const wrapper = shallow(<AuthScreen />);
    moxios.stubRequest(`${baseURL}/${SIGN_IN}`, {
      responseText: 'IAmToken',
      status: 200,
    });
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.AUTH_SCREEN__EMAIL_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla@gmail.com');
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.AUTH_SCREEN__PASSWORD_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla');
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_IN))
      )
      .first()
      .invoke<any>('onPress')();
    expect(wrapper.find('ActivityIndicator')).toHaveLength(1);
    await wait(0);
    expect(mockAddToken).toBeCalledTimes(1);
    moxios.uninstall(server as any);
  });
  it('should not call addToken method after pressing FormSubmit button, when any of form values is not provided', async () => {
    let wrapper = shallow(<AuthScreen />);
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.AUTH_SCREEN__PASSWORD_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla');
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_IN))
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(
      wrapper.contains(i18n.t(TranslationsKeys.AUTH_SCREEN__EMAIL_NOT_PROVIDED))
    ).toBe(true);
    expect(mockAddToken).toBeCalledTimes(0);
    wrapper = shallow(<AuthScreen />);
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.AUTH_SCREEN__EMAIL_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla@gmail.com');
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_IN))
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.AUTH_SCREEN__PASSWORD_NOT_PROVIDED)
      )
    ).toBe(true);
    expect(mockAddToken).toBeCalledTimes(0);
  });
  it('should call addToken method after pressing GoogleButton', async () => {
    moxios.install(server as any);
    const wrapper = shallow(<AuthScreen />);
    moxios.stubRequest(`${baseURL}/${SIGN_UP_GOOGLE}`, {
      responseText: 'IAmGoogleToken',
      status: 200,
    });
    await act(() => {
      wrapper
        .find('TouchableOpacity')
        .filterWhere(
          btn =>
            btn.find('Icon').filterWhere(icn => icn.prop('name') === 'google')
              .length === 1
        )
        .first()
        .invoke<any>('onPress')();
    });
    expect(wrapper.find('ActivityIndicator')).toHaveLength(1);
    await wait(0);
    expect(mockAddToken).toBeCalledTimes(1);
    moxios.uninstall(server as any);
  });
  it('should have <TouchableOpacity /> with "Sign up" <Text />', () => {
    const wrapper = shallow(<AuthScreen />);
    expect(
      wrapper
        .find('TouchableOpacity')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_UP))
        )
    ).toHaveLength(1);
  });
  it('should call navigate method after pressing navigateToRegistartion button', async () => {
    const wrapper = shallow(<AuthScreen />);
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_UP))
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(mockNavigate).toBeCalledWith('Registration');
  });
  it('should display error text after getting "Invalid email" error message', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${SIGN_IN}`, {
      response: { error: 'Invalid email' },
      status: 401,
    });
    const wrapper = shallow(<AuthScreen />);
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.AUTH_SCREEN__EMAIL_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla@gmail.com');
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.AUTH_SCREEN__PASSWORD_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla');
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_IN))
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.AUTH_SCREEN__SERVER_INVALID_EMAIL)
      )
    ).toBe(true);
    moxios.uninstall(server as any);
  });
  it('should display error text after providing email with invalid format', async () => {
    const wrapper = shallow(<AuthScreen />);
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.AUTH_SCREEN__EMAIL_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla');
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.AUTH_SCREEN__PASSWORD_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla');
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_IN))
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.AUTH_SCREEN__EMAIL_HAS_INVALID_FORMAT)
      )
    ).toBe(true);
  });
  it('should display error text after getting "Invalid password" error message', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${SIGN_IN}`, {
      response: { error: 'Invalid password' },
      status: 401,
    });
    const wrapper = shallow(<AuthScreen />);
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.AUTH_SCREEN__EMAIL_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla@gmail.com');
    wrapper
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.AUTH_SCREEN__PASSWORD_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('blablabla');
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_IN))
      )
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.AUTH_SCREEN__SERVER_INVALID_PASSWORD)
      )
    ).toBe(true);
    moxios.uninstall(server as any);
  });
  it('should have <ChangeLocale />', () => {
    const wrapper = shallow(<AuthScreen />);
    expect(wrapper.find('ChangeLocale')).toHaveLength(1);
  });
});
