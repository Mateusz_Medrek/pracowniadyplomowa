import 'jsdom-global/register';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import i18n from 'i18n-js';
import { NativeModules } from 'react-native';
import mockAsyncStorage from './mocks/rncAsyncStorage';
import RNCNetInfoMock from '@react-native-community/netinfo/jest/netinfo-mock';

import en from '../translations/en';

// ENZYME MOUNT;
Enzyme.configure({ adapter: new Adapter() });

const originalConsoleError = console.error;
console.error = message => {
  if (message.startsWith('Warning:')) {
    return;
  }
  originalConsoleError(message);
};
//

// @react-native-community/async-storage
jest.mock('@react-native-community/async-storage', () => mockAsyncStorage);
//

// @react-native-community/netinfo
NativeModules.RNCNetInfo = RNCNetInfoMock;
//

// react-native-localize
const mockInitialConstants = {
  calendar: 'gregorian',
  country: 'GB',
  currencies: ['GBP', 'EUR', 'PLN'],
  locales: [
    // you can choose / add the locales you want
    {
      countryCode: 'GB',
      languageTag: 'en-GB',
      languageCode: 'en',
      isRTL: false,
    },
    {
      countryCode: 'PL',
      languageTag: 'pl-PL',
      languageCode: 'pl',
      isRTL: false,
    },
  ],
  numberFormatSettings: {
    decimalSeparator: '.',
    groupingSeparator: ',',
  },
  temperatureUnit: 'celsius',
  timeZone: 'Europe/Paris',
  uses24HourClock: true,
  usesMetricSystem: true,
  usesAutoDateAndTime: true,
  usesAutoTimeZone: true,
};
NativeModules.RNLocalize = {
  initialConstants: mockInitialConstants,
};
jest.mock('react-native-localize', () => ({
  getLocales: jest.fn(() => mockInitialConstants.locales),
  findBestAvailableLanguage: jest.fn(() => ({
    // use a provided translation, or return undefined to test your fallback
    languageTag: 'en-GB',
    isRTL: false,
  })),
  getNumberFormatSettings: jest.fn(
    () => mockInitialConstants.numberFormatSettings
  ),
  getCalendar: jest.fn(() => mockInitialConstants.calendar), // or "japanese", "buddhist"
  getCountry: jest.fn(() => mockInitialConstants.country), // the country code you want
  getCurrencies: jest.fn(() => mockInitialConstants.currencies), // can be empty array
  getTemperatureUnit: jest.fn(() => mockInitialConstants.temperatureUnit),
  getTimeZone: jest.fn(() => mockInitialConstants.timeZone), // the timezone you want
  uses24HourClock: jest.fn(() => mockInitialConstants.uses24HourClock),
  usesMetricSystem: jest.fn(() => mockInitialConstants.usesMetricSystem),
  addEventListener: jest.fn(),
  removeEventListener: jest.fn(),
}));
//

// react-native-reanimated
jest.mock('react-native-reanimated', () =>
  require('react-native-reanimated/mock')
);
//

// I18n tests setup
i18n.translations = {
  'en-GB': en,
};
i18n.locale = 'en-GB';
//
