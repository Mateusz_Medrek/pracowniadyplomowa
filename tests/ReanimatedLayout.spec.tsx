import { shallow } from 'enzyme';
import React from 'react';

import ReanimatedDrawerLayout from '../src/components/ReanimatedDrawerLayout';

describe('ReanimatedLayout', () => {
  it('should contain DrawerView', () => {
    const wrapper = shallow<ReanimatedDrawerLayout>(<ReanimatedDrawerLayout />);
    expect(wrapper.find('DrawerView')).toHaveLength(1);
  });
  it('should set isDrawerOpen state to true after calling openDrawer method', () => {
    const wrapper = shallow<ReanimatedDrawerLayout>(<ReanimatedDrawerLayout />);
    wrapper.instance().openDrawer();
    expect(wrapper.state('isDrawerOpen')).toBe(true);
  });
  it('should set isDrawerOpen state to false after calling closeDrawer method', () => {
    const wrapper = shallow<ReanimatedDrawerLayout>(<ReanimatedDrawerLayout />);
    wrapper.instance().openDrawer();
    expect(wrapper.state('isDrawerOpen')).toBe(true);
    wrapper.instance().closeDrawer();
    expect(wrapper.state('isDrawerOpen')).toBe(false);
  });
});
