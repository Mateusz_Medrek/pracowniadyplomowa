import './mocks/rnGestureHandler';
import './mocks/rnScrollView';
import './mocks/rnVectorIcons';

import { useNavigation } from '@react-navigation/native';
import { mount, shallow } from 'enzyme';
import i18n from 'i18n-js';
import moxios from 'moxios';
import React from 'react';
import wait from 'waait';

import {
  CHANGE_EMAIL,
  CHANGE_NAME,
  CHANGE_PASSWORD,
  GET_CURRENT_USER,
} from '../src/api/routes';
import server, { baseURL } from '../src/api/server';
import SettingsScreen from '../src/screens/SettingsScreen';
import { IUser, TokenProviders } from '../src/store';
import { TranslationsKeys } from '../translations/keys';

const mockGoBack = jest.fn();
jest.mock('@react-navigation/native', () => ({
  useNavigation: jest.fn(() => ({
    goBack: mockGoBack,
  })),
}));

const mockSaveUser = jest.fn();
const mockTokenType = TokenProviders.server;
const mockUser: IUser = {
  id: '01234567',
  accountType: mockTokenType,
  email: 'se@gmail.com',
  name: 'XYZ',
  tasksIds: [],
  friends: [{ id: '0987654', name: 'CBA' }],
};
const mockState = {
  token: {
    isLoading: false,
    token: 'IAmToken',
    tokenType: mockTokenType,
  },
  user: mockUser,
};
jest.mock('react-redux', () => ({
  useDispatch: jest.fn(() => ({
    user: {
      saveUser: mockSaveUser,
    },
  })),
  useSelector: jest.fn((callback: (state: Record<string, any>) => any) =>
    callback(mockState)
  ),
}));

describe('SettingsScreen', () => {
  beforeEach(() => {
    mockSaveUser.mockClear();
    (useNavigation as any).mockClear();
  });
  it('should have <Text /> for email, name and changing email, name, and password', async () => {
    const wrapper = shallow(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_EMAIL)
      )
    ).toBe(true);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_NAME)
      )
    ).toBe(true);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_PASSWORD)
      )
    ).toBe(true);
  });
  it('should have <ActivityIndicator /> when email request is pending', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${CHANGE_EMAIL}`, { status: 200 });
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_EMAIL)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_EMAIL_FORM__NEW_EMAIL_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('someemail@gmail.com');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__PASSWORD_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('sometext');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL))
      )
      .invoke<any>('onPress')();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
        )
        .first()
        .find('TouchableOpacity')
        .filterWhere(btn => btn.find('ActivityIndicator').length === 2) // mount
    ).toHaveLength(1);
    moxios.uninstall(server as any);
  });
  it('should have <ActivityIndicator /> when name request is pending', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${CHANGE_NAME}`, { status: 200 });
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_NAME)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__NEW_NAME_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('sometext');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
      )
      .first()
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME))
      )
      .invoke<any>('onPress')();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
        )
        .first()
        .find('TouchableOpacity')
        .filterWhere(btn => btn.find('ActivityIndicator').length === 2) // mount
    ).toHaveLength(1);
    moxios.uninstall(server as any);
  });
  it('should have <ActivityIndicator /> when password request is pending', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${CHANGE_PASSWORD}`, { status: 200 });
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_PASSWORD)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_PASSWORD_FORM__OLD_PASSWORD_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('sometext');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_PASSWORD_FORM__NEW_PASSWORD_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('someothertext');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
        )
      )
      .invoke<any>('onPress')();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
        )
        .find('TouchableOpacity')
        .filterWhere(btn => btn.find('ActivityIndicator').length === 2) // mount
    ).toHaveLength(1);
    moxios.uninstall(server as any);
  });
  it('should have error <Text /> when email has invalid format', async () => {
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_EMAIL)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_EMAIL_FORM__NEW_EMAIL_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('someemailgmail.com');
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__PASSWORD_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('sometext');
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
        )
        .first()
        .contains(
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__EMAIL_HAS_INVALID_FORMAT)
        )
    ).toBe(true);
    wrapper.unmount();
  });
  it('should have error <Text /> when email is not provided', async () => {
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_EMAIL)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
        )
        .first()
        .contains(
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__NEW_EMAIL_NOT_PROVIDED)
        )
    ).toBe(true);
    wrapper.unmount();
  });
  it('should have error <Text /> when password is not provided during changing email process', async () => {
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_EMAIL)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_EMAIL_FORM__NEW_EMAIL_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('someemail@gmail.com');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
        )
        .first()
        .contains(
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__PASSWORD_NOT_PROVIDED)
        )
    ).toBe(true);
    wrapper.unmount();
  });
  it('should have error <Text /> when email request return error', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${CHANGE_EMAIL}`, { status: 400 });
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_EMAIL)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_EMAIL_FORM__NEW_EMAIL_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('someemail@gmail.com');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__PASSWORD_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('sometext');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
        )
        .first()
        .contains(i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__UNKNOWN_ERROR))
    ).toBe(true);
    wrapper.unmount();
    moxios.uninstall(server as any);
  });
  it('should have error <Text /> when name is not provided', async () => {
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_NAME)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
      )
      .first()
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
        )
        .first()
        .contains(
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__NEW_NAME_NOT_PROVIDED)
        )
    ).toBe(true);
    wrapper.unmount();
  });
  it('should have error <Text /> when name request return error', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${CHANGE_NAME}`, { status: 400 });
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_NAME)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__NEW_NAME_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('sometext');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
      )
      .first()
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
        )
        .first()
        .contains(i18n.t(TranslationsKeys.CHANGE_NAME_FORM__UNKNOWN_ERROR))
    ).toBe(true);
    wrapper.unmount();
    moxios.uninstall(server as any);
  });
  it('should have error <Text /> when oldPassword is not provided', async () => {
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_PASSWORD)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
        )
        .contains(
          i18n.t(
            TranslationsKeys.CHANGE_PASSWORD_FORM__OLD_PASSWORD_NOT_PROVIDED
          )
        )
    ).toBe(true);
    wrapper.unmount();
  });
  it('should have error <Text /> when newPassword is not provided', async () => {
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_PASSWORD)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_PASSWORD_FORM__OLD_PASSWORD_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('sometext');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
        )
        .contains(
          i18n.t(
            TranslationsKeys.CHANGE_PASSWORD_FORM__NEW_PASSWORD_NOT_PROVIDED
          )
        )
    ).toBe(true);
    wrapper.unmount();
  });
  it('should have error <Text /> when password request return error', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${CHANGE_PASSWORD}`, { status: 400 });
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_PASSWORD)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_PASSWORD_FORM__OLD_PASSWORD_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('sometext');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_PASSWORD_FORM__NEW_PASSWORD_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('someothertext');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
        )
        .contains(i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__UNKNOWN_ERROR))
    ).toBe(true);
    wrapper.unmount();
    moxios.uninstall(server as any);
  });
  it('should have <Modal /> not visible on default', async () => {
    const wrapper = shallow(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ReanimatedModal')
        .first()
        .prop('isVisible')
    ).toBe(0);
    moxios.uninstall(server as any);
  });
  it('should have <Modal /> visible when change email request is completed and not visible when user dismisses it', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${CHANGE_EMAIL}`, { status: 200 });
    const response = {
      id: '01234567',
      accountType: TokenProviders.server,
      email: 'somemail@gmail.com',
      name: 'XYZ',
      tasksIds: [],
      friends: [{ id: '0987654', name: 'CBA' }],
    };
    moxios.stubRequest(`${baseURL}/${GET_CURRENT_USER}`, { response });
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_EMAIL)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_EMAIL_FORM__NEW_EMAIL_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')(response.email);
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__PASSWORD_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('sometext');
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ReanimatedModal')
        .first()
        .prop('isVisible')
    ).toBe(1);
    wrapper
      .find('ReanimatedModal')
      .first()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ReanimatedModal')
        .first()
        .prop('isVisible')
    ).toBe(0);
    wrapper.unmount();
    moxios.uninstall(server as any);
  });
  it('should have <Modal /> visible when change name request is completed and not visible when user dismisses it', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${CHANGE_NAME}`, { status: 200 });
    const response = {
      id: '01234567',
      accountType: TokenProviders.server,
      email: 'somemail@gmail.com',
      name: 'someText',
      tasksIds: [],
      friends: [{ id: '0987654', name: 'CBA' }],
    };
    moxios.stubRequest(`${baseURL}/${GET_CURRENT_USER}`, { response });
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_NAME)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__NEW_NAME_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')(response.name);
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
      )
      .first()
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ReanimatedModal')
        .first()
        .prop('isVisible')
    ).toBe(1);
    wrapper
      .find('ReanimatedModal')
      .first()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ReanimatedModal')
        .first()
        .prop('isVisible')
    ).toBe(0);
    wrapper.unmount();
    moxios.uninstall(server as any);
  });
  it('should have <Modal /> visible when change password request is completed and not visible when user dismisses it', async () => {
    moxios.install(server as any);
    moxios.stubRequest(`${baseURL}/${CHANGE_PASSWORD}`, { status: 200 });
    const response = {
      id: '01234567',
      accountType: TokenProviders.server,
      email: 'somemail@gmail.com',
      name: 'someText',
      tasksIds: [],
      friends: [{ id: '0987654', name: 'CBA' }],
    };
    moxios.stubRequest(`${baseURL}/${GET_CURRENT_USER}`, { response });
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_PASSWORD)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_PASSWORD_FORM__OLD_PASSWORD_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('sometext');
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_PASSWORD_FORM__NEW_PASSWORD_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('someothertext');
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ReanimatedModal')
        .first()
        .prop('isVisible')
    ).toBe(1);
    wrapper
      .find('ReanimatedModal')
      .first()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ReanimatedModal')
        .first()
        .prop('isVisible')
    ).toBe(0);
    wrapper.unmount();
    moxios.uninstall(server as any);
  });
  it('should clear email form after clicking X button', async () => {
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_EMAIL)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_EMAIL_FORM__NEW_EMAIL_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('someText@gmail.com');
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__PASSWORD_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('sometext');
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
      )
      .first()
      .find('IconButton')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
        )
        .first()
        .find('TextInput')
        .filterWhere(
          textinput =>
            textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.CHANGE_EMAIL_FORM__NEW_EMAIL_INPUT_PLACEHOLDER
            )
        )
        .first()
        .prop('value')
    ).toBe('');
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL)
        )
        .first()
        .find('TextInput')
        .filterWhere(
          textinput =>
            textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.CHANGE_EMAIL_FORM__PASSWORD_INPUT_PLACEHOLDER
            )
        )
        .first()
        .prop('value')
    ).toBe('');
  });
  it('should clear name form after clicking X button', async () => {
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_EMAIL)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
      )
      .first()
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__NEW_NAME_INPUT_PLACEHOLDER)
      )
      .first()
      .invoke<any>('onChangeText')('someText');
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
      )
      .first()
      .find('IconButton')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME)
        )
        .first()
        .find('TextInput')
        .filterWhere(
          textinput =>
            textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.CHANGE_NAME_FORM__NEW_NAME_INPUT_PLACEHOLDER
            )
        )
        .first()
        .prop('value')
    ).toBe('');
  });
  it('should clear password form after clicking X button', async () => {
    const wrapper = mount(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(
          i18n.t(TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_EMAIL)
        )
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_PASSWORD_FORM__OLD_PASSWORD_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('someText');
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('TextInput')
      .filterWhere(
        textinput =>
          textinput.prop('placeholder') ===
          i18n.t(
            TranslationsKeys.CHANGE_PASSWORD_FORM__NEW_PASSWORD_INPUT_PLACEHOLDER
          )
      )
      .first()
      .invoke<any>('onChangeText')('someText');
    await wait(0);
    wrapper.update();
    wrapper
      .find('ChangeUserDetailsForm')
      .filterWhere(
        form =>
          form.prop('submitButtonText') ===
          i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
      )
      .find('IconButton')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
        )
        .find('TextInput')
        .filterWhere(
          textinput =>
            textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.CHANGE_PASSWORD_FORM__OLD_PASSWORD_INPUT_PLACEHOLDER
            )
        )
        .first()
        .prop('value')
    ).toBe('');
    expect(
      wrapper
        .find('ChangeUserDetailsForm')
        .filterWhere(
          form =>
            form.prop('submitButtonText') ===
            i18n.t(TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD)
        )
        .find('TextInput')
        .filterWhere(
          textinput =>
            textinput.prop('placeholder') ===
            i18n.t(
              TranslationsKeys.CHANGE_PASSWORD_FORM__NEW_PASSWORD_INPUT_PLACEHOLDER
            )
        )
        .first()
        .prop('value')
    ).toBe('');
  });
  it('should call goBack method after clicking on <TouchableOpacity /> with "Return" <Text />', async () => {
    const wrapper = shallow(<SettingsScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .filterWhere(btn =>
        btn.contains(i18n.t(TranslationsKeys.SETTINGS_SCREEN__RETURN))
      )
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(mockGoBack).toBeCalledTimes(1);
  });
});
