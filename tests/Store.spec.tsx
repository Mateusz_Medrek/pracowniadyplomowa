import AsyncStorage from '@react-native-community/async-storage';
import { init } from '@rematch/core';
import moxios from 'moxios';

import { ADD_FRIEND, REMOVE_FRIEND } from '../src/api/routes';
import server, { baseURL } from '../src/api/server';
import { models } from '../src/store/models';
import { IUser, TokenProviders } from '../src/store';

const { locale, token, user } = models;

describe('models', () => {
  afterEach(() => {
    AsyncStorage.clear();
  });
  describe('locale', () => {
    it('should have initial state of empty string', () => {
      const store = init({ models: { locale } });
      expect(store.getState().locale).toBe('');
    });
    it('should have new state after calling changeLocale reducer with new locale passed', () => {
      const store = init({ models: { locale } });
      const newLocale = 'pl-PL';
      store.dispatch.locale.changeLocale(newLocale);
      expect(store.getState().locale).toBe(newLocale);
    });
    it('should have old state after calling changeLocale reducer with old locale passed', () => {
      const store = init({ models: { locale } });
      const oldLocale = 'pl-PL';
      store.dispatch.locale.changeLocale(oldLocale);
      store.dispatch.locale.changeLocale(oldLocale);
      expect(store.getState().locale).toBe('pl-PL');
    });
  });
  describe('token', () => {
    it('should have initial state of isLoading set to true', () => {
      const store = init({ models: { token } });
      expect(store.getState().token.isLoading).toBe(true);
      expect(store.getState().token.token).toBe('');
      expect(store.getState().token.tokenType).toBe(null);
    });
    it('should have new token state after calling addToken effect', async () => {
      const store = init({ models: { token } });
      const tokenValue = 'token';
      const tokenType = TokenProviders.server;
      await store.dispatch.token.addToken({ tokenValue, tokenType });
      const newState = store.getState().token;
      expect(newState.isLoading).toBe(false);
      expect(newState.token).toBe(tokenValue);
      expect(newState.tokenType).toBe(tokenType);
    });
    it('should have new token state after calling getToken effect', async () => {
      const store = init({ models: { token } });
      const tokenValue = 'token';
      const tokenType = TokenProviders.server;
      await AsyncStorage.setItem('token', tokenValue);
      await AsyncStorage.setItem('tokenType', tokenType);
      await store.dispatch.token.getToken();
      const newState = store.getState().token;
      expect(newState.isLoading).toBe(false);
      expect(newState.token).toBe(tokenValue);
      expect(newState.tokenType).toBe(tokenType);
    });
    it('should have new token state after calling removeToken effect and should have user state of null', async () => {
      const store = init({ models: { token, user } });
      const tokenValue = 'token';
      const tokenType = TokenProviders.server;
      const newUser: IUser = {
        id: '012345',
        name: 'Name',
        email: 'email@gmail.com',
        accountType: TokenProviders.server,
        tasksIds: [],
        friends: [],
      };
      store.dispatch.user.saveUser(newUser);
      await store.dispatch.token.addToken({ tokenValue, tokenType });
      await store.dispatch.token.removeToken();
      const newState = store.getState().token;
      expect(newState.isLoading).toBe(false);
      expect(newState.token).toBe('');
      expect(newState.tokenType).toBe(null);
      expect(store.getState().user).toBe(null);
    });
  });
  describe('user', () => {
    it('should have initial state of null', () => {
      const store = init({ models: { user } });
      expect(store.getState().user).toBe(null);
    });
    it('should have new user state of updated user after calling addFriend effect', async () => {
      moxios.install(server as any);
      moxios.stubRequest(`${baseURL}/${ADD_FRIEND}`, { status: 200 });
      const store = init({ models: { user } });
      const newFriend = { id: '033333', name: 'newFriend' };
      const friends = [{ id: '054321', name: 'Friend' }];
      const newUser: IUser = {
        id: '012345',
        name: 'Name',
        email: 'email@gmail.com',
        accountType: TokenProviders.server,
        tasksIds: [],
        friends: [...friends],
      };
      store.dispatch.user.saveUser(newUser);
      await store.dispatch.user.addFriend(newFriend, '');
      expect(store.getState().user?.friends).toHaveLength(friends.length + 1);
      moxios.uninstall(server as any);
    });
    it('should have new user state of null after calling deleteUser reducer', async () => {
      const store = init({ models: { user } });
      const newUser: IUser = {
        id: '012345',
        name: 'Name',
        email: 'email@gmail.com',
        accountType: TokenProviders.server,
        tasksIds: [],
        friends: [],
      };
      store.dispatch.user.saveUser(newUser);
      store.dispatch.user.deleteUser(newUser);
      expect(store.getState().user).toBe(null);
    });
    it('should have new user state of updated user after calling removeFriend effect', async () => {
      moxios.install(server as any);
      moxios.stubRequest(`${baseURL}/${REMOVE_FRIEND}`, { status: 200 });
      const store = init({ models: { user } });
      const oldFriend = { id: '033333', name: 'newFriend' };
      const friends = [oldFriend];
      const newUser: IUser = {
        id: '012345',
        name: 'Name',
        email: 'email@gmail.com',
        accountType: TokenProviders.server,
        tasksIds: [],
        friends: [...friends],
      };
      store.dispatch.user.saveUser(newUser);
      await store.dispatch.user.removeFriend(oldFriend, '');
      expect(store.getState().user?.friends).toHaveLength(
        newUser.friends.length - 1
      );
      moxios.uninstall(server as any);
    });
    it('should have new user state of user after calling saveUser reducer', async () => {
      const store = init({ models: { user } });
      const newUser: IUser = {
        id: '012345',
        name: 'Name',
        email: 'email@gmail.com',
        accountType: TokenProviders.server,
        tasksIds: [],
        friends: [],
      };
      store.dispatch.user.saveUser(newUser);
      expect(store.getState().user).toStrictEqual(newUser);
    });
  });
});
