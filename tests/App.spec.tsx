import './mocks/googleauth';
import './mocks/rnScreens';

import { GoogleSignin } from '@react-native-community/google-signin';
import { useNetInfo } from '@react-native-community/netinfo';
import { mount } from 'enzyme';
import React from 'react';
import * as RNLocalize from 'react-native-localize';
import { useSelector } from 'react-redux';
import wait from 'waait';

import App from '../src/App';

jest.mock('@react-native-community/netinfo', () => ({
  useNetInfo: jest.fn(() => ({
    isConnected: false,
    isInternetReachable: false,
  })),
}));

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');

const mockChangeLocale = jest.fn();
const mockGetToken = jest.fn();
const mockState = {
  locale: 'en-GB',
  token: { isLoading: false },
};
jest.mock('react-redux', () => ({
  useDispatch: jest.fn(() => ({
    locale: {
      changeLocale: mockChangeLocale,
    },
    token: {
      getToken: mockGetToken,
    },
  })),
  useSelector: jest.fn((callback: (state: Record<string, any>) => any) =>
    callback(mockState)
  ),
}));

jest.mock('../src/hooks/useCurrentUser', () => {
  return () => [jest.fn(() => Promise.resolve())];
});

// Mock navigators to prevent displaying all components inside them when using enzyme's mount;
jest.mock('../src/navigation/AuthNavigator', () => 'AuthNavigator');
jest.mock('../src/navigation/MainNavigator', () => 'MainNavigator');

describe('App', () => {
  beforeEach(() => {
    (GoogleSignin.configure as any).mockClear();
    (RNLocalize.addEventListener as any).mockClear();
    (RNLocalize.findBestAvailableLanguage as any).mockClear();
    (RNLocalize.removeEventListener as any).mockClear();
  });
  it('should call getToken, GoogleSignin.configure, RNLocalize.addEventListener, RNLocalize.findBestAvailableLanguage methods on mount, should call RNLocalize.removeEventListener on unmount', async () => {
    const wrapper = mount(<App />);
    await wait(0);
    expect(mockGetToken).toBeCalledTimes(1);
    expect(GoogleSignin.configure).toBeCalledTimes(1);
    expect(RNLocalize.addEventListener).toBeCalledTimes(1);
    expect(RNLocalize.findBestAvailableLanguage).toBeCalledTimes(1);
    wrapper.unmount();
    expect(RNLocalize.removeEventListener).toBeCalledTimes(1);
  });
  it('should not render <AuthNavigator /> or <MainNavigator /> when TokenState.isLoading equals true', async () => {
    (useSelector as any).mockImplementation(
      (callback: (state: Record<string, any>) => any) =>
        callback({
          locale: 'en-GB',
          token: { isLoading: true },
        })
    );
    const wrapper = mount(<App />);
    expect(wrapper.find('AuthNavigator')).toHaveLength(0);
    expect(wrapper.find('MainNavigator')).toHaveLength(0);
    wrapper.unmount();
    (useSelector as any).mockRestore();
    (useSelector as any).mockImplementation(
      (callback: (state: Record<string, any>) => any) => callback(mockState)
    );
  });
  it('should render AuthNavigator when there is no token', async () => {
    const wrapper = mount(<App />);
    await wait(0);
    expect(wrapper.find('AuthNavigator')).toHaveLength(1);
    wrapper.unmount();
  });
  it('should render MainNavigator when there is token', async () => {
    (useSelector as any).mockImplementation(
      (callback: (state: Record<string, any>) => any) =>
        callback({
          locale: 'en-GB',
          token: {
            isLoading: false,
            token: 'IAMTOKEN',
            tokenType: '0',
          },
        })
    );
    const wrapper = mount(<App />);
    await wait(0);
    expect(wrapper.find('MainNavigator')).toHaveLength(1);
    wrapper.unmount();
    (useSelector as any).mockRestore();
    (useSelector as any).mockImplementation(
      (callback: (state: Record<string, any>) => any) => callback(mockState)
    );
  });
  it('should not show <NetInfoWarning /> when device is connected to internet', async () => {
    (useNetInfo as any) = jest.fn(() => ({
      isConnected: true,
      isInternetReachable: true,
    }));
    const wrapper = mount(<App />);
    await wait(0);
    expect(wrapper.find('NetInfoWarning')).toHaveLength(0);
    wrapper.unmount();
  });
  it('should show <NetInfoWarning /> when device is not connected to internet', async () => {
    (useNetInfo as any) = jest.fn(() => ({
      isConnected: false,
      isInternetReachable: false,
    }));
    const wrapper = mount(<App />);
    await wait(0);
    expect(wrapper.find('NetInfoWarning')).toHaveLength(1);
    wrapper.unmount();
  });
  it('should show <NetInfoWarning /> when internet is no reachable', async () => {
    (useNetInfo as any) = jest.fn(() => ({
      isConnected: true,
      isInternetReachable: false,
    }));
    const wrapper = mount(<App />);
    await wait(0);
    expect(wrapper.find('NetInfoWarning')).toHaveLength(1);
    wrapper.unmount();
  });
});
