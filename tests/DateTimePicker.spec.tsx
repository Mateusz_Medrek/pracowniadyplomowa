import './mocks/rncDateTimePicker';
import './mocks/rnGestureHandler';
import './mocks/rnVectorIcons';

import { shallow } from 'enzyme';
import i18n from 'i18n-js';
import React from 'react';
import wait from 'waait';

import DateTimePicker from '../src/components/DateTimePicker';
import { TranslationsKeys } from '../translations/keys';

describe('DateTimePicker', () => {
  it('should display <RNCDateTimePicker /> after pressing button', async () => {
    const wrapper = shallow(
      <DateTimePicker dateTime={new Date()} setDateTime={jest.fn()} />
    );
    wrapper
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    expect(wrapper.find('RNCDateTimePicker')).toHaveLength(1);
  });
  it('should have "Set date" <Text /> by default', () => {
    const wrapper = shallow(
      <DateTimePicker dateTime={new Date()} setDateTime={jest.fn()} />
    );
    expect(
      wrapper.contains(i18n.t(TranslationsKeys.DATE_TIME_PICKER__SET_DATE))
    ).toBe(true);
  });
  it('should have title prop as <Text />', () => {
    const title = 'Ustaw datę rozpoczęcia';
    const wrapper = shallow(
      <DateTimePicker
        dateTime={new Date()}
        title={title}
        setDateTime={jest.fn()}
      />
    );
    expect(wrapper.contains(title)).toBe(true);
  });
});
