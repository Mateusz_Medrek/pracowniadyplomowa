import './mocks/rnGestureHandler';
import './mocks/rnVectorIcons';

import { useNavigation } from '@react-navigation/native';
import { mount, shallow } from 'enzyme';
import i18n from 'i18n-js';
import moxios from 'moxios';
import React from 'react';
import wait from 'waait';

import { GET_ALL_COMPLETED_TASKS, UNCHECK_TASK } from '../src/api/routes';
import server from '../src/api/server';
import CompletedTasksScreen, {
  FlatListEmptyComponent,
  FlatListHeaderComponent,
} from '../src/screens/CompletedTasksScreen';
import { TokenProviders } from '../src/store';
import { TranslationsKeys } from '../translations/keys';

const mockGoBack = jest.fn();
const mockNavigate = jest.fn();
jest.mock('@react-navigation/native', () => ({
  useNavigation: jest.fn(() => ({
    goBack: mockGoBack,
    navigate: mockNavigate,
  })),
}));

const mockTokenType = TokenProviders.server;
const mockState = {
  locale: 'en-GB',
  token: {
    isLoading: false,
    token: 'IAmValidToken',
    tokenType: mockTokenType,
  },
};
jest.mock('react-redux', () => ({
  useSelector: jest.fn((callback: (state: Record<string, any>) => any) =>
    callback(mockState)
  ),
}));

const tasks = [{ id: '1234', title: 'ABC' }];

describe('CompletedTasksScreen', () => {
  beforeEach(() => {
    moxios.install(server as any);
    moxios.stubRequest(
      `https://dry-castle-75947.herokuapp.com/${GET_ALL_COMPLETED_TASKS}`,
      { response: { tasks } }
    );
    moxios.stubRequest(
      `https://dry-castle-75947.herokuapp.com/${UNCHECK_TASK}`,
      { status: 200 }
    );
    (useNavigation as any).mockClear();
  });
  afterEach(() => {
    moxios.uninstall(server as any);
  });
  it('should have <FlatList />', async () => {
    const wrapper = mount(<CompletedTasksScreen />);
    await wait(0);
    wrapper.update();
    expect(wrapper.find('FlatList')).toHaveLength(1);
    wrapper.unmount();
  });
  it('should have <TouchableOpacity /> with "Return" <Text />', async () => {
    const wrapper = mount(<CompletedTasksScreen />);
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('TouchableOpacity')
        .filterWhere(btn =>
          btn.contains(i18n.t(TranslationsKeys.COMPLETED_TASKS_SCREEN__RETURN))
        )
    ).toHaveLength(1);
    wrapper.unmount();
  });
  it('should call goBack method after clicking "Return" <TouchableOpacity />', async () => {
    const wrapper = mount(<CompletedTasksScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('TouchableOpacity')
      .last()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(mockGoBack).toBeCalledTimes(1);
    wrapper.unmount();
  });
  it('should delete item from <FlatList /> when user taps on item\'s "Bring task back" <TouchableOpacity />', async () => {
    const wrapper = mount(<CompletedTasksScreen />);
    await wait(0);
    wrapper.update();
    wrapper
      .find('FlatList')
      .first()
      .find('CellRenderer')
      .first()
      .find('TouchableOpacity')
      .first()
      .invoke<any>('onPress')();
    await wait(0);
    wrapper.update();
    expect(
      wrapper
        .find('FlatList')
        .first()
        .find('CellRenderer')
    ).toHaveLength(tasks.length - 1);
    wrapper.unmount();
  });
});

describe('FlatListEmptyComponent', () => {
  it('should have "No tasks" <Text />', () => {
    const wrapper = shallow(<FlatListEmptyComponent />);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.COMPLETED_TASKS_SCREEN__NO_TASKS)
      )
    ).toBe(true);
  });
});

describe('FlatListHeaderComponent', () => {
  it('should have "Completed tasks" <Text />', async () => {
    const wrapper = shallow(<FlatListHeaderComponent />);
    expect(
      wrapper.contains(
        i18n.t(TranslationsKeys.COMPLETED_TASKS_SCREEN__COMPLETED_TASKS)
      )
    ).toBe(true);
  });
});
