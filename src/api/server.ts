import axios from 'axios';

export const baseURL = 'https://dry-castle-75947.herokuapp.com';

export default axios.create({ baseURL });
