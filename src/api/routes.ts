/**
 * Auth related routes
 */

export const SIGN_IN = 'auth/signin';
export const SIGN_UP_EMAIL = 'auth/signupemail';
export const SIGN_UP_GOOGLE = 'auth/signupgoogle';

/**
 * Task related routes
 */

export const CHECK_TASK = 'task/check';
export const CREATE_TASK = 'task/create';
export const DELETE_TASK = 'task/delete';
export const EDIT_TASK = 'task/edit';
export const GET_ALL_COMPLETED_TASKS = 'task/getallcompleted';
export const GET_ALL_TASKS = 'task/getall';
export const GET_ALL_TODAY_TASKS = 'task/getalltoday';
export const GET_TASK = 'task/get';
export const UNCHECK_TASK = 'task/uncheck';
export const CHECK_SUBTASK = 'subtask/check';
export const UNCHECK_SUBTASK = 'subtask/uncheck';

/**
 * User related routes
 */

export const CHANGE_EMAIL = 'user/emailchange';
export const CHANGE_NAME = 'user/namechange';
export const CHANGE_PASSWORD = 'user/passwordchange';
export const GET_CURRENT_USER = 'user/current';
export const GET_USER = 'user/get';
export const GET_USER_BY_NAME = 'user/getbyname';
export const ADD_FRIEND = 'friend/add';
export const REMOVE_FRIEND = 'friend/remove';
