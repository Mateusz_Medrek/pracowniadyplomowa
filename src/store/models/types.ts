// LOCALE STATE;
export type LocaleState = string;

// TOKEN STATE;
export enum TokenProviders {
  google = '2',
  server = '0',
}

export interface TokenState {
  isLoading: boolean;
  token: string;
  tokenType: TokenProviders | null;
}
//

// USER STATE;
export interface IFriend {
  id: string;
  name: string;
}

export interface IUser {
  id: string;
  name: string;
  accountType: TokenProviders;
  email: string;
  tasksIds: string[];
  friends: IFriend[];
}

export type UserState = IUser | null;
//

// OTHER TYPES;
export interface ISubTask {
  assigneeId: string;
  category: number;
  id: number;
  isCompleted?: boolean;
  title: string;
}

export interface ITaskTitle {
  id: string;
  title: string;
}

export interface ITask {
  title: string;
  description: string;
  startingTime: Date;
  finishingTime: Date;
  subTasks: ISubTask[];
  isCompleted?: boolean;
}
//
