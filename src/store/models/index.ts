import AsyncStorage from '@react-native-community/async-storage';
import { Dispatch } from '../index';

import { ADD_FRIEND, REMOVE_FRIEND } from '../../api/routes';
import server from '../../api/server';
import {
  IFriend,
  IUser,
  LocaleState,
  TokenProviders,
  TokenState,
  UserState,
} from './types';

// LOCALE STATE;
const INITIAL_LOCALE_STATE: LocaleState = '';

const locale = {
  state: INITIAL_LOCALE_STATE,
  reducers: {
    changeLocale: (state: LocaleState, newLocale: string) => {
      if (state != newLocale) {
        return newLocale;
      }
      return state;
    },
  },
};

// TOKEN STATE;
const INITIAL_TOKEN_STATE: TokenState = {
  isLoading: true,
  token: '',
  tokenType: null,
};

const token = {
  state: INITIAL_TOKEN_STATE,
  reducers: {
    addOrGetTokenReducer: (
      _: TokenState,
      payload: { tokenType: TokenProviders | null; tokenValue: string }
    ): TokenState => ({
      isLoading: false,
      token: payload.tokenValue,
      tokenType: payload.tokenType,
    }),
    removeTokenReducer: (): TokenState => ({
      isLoading: false,
      token: '',
      tokenType: null,
    }),
  },
  effects: (dispatch: Dispatch) => ({
    async addToken(payload: { tokenValue: string; tokenType: TokenProviders }) {
      try {
        await AsyncStorage.setItem('token', payload.tokenValue);
        await AsyncStorage.setItem('tokenType', payload.tokenType);
        dispatch.token.addOrGetTokenReducer(payload);
      } catch (err) {
        console.log(err.message);
      }
    },
    async getToken() {
      try {
        const tokenValue = (await AsyncStorage.getItem('token')) || '';
        const tokenType =
          ((await AsyncStorage.getItem('tokenType')) as TokenProviders) || null;
        dispatch.token.addOrGetTokenReducer({ tokenValue, tokenType });
      } catch (err) {
        console.log(err.message);
      }
    },
    async removeToken() {
      try {
        await AsyncStorage.removeItem('token');
        await AsyncStorage.removeItem('tokenType');
        dispatch.token.removeTokenReducer();
        dispatch.user.deleteUser();
      } catch (err) {
        console.log(err.message);
      }
    },
  }),
};
//

// USER STATE;
const INITIAL_USER_STATE: UserState = null;

const user = {
  state: INITIAL_USER_STATE as UserState,
  reducers: {
    addFriendReducer: (state: UserState, payload: IFriend): UserState => {
      const user = state;
      if (!user) {
        return null;
      }
      user.friends.push(payload);
      return { ...user };
    },
    deleteUser: (): UserState => null,
    removeFriendReducer: (state: UserState, payload: IFriend): UserState => {
      const user = state;
      if (!user) {
        return null;
      }
      user.friends = user.friends.filter(friend => friend.id !== payload.id);
      return { ...user };
    },
    saveUser: (_: UserState, payload: IUser): UserState => ({ ...payload }),
  },
  effects: (dispatch: Dispatch) => ({
    async addFriend({ id, name }: IFriend, authorization: string) {
      try {
        const response = await server.post(
          `/${ADD_FRIEND}`,
          { id },
          { headers: { authorization } }
        );
        if (response.status === 200) {
          dispatch.user.addFriendReducer({ id, name });
        } else {
          throw new Error('Adding friend failure');
        }
      } catch (err) {
        console.log(err.message);
      }
    },
    async removeFriend({ id, name }: IFriend, authorization: string) {
      try {
        const response = await server.post(
          `/${REMOVE_FRIEND}`,
          { id },
          { headers: { authorization } }
        );
        if (response.status === 200) {
          dispatch.user.removeFriendReducer({ id, name });
        } else {
          throw new Error('Deleting friend failure');
        }
      } catch (err) {
        console.log(err.message);
      }
    },
  }),
};
//

export interface RootModel {
  locale: typeof locale;
  token: typeof token;
  user: typeof user;
}

export const models: RootModel = {
  locale,
  token,
  user,
};
