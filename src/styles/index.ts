import { ImageStyle, StyleSheet, TextStyle, ViewStyle } from 'react-native';

export const COLORS = {
  /** #000000 */
  BLACK: '#000000',
  /** #1C304A */
  DARKBLUE: '#1C304A',
  /** #45474C */
  DARKGRAY: '#45474C',
  /** #4267B2 */
  FACEBOOKICON: '#4267B2',
  /** #8A8F99 */
  GRAY: '#8A8F99',
  /** #00FF00 */
  GREEN: '#00FF00',
  /** #FF008A */
  HEART: '#FF008A',
  /** #16AEF2 */
  LIGHTBLUE: '#16AEF2',
  /** #C4CAD9 */
  LIGHTGRAY: '#C4CAD9',
  /** #1676BB */
  MEDIUMBLUE: '#1676BB',
  /** #165699 */
  MEDIUMDARKBLUE: '#165699',
  /** #2E89E9 */
  MEDIUMLIGHTBLUE: '#2E89E9',
  /** #FF0000 */
  RED: '#FF0000',
  /** #FFFFFF */
  WHITE: '#FFFFFF',
};

export const CONSTS = {
  /** 18 */
  FONT_SIZE: 18,
  /** 24 */
  LARGE_FONT_SIZE: 24,
  /** 10 */
  TILE_BORDERRADIUS: 10,
};

type ElevationStyle = Pick<
  ViewStyle,
  | 'elevation'
  | 'shadowColor'
  | 'shadowOffset'
  | 'shadowOpacity'
  | 'shadowRadius'
>;

/**
 * Cross platform function which applies elevation style to both Android & IOS
 *
 * @param {number} elevation elevation value
 */
export const withShadow = (elevation = 10): ElevationStyle => {
  return {
    elevation,
    shadowColor: COLORS.BLACK,
    shadowOffset: { width: 0, height: elevation - 1 },
    shadowOpacity: 0.24,
    shadowRadius: elevation,
  };
};

// IMAGE STYLES;
const _FLAG_IMAGE = {
  height: 16,
  width: 32,
};
type FlagImageStyle = Pick<ImageStyle, keyof typeof _FLAG_IMAGE>;
//

// TEXT STYLES;
const _BASE_INPUT = {
  borderBottomColor: COLORS.WHITE,
  borderBottomWidth: 2,
  color: COLORS.WHITE,
  fontSize: CONSTS.FONT_SIZE,
  marginHorizontal: 10,
  marginVertical: 5,
  opacity: 0.6,
  paddingHorizontal: 5,
  paddingVertical: 0,
};
type BaseInputStyle = Pick<TextStyle, keyof typeof _BASE_INPUT>;

const _BASE_TEXT = {
  color: COLORS.WHITE,
  fontSize: CONSTS.FONT_SIZE,
  textAlign: 'center',
  textAlignVertical: 'center',
};
type BaseTextStyle = Pick<TextStyle, keyof typeof _BASE_TEXT>;

const _ERROR_TEXT = {
  color: COLORS.RED,
  fontSize: CONSTS.FONT_SIZE,
  margin: 5,
  textAlign: 'center',
};
type ErrorText = Pick<TextStyle, keyof typeof _ERROR_TEXT>;

const _ICON_RESET = {
  borderRadius: 0,
  marginRight: 0,
};
type IconResetStyle = Pick<TextStyle, keyof typeof _ICON_RESET>;

const _SAVE_BUTTON_TEXT = {
  paddingHorizontal: 10,
};
type SaveButtonTextStyle = Pick<TextStyle, keyof typeof _SAVE_BUTTON_TEXT>;
//

// VIEW STYLES;
//     BUTTONS:
const _BASE_BUTTON = {
  alignItems: 'center',
  backgroundColor: COLORS.DARKBLUE,
  flexDirection: 'row',
  justifyContent: 'center',
  marginHorizontal: 10,
  marginVertical: 5,
  padding: 5,
  ...withShadow(),
};
type BaseButtonStyle = Pick<ViewStyle, keyof typeof _BASE_BUTTON>;

const _FLAG_BUTTON = {
  marginHorizontal: 30,
  marginVertical: 10,
  padding: 10,
};
type FlagButtonStyle = Pick<ViewStyle, keyof typeof _FLAG_BUTTON>;

const _LIGHTBLUE_BUTTON = {
  backgroundColor: COLORS.LIGHTBLUE,
};
type LightBlueButtonStyle = Pick<ViewStyle, keyof typeof _LIGHTBLUE_BUTTON>;

const _MEDIUMBLUE_BUTTON = {
  backgroundColor: COLORS.MEDIUMBLUE,
};
type MediumBlueButtonStyle = Pick<ViewStyle, keyof typeof _MEDIUMBLUE_BUTTON>;
//

//     CONTAINERS:
const _BASE_CONTAINER = {
  alignItems: 'center',
  flex: 1,
  justifyContent: 'center',
};
type BaseContainerStyle = Pick<ViewStyle, keyof typeof _BASE_CONTAINER>;

const _FLEX_ONE_CONTAINER = {
  flex: 1,
};
type FlexOneContainer = Pick<ViewStyle, keyof typeof _FLEX_ONE_CONTAINER>;

const _MODAL_CONTAINER = {
  alignItems: 'center',
  backgroundColor: 'transparent',
  flex: 1,
  justifyContent: 'center',
  margin: 10,
  padding: 10,
  zIndex: 15,
};
type ModalContainerStyle = Pick<ViewStyle, keyof typeof _MODAL_CONTAINER>;

const _SCREEN_CONTAINER = {
  alignItems: 'center',
  backgroundColor: COLORS.MEDIUMDARKBLUE,
  flex: 1,
};
type ScreenContainerStyle = Pick<ViewStyle, keyof typeof _SCREEN_CONTAINER>;
//

//     FLATLISTS:
const _BASE_FLATLIST = {
  backgroundColor: COLORS.MEDIUMBLUE,
  flex: 1,
  margin: 10,
  paddingVertical: 5,
  ...withShadow(),
};
type BaseFlatListStyle = Pick<ViewStyle, keyof typeof _BASE_FLATLIST>;
//

//     ROWS:
const _ROW_CENTER = {
  alignItems: 'center',
  flexDirection: 'row',
  justifyContent: 'center',
};
type RowCenterStyle = Pick<ViewStyle, keyof typeof _ROW_CENTER>;

const _ROW_CENTER_FLEX_START = {
  alignItems: 'center',
  flexDirection: 'row',
  justifyContent: 'flex-start',
};
type RowCenterFlexStartStyle = Pick<
  ViewStyle,
  keyof typeof _ROW_CENTER_FLEX_START
>;

const _ROW_CENTER_SPACE_BETWEEN = {
  alignItems: 'center',
  flexDirection: 'row',
  justifyContent: 'space-between',
};
type RowCenterSpaceBetweenStyle = Pick<
  ViewStyle,
  keyof typeof _ROW_CENTER_SPACE_BETWEEN
>;
//

//

export const STYLESHEET = StyleSheet.create({
  /**
   * its backgroundColor is COLORS.DARKBLUE;
   */
  baseButton: _BASE_BUTTON as BaseButtonStyle,
  baseContainer: _BASE_CONTAINER as BaseContainerStyle,
  baseFlatList: _BASE_FLATLIST as BaseFlatListStyle,
  baseInput: _BASE_INPUT as BaseInputStyle,
  baseText: _BASE_TEXT as BaseTextStyle,
  errorText: _ERROR_TEXT as ErrorText,
  flagButton: _FLAG_BUTTON as FlagButtonStyle,
  flagImage: _FLAG_IMAGE as FlagImageStyle,
  flexOneContainer: _FLEX_ONE_CONTAINER as FlexOneContainer,
  iconReset: _ICON_RESET as IconResetStyle,
  lightBlueButton: _LIGHTBLUE_BUTTON as LightBlueButtonStyle,
  mediumBlueButton: _MEDIUMBLUE_BUTTON as MediumBlueButtonStyle,
  modalContainer: _MODAL_CONTAINER as ModalContainerStyle,
  /**
   * alignItems = 'center';
   *
   * justifyContent = 'center';
   */
  rowCenter: _ROW_CENTER as RowCenterStyle,
  /**
   * alignItems = 'center';
   *
   * justifyContent = 'flex-start';
   */
  rowCenterFlexStart: _ROW_CENTER_FLEX_START as RowCenterFlexStartStyle,
  /**
   * alignItems = 'center';
   *
   * justifyContent = 'space-between';
   */
  rowCenterSpaceBetween: _ROW_CENTER_SPACE_BETWEEN as RowCenterSpaceBetweenStyle,
  saveButtonText: _SAVE_BUTTON_TEXT as SaveButtonTextStyle,
  screenContainer: _SCREEN_CONTAINER as ScreenContainerStyle,
});
