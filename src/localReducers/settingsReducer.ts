import i18n from 'i18n-js';

import { TranslationsKeys } from '../../translations/keys';

type EMAIL_CHANGE_COMPLETED = { type: 'EMAIL_CHANGE_COMPLETED' };
type CLEAR_EMAIL_FORM = { type: 'CLEAR_EMAIL_FORM' };
type CLEAR_NAME_FORM = { type: 'CLEAR_NAME_FORM' };
type CLEAR_PASSWORD_FORM = { type: 'CLEAR_PASSWORD_FORM' };
type HIDE_MODAL = { type: 'HIDE_MODAL' };
type NAME_CHANGE_COMPLETED = { type: 'NAME_CHANGE_COMPLETED' };
type PASSWORD_CHANGE_COMPLETED = { type: 'PASSWORD_CHANGE_COMPLETED' };
type RESET = { type: 'RESET' };
type SET_NEW_EMAIL = { type: 'SET_NEW_EMAIL'; payload: string };
type SET_NEW_EMAIL_ERROR = { type: 'SET_NEW_EMAIL_ERROR'; payload: string };
type SET_NEW_NAME = { type: 'SET_NEW_NAME'; payload: string };
type SET_NEW_NAME_ERROR = { type: 'SET_NEW_NAME_ERROR'; payload: string };
type SET_NEW_PASSWORD = { type: 'SET_NEW_PASSWORD'; payload: string };
type SET_NEW_PASSWORD_ERROR = {
  type: 'SET_NEW_PASSWORD_ERROR';
  payload: string;
};
type SET_OLD_PASSWORD = { type: 'SET_OLD_PASSWORD'; payload: string };
type SET_PASSWORD = { type: 'SET_PASSWORD'; payload: string };
type SHOW_EMAIL_INDICATOR = { type: 'SHOW_EMAIL_INDICATOR' };
type SHOW_NAME_INDICATOR = { type: 'SHOW_NAME_INDICATOR' };
type SHOW_PASSWORD_INDICATOR = { type: 'SHOW_PASSWORD_INDICATOR' };

type ACTION =
  | EMAIL_CHANGE_COMPLETED
  | CLEAR_EMAIL_FORM
  | CLEAR_NAME_FORM
  | CLEAR_PASSWORD_FORM
  | HIDE_MODAL
  | NAME_CHANGE_COMPLETED
  | PASSWORD_CHANGE_COMPLETED
  | RESET
  | SET_NEW_EMAIL
  | SET_NEW_EMAIL_ERROR
  | SET_NEW_NAME
  | SET_NEW_NAME_ERROR
  | SET_NEW_PASSWORD
  | SET_NEW_PASSWORD_ERROR
  | SET_OLD_PASSWORD
  | SET_PASSWORD
  | SHOW_EMAIL_INDICATOR
  | SHOW_NAME_INDICATOR
  | SHOW_PASSWORD_INDICATOR;

export const INITIAL_STATE = {
  modalText: '',
  shouldShowModal: 0 as 0 | 1,
  // CHANGE EMAIL FORM
  newEmail: '',
  newEmailError: '',
  password: '',
  shouldShowEmailIndicator: false,
  // CHANGE NAME FORM
  newName: '',
  newNameError: '',
  shouldShowNameIndicator: false,
  // CHANGE PASSWORD FORM
  newPassword: '',
  newPasswordError: '',
  oldPassword: '',
  shouldShowPasswordIndicator: false,
};

const settingsReducer = (
  state: typeof INITIAL_STATE,
  action: ACTION
): typeof INITIAL_STATE => {
  switch (action.type) {
    case 'EMAIL_CHANGE_COMPLETED':
      return {
        ...state,
        newEmail: '',
        newEmailError: '',
        modalText: i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__MODAL_MESSAGE),
        password: '',
        shouldShowEmailIndicator: false,
        shouldShowModal: 1,
      };
    case 'CLEAR_EMAIL_FORM':
      return {
        ...state,
        newEmail: '',
        newEmailError: '',
        password: '',
      };
    case 'CLEAR_NAME_FORM':
      return {
        ...state,
        newName: '',
        newNameError: '',
      };
    case 'CLEAR_PASSWORD_FORM':
      return {
        ...state,
        newPassword: '',
        newPasswordError: '',
        oldPassword: '',
      };
    case 'HIDE_MODAL':
      return { ...state, shouldShowModal: 0 };
    case 'NAME_CHANGE_COMPLETED':
      return {
        ...state,
        newName: '',
        newNameError: '',
        modalText: i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__MODAL_MESSAGE),
        shouldShowModal: 1,
        shouldShowNameIndicator: false,
      };
    case 'PASSWORD_CHANGE_COMPLETED':
      return {
        ...state,
        newPassword: '',
        newPasswordError: '',
        modalText: i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__MODAL_MESSAGE),
        oldPassword: '',
        shouldShowModal: 1,
        shouldShowPasswordIndicator: false,
      };
    case 'RESET':
      return { ...INITIAL_STATE };
    case 'SET_NEW_EMAIL':
      return { ...state, newEmail: action.payload };
    case 'SET_NEW_EMAIL_ERROR':
      console.log(action.payload);
      return {
        ...state,
        newEmailError: i18n.t(action.payload),
        shouldShowEmailIndicator: false,
      };
    case 'SET_NEW_NAME':
      return { ...state, newName: action.payload };
    case 'SET_NEW_NAME_ERROR':
      console.log(action.payload);
      return {
        ...state,
        newNameError: i18n.t(action.payload),
        shouldShowNameIndicator: false,
      };
    case 'SET_NEW_PASSWORD':
      return { ...state, newPassword: action.payload };
    case 'SET_NEW_PASSWORD_ERROR':
      console.log(action.payload);
      return {
        ...state,
        newPasswordError: i18n.t(action.payload),
        shouldShowPasswordIndicator: false,
      };
    case 'SET_OLD_PASSWORD':
      return { ...state, oldPassword: action.payload };
    case 'SET_PASSWORD':
      return { ...state, password: action.payload };
    case 'SHOW_EMAIL_INDICATOR':
      return { ...state, shouldShowEmailIndicator: true };
    case 'SHOW_NAME_INDICATOR':
      return { ...state, shouldShowNameIndicator: true };
    case 'SHOW_PASSWORD_INDICATOR':
      return { ...state, shouldShowPasswordIndicator: true };
    default:
      return state;
  }
};

export default settingsReducer;
