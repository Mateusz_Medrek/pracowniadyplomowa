import i18n from 'i18n-js';

type FORM_AUTH_COMPLETED = { type: 'FORM_AUTH_COMPLETED' };
type FORM_AUTH_FAILED = { type: 'FORM_AUTH_FAILED'; payload: string };
type GOOGLE_AUTH_COMPLETED = { type: 'GOOGLE_AUTH_COMPLETED' };
type GOOGLE_AUTH_FAILED = { type: 'GOOGLE_AUTH_FAILED'; payload: string };
type RESET = { type: 'RESET' };
type SET_EMAIL = { type: 'SET_EMAIL'; payload: string };
type SET_EMAIL_ERROR = {
  type: 'SET_EMAIL_ERROR';
  payload: string;
};
type SET_PASSWORD = { type: 'SET_PASSWORD'; payload: string };
type SET_PASSWORD_ERROR = {
  type: 'SET_PASSWORD_ERROR';
  payload: string;
};
type SHOW_FORM_INDICATOR = { type: 'SHOW_FORM_INDICATOR' };
type SHOW_GOOGLE_INDICATOR = { type: 'SHOW_GOOGLE_INDICATOR' };

type ACTION =
  | FORM_AUTH_COMPLETED
  | FORM_AUTH_FAILED
  | GOOGLE_AUTH_COMPLETED
  | GOOGLE_AUTH_FAILED
  | RESET
  | SET_EMAIL
  | SET_EMAIL_ERROR
  | SET_PASSWORD
  | SET_PASSWORD_ERROR
  | SHOW_FORM_INDICATOR
  | SHOW_GOOGLE_INDICATOR;

export const INITIAL_STATE = {
  email: '',
  emailError: '',
  password: '',
  passwordError: '',
  shouldShowFormIndicator: false,
  shouldShowGoogleIndicator: false,
};

const authReducer = (
  state: typeof INITIAL_STATE,
  action: ACTION
): typeof INITIAL_STATE => {
  switch (action.type) {
    case 'FORM_AUTH_COMPLETED':
      return {
        ...state,
        emailError: '',
        passwordError: '',
        shouldShowFormIndicator: false,
      };
    case 'FORM_AUTH_FAILED':
      console.log(action.payload);
      return { ...state, shouldShowFormIndicator: false };
    case 'GOOGLE_AUTH_COMPLETED':
      return {
        ...state,
        emailError: '',
        passwordError: '',
        shouldShowGoogleIndicator: false,
      };
    case 'GOOGLE_AUTH_FAILED':
      console.log(action.payload);
      return { ...state, shouldShowGoogleIndicator: false };
    case 'RESET':
      return { ...INITIAL_STATE };
    case 'SET_EMAIL':
      return { ...state, email: action.payload };
    case 'SET_EMAIL_ERROR':
      return {
        ...state,
        emailError: i18n.t(action.payload),
        shouldShowFormIndicator: false,
      };
    case 'SET_PASSWORD':
      return { ...state, password: action.payload };
    case 'SET_PASSWORD_ERROR':
      return {
        ...state,
        passwordError: i18n.t(action.payload),
        shouldShowFormIndicator: false,
      };
    case 'SHOW_FORM_INDICATOR':
      return { ...state, shouldShowFormIndicator: true };
    case 'SHOW_GOOGLE_INDICATOR':
      return { ...state, shouldShowGoogleIndicator: true };
    default:
      return state;
  }
};

export default authReducer;
