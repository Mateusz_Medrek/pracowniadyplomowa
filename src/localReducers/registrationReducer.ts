import i18n from 'i18n-js';

type REGISTRATION_COMPLETED = { type: 'REGISTRATION_COMPLETED' };
type REGISTRATION_FAILED = { type: 'REGISTRATION_FAILED'; payload: string };
type RESET = { type: 'RESET' };
type SET_EMAIL = { type: 'SET_EMAIL'; payload: string };
type SET_EMAIL_ERROR = { type: 'SET_EMAIL_ERROR'; payload: string };
type SET_NAME = { type: 'SET_NAME'; payload: string };
type SET_NAME_ERROR = { type: 'SET_NAME_ERROR'; payload: string };
type SET_PASSWORD = { type: 'SET_PASSWORD'; payload: string };
type SET_PASSWORD_ERROR = { type: 'SET_PASSWORD_ERROR'; payload: string };
type SHOW_INDICATOR = { type: 'SHOW_INDICATOR' };

type ACTION =
  | REGISTRATION_COMPLETED
  | REGISTRATION_FAILED
  | RESET
  | SET_EMAIL
  | SET_EMAIL_ERROR
  | SET_NAME
  | SET_NAME_ERROR
  | SET_PASSWORD
  | SET_PASSWORD_ERROR
  | SHOW_INDICATOR;

export const INITIAL_STATE = {
  email: '',
  emailError: '',
  name: '',
  nameError: '',
  password: '',
  passwordError: '',
  shouldShowIndicator: false,
};

const registrationReducer = (
  state: typeof INITIAL_STATE,
  action: ACTION
): typeof INITIAL_STATE => {
  switch (action.type) {
    case 'REGISTRATION_COMPLETED':
      return {
        ...state,
        emailError: '',
        nameError: '',
        passwordError: '',
        shouldShowIndicator: false,
      };
    case 'REGISTRATION_FAILED':
      console.log(action.payload);
      return { ...state, shouldShowIndicator: false };
    case 'RESET':
      return { ...INITIAL_STATE };
    case 'SET_EMAIL':
      return { ...state, email: action.payload };
    case 'SET_EMAIL_ERROR':
      return {
        ...state,
        emailError: i18n.t(action.payload),
        shouldShowIndicator: false,
      };
    case 'SET_NAME':
      return { ...state, name: action.payload };
    case 'SET_NAME_ERROR':
      return {
        ...state,
        nameError: i18n.t(action.payload),
        shouldShowIndicator: false,
      };
    case 'SET_PASSWORD':
      return { ...state, password: action.payload };
    case 'SET_PASSWORD_ERROR':
      return {
        ...state,
        passwordError: i18n.t(action.payload),
        shouldShowIndicator: false,
      };
    case 'SHOW_INDICATOR':
      return { ...state, shouldShowIndicator: true };
    default:
      return state;
  }
};

export default registrationReducer;
