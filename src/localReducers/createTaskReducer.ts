import i18n from 'i18n-js';

import { ISubTask, ITask } from '../store';

type ADD_SUBTASK = {
  type: 'ADD_SUBTASK';
  payload: Omit<ISubTask, 'id' | 'isCompleted'>;
};
type CREATE_EDIT_TASK_COMPLETED = { type: 'CREATE_EDIT_TASK_COMPLETED' };
type CREATE_EDIT_TASK_FAILED = {
  type: 'CREATE_EDIT_TASK_FAILED';
  payload: string;
};
type EDIT_SUBTASK = { type: 'EDIT_SUBTASK'; payload: ISubTask };
type HIDE_MODAL = { type: 'HIDE_MODAL' };
type SET_FINISHING_DATETIME = { type: 'SET_FINISHING_DATETIME'; payload: Date };
type SET_STARTING_DATETIME = { type: 'SET_STARTING_DATETIME'; payload: Date };
type SET_TASK_DESCRIPTION = { type: 'SET_TASK_DESCRIPTION'; payload: string };
type SET_TASK_DESCRIPTION_ERROR = {
  type: 'SET_TASK_DESCRIPTION_ERROR';
  payload: string;
};
type SET_TASK_TITLE = { type: 'SET_TASK_TITLE'; payload: string };
type SET_TASK_TITLE_ERROR = { type: 'SET_TASK_TITLE_ERROR'; payload: string };
type SORT_SUBTASKS = { type: 'SORT_SUBTASKS'; payload: ISubTask[] };
type SHOW_INDICATOR = { type: 'SHOW_INDICATOR' };
type SHOW_MODAL = { type: 'SHOW_MODAL' };

type ACTION =
  | ADD_SUBTASK
  | CREATE_EDIT_TASK_COMPLETED
  | CREATE_EDIT_TASK_FAILED
  | EDIT_SUBTASK
  | HIDE_MODAL
  | SET_FINISHING_DATETIME
  | SET_STARTING_DATETIME
  | SORT_SUBTASKS
  | SET_TASK_DESCRIPTION
  | SET_TASK_DESCRIPTION_ERROR
  | SET_TASK_TITLE
  | SET_TASK_TITLE_ERROR
  | SHOW_INDICATOR
  | SHOW_MODAL;

type INITIAL_STATE = {
  finishingDatetime: Date;
  shouldShowIndicator: boolean;
  shouldShowModal: 0 | 1;
  startingDatetime: Date;
  subTasks: ISubTask[];
  taskDescription: string;
  taskDescriptionError: string;
  taskTitle: string;
  taskTitleError: string;
};

export const initState = (taskData?: ITask & { id: string }): INITIAL_STATE => {
  const commonState = {
    shouldShowIndicator: false,
    shouldShowModal: 0 as 0 | 1,
    taskDescriptionError: '',
    taskTitleError: '',
  };
  if (!taskData) {
    const newDate = new Date();
    newDate.setSeconds(0);
    return {
      ...commonState,
      finishingDatetime: newDate,
      startingDatetime: newDate,
      subTasks: [],
      taskDescription: '',
      taskTitle: '',
    };
  }
  return {
    ...commonState,
    finishingDatetime: new Date(taskData.finishingTime.valueOf()),
    startingDatetime: new Date(taskData.startingTime.valueOf()),
    subTasks: taskData.subTasks,
    taskDescription: taskData.description,
    taskTitle: taskData.title,
  };
};

const createTaskReducer = (
  state: INITIAL_STATE,
  action: ACTION
): INITIAL_STATE => {
  switch (action.type) {
    case 'ADD_SUBTASK':
      return {
        ...state,
        subTasks: [
          ...state.subTasks,
          { ...action.payload, id: state.subTasks.length },
        ],
      };
    case 'CREATE_EDIT_TASK_COMPLETED':
      const newDatetime = new Date();
      newDatetime.setSeconds(0);
      return {
        ...state,
        finishingDatetime: newDatetime,
        shouldShowIndicator: false,
        shouldShowModal: 1,
        startingDatetime: newDatetime,
        subTasks: [],
        taskDescription: '',
        taskDescriptionError: '',
        taskTitle: '',
        taskTitleError: '',
      };
    case 'CREATE_EDIT_TASK_FAILED':
      console.log(action.payload);
      return { ...state, shouldShowIndicator: false };
    case 'EDIT_SUBTASK':
      return {
        ...state,
        subTasks: state.subTasks.map(subTask =>
          subTask.id === action.payload.id ? action.payload : subTask
        ),
      };
    case 'HIDE_MODAL':
      return { ...state, shouldShowModal: 0 };
    case 'SET_FINISHING_DATETIME':
      return { ...state, finishingDatetime: action.payload };
    case 'SET_STARTING_DATETIME':
      return { ...state, startingDatetime: action.payload };
    case 'SET_TASK_DESCRIPTION':
      return { ...state, taskDescription: action.payload };
    case 'SET_TASK_DESCRIPTION_ERROR':
      return {
        ...state,
        shouldShowIndicator: false,
        taskDescriptionError: i18n.t(action.payload),
      };
    case 'SET_TASK_TITLE':
      return { ...state, taskTitle: action.payload };
    case 'SET_TASK_TITLE_ERROR':
      return {
        ...state,
        shouldShowIndicator: false,
        taskTitleError: i18n.t(action.payload),
      };
    case 'SORT_SUBTASKS':
      return { ...state, subTasks: action.payload };
    case 'SHOW_INDICATOR':
      return { ...state, shouldShowIndicator: true };
    default:
      return state;
  }
};

export default createTaskReducer;
