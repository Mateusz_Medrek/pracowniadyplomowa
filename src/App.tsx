import { useNetInfo } from '@react-native-community/netinfo';
import { GoogleSignin } from '@react-native-community/google-signin';
import { NavigationContainer } from '@react-navigation/native';
import i18n from 'i18n-js';
import React, { useEffect } from 'react';
import * as RNLocalize from 'react-native-localize';
import { enableScreens } from 'react-native-screens';
import { useDispatch, useSelector } from 'react-redux';

import en from '../translations/en';
import pl from '../translations/pl';
import NetInfoWarning from './components/NetInfoWarning';
import AuthNavigator from './navigation/AuthNavigator';
import MainNavigator from './navigation/MainNavigator';
import { Dispatch, RootState, TokenState } from './store';

const App = () => {
  const netInfo = useNetInfo();
  const locale = useSelector<RootState, string>(state => state.locale);
  const tokenState = useSelector<RootState, TokenState>(state => state.token);
  const dispatch = useDispatch<Dispatch>();
  /** Called everytime when device locale changes */
  const handleLocalizationChange = () => {
    const fallback = { languageTag: 'en-GB', isRTL: false };
    const bestLanguage =
      RNLocalize.findBestAvailableLanguage(Object.keys(i18n.translations)) ||
      fallback;
    i18n.locale = bestLanguage.languageTag;
    dispatch.locale.changeLocale(i18n.locale);
    return i18n.locale;
  };
  useEffect(() => {
    dispatch.token.getToken();
    GoogleSignin.configure({
      webClientId:
        '610207106437-skavj67biscbf23irha0p38lve2gdfjt.apps.googleusercontent.com',
    });
    i18n.translations = {
      'en-GB': en,
      'en-US': en,
      'pl-PL': pl,
    };
    handleLocalizationChange();
    RNLocalize.addEventListener('change', handleLocalizationChange);
    return () => {
      RNLocalize.removeEventListener('change', handleLocalizationChange);
    };
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  if (tokenState.isLoading || !locale) {
    return null;
  }
  enableScreens();
  return (
    <>
      {(!netInfo.isConnected || !netInfo.isInternetReachable) && (
        <NetInfoWarning />
      )}
      <NavigationContainer>
        {!tokenState.token || !tokenState.tokenType ? (
          <AuthNavigator />
        ) : (
          <MainNavigator />
        )}
      </NavigationContainer>
    </>
  );
};

export default App;
