import React from 'react';

import DrawerView from '../animations/DrawerView';
import CustomDrawerContent from './CustomDrawerContent';

export interface ReanimatedLayoutProps {
  drawerPosition: 'left' | 'right';
}

interface ReanimatedLayoutState {
  isDrawerOpen: boolean;
}

class ReanimatedDrawerLayout extends React.Component<
  ReanimatedLayoutProps,
  ReanimatedLayoutState
> {
  static defaultProps = {
    drawerPosition: 'left',
  };
  constructor(props: ReanimatedLayoutProps) {
    super(props);
    this.state = {
      isDrawerOpen: false,
    };
  }
  public closeDrawer = () => {
    this.state.isDrawerOpen && this.setState({ isDrawerOpen: false });
  };
  public openDrawer = () => {
    !this.state.isDrawerOpen && this.setState({ isDrawerOpen: true });
  };
  private renderDrawerContent = () => {
    return (
      <CustomDrawerContent
        closeDrawer={this.closeDrawer}
        drawerPosition={this.props.drawerPosition}
      />
    );
  };
  render() {
    return (
      <DrawerView
        drawerPosition={this.props.drawerPosition}
        onClose={this.closeDrawer}
        onOpen={this.openDrawer}
        open={this.state.isDrawerOpen}
        renderDrawerContent={this.renderDrawerContent}
      >
        {this.props.children}
      </DrawerView>
    );
  }
}

export default ReanimatedDrawerLayout;
