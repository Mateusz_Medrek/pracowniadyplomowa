import React from 'react';
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import TouchableOpacity from '../animations/TouchableOpacity';
import useDimensions from '../hooks/useDimensions';
import { COLORS, STYLESHEET, withShadow } from '../styles';

interface Props {
  headerDetails?: [string, string];
  hideForm: () => void;
  onSubmit: () => void;
  showIndicator: boolean;
  submitButtonText: string;
}

const ChangeUserDetailsForm: React.FC<Props> = ({
  children,
  headerDetails,
  hideForm,
  onSubmit,
  showIndicator,
  submitButtonText,
}) => {
  const { containerWidth } = useDimensions();
  return (
    <View style={styles.form}>
      <View
        style={[
          STYLESHEET.rowCenterSpaceBetween,
          styles.data,
          { width: containerWidth - 20 },
        ]}
      >
        {headerDetails ? (
          <View style={styles.formHeader}>
            <Text
              style={[STYLESHEET.baseText, styles.dataText, { opacity: 0.6 }]}
            >
              {headerDetails[0]}
            </Text>
            <Text style={[STYLESHEET.baseText, styles.dataText]}>
              {headerDetails[1]}
            </Text>
          </View>
        ) : (
          <View style={{ flex: 1 }} />
        )}
        <Icon.Button
          backgroundColor="transparent"
          color="red"
          iconStyle={STYLESHEET.iconReset}
          name="close-box"
          onPress={hideForm}
          size={50}
        />
      </View>
      {children}
      <TouchableOpacity
        onPress={onSubmit}
        style={[
          STYLESHEET.baseButton,
          STYLESHEET.lightBlueButton,
          { width: containerWidth - 20 },
        ]}
      >
        {!showIndicator ? (
          <Text style={[STYLESHEET.baseText, STYLESHEET.saveButtonText]}>
            {submitButtonText}
          </Text>
        ) : (
          <ActivityIndicator color={COLORS.WHITE} />
        )}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  data: {
    margin: 10,
  },
  dataText: {
    textAlign: 'left',
  },
  form: {
    alignItems: 'center',
    backgroundColor: COLORS.MEDIUMLIGHTBLUE,
    justifyContent: 'center',
    ...withShadow(),
  },
  formHeader: {
    paddingHorizontal: 5,
  },
});

export default ChangeUserDetailsForm;
