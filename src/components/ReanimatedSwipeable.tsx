import { useIsFocused } from '@react-navigation/native';
import React, { useEffect, useRef } from 'react';

import Swipeable, { SwipeableProps } from '../animations/Swipeable';

const ReanimatedSwipeable: React.FC<Pick<
  SwipeableProps,
  'renderLeftActions' | 'renderRightActions'
>> = ({ children, renderLeftActions, renderRightActions }) => {
  const swipeableRef = useRef<Swipeable>(null);
  const focused = useIsFocused();
  useEffect(() => {
    if (focused) {
      return;
    }
    swipeableRef.current?.close();
  }, [focused]);
  return (
    <Swipeable
      ref={swipeableRef}
      friction={3}
      leftThreshold={30}
      rightThreshold={30}
      renderLeftActions={renderLeftActions}
      renderRightActions={renderRightActions}
    >
      {children}
    </Swipeable>
  );
};

export default ReanimatedSwipeable;
