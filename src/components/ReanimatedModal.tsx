import React, { useEffect, useRef } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Animated from 'react-native-reanimated';

import runTiming from '../animations/runTiming';
import TouchableOpacity from '../animations/TouchableOpacity';
import { COLORS, STYLESHEET, withShadow } from '../styles';

interface Props {
  isVisible: 0 | 1;
  modalButtonText: string;
  modalText: string;
  onModalClose: () => void;
}

const {
  Clock,
  Value,
  and,
  block,
  cond,
  eq,
  interpolate,
  or,
  set,
  useCode,
} = Animated;

const HIDDEN_POSITION = 1000;
const SHOWN_POSITION = 0;

const ReanimatedModal: React.FC<Props> = ({
  isVisible,
  modalButtonText,
  modalText,
  onModalClose,
}) => {
  const clock = useRef<Animated.Clock>(new Clock());
  const animateContainerValue = useRef<Animated.Value<number>>(
    new Value(HIDDEN_POSITION)
  );
  const visibleValue = useRef<Animated.Value<number>>(new Value(isVisible));
  useEffect(() => {
    visibleValue.current.setValue(isVisible);
  }, [isVisible]);
  /** ANIMATE MODAL BLOCK */
  useCode(
    () =>
      block([
        cond(
          or(
            and(
              eq(animateContainerValue.current, HIDDEN_POSITION),
              eq(visibleValue.current, 0)
            ),
            and(
              eq(animateContainerValue.current, SHOWN_POSITION),
              eq(visibleValue.current, 1)
            )
          ),
          0,
          set(
            animateContainerValue.current,
            runTiming(
              clock.current,
              interpolate(visibleValue.current, {
                inputRange: [0, 1],
                outputRange: [SHOWN_POSITION, HIDDEN_POSITION],
              }),
              interpolate(visibleValue.current, {
                inputRange: [0, 1],
                outputRange: [HIDDEN_POSITION, SHOWN_POSITION],
              }),
              new Value(0)
            )
          )
        ),
      ]),
    [visibleValue.current]
  );
  return (
    <Animated.View
      style={[
        StyleSheet.absoluteFillObject,
        withShadow(999),
        {
          transform: [{ translateY: animateContainerValue.current }],
        },
      ]}
    >
      <View style={STYLESHEET.modalContainer}>
        <View style={styles.modalContent}>
          <Text style={STYLESHEET.baseText}>{modalText}</Text>
          <TouchableOpacity
            onPress={onModalClose}
            style={[STYLESHEET.baseButton, styles.modalButton]}
          >
            <Text style={STYLESHEET.baseText}>{modalButtonText}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  modalButton: {
    backgroundColor: COLORS.LIGHTBLUE,
    borderColor: COLORS.LIGHTBLUE,
    borderRadius: 20,
    borderWidth: 1,
    marginTop: 20,
    paddingHorizontal: 40,
  },
  modalContent: {
    alignItems: 'center',
    backgroundColor: COLORS.MEDIUMLIGHTBLUE,
    borderColor: COLORS.MEDIUMLIGHTBLUE,
    borderRadius: 20,
    borderWidth: 1,
    justifyContent: 'center',
    paddingHorizontal: 80,
    paddingVertical: 20,
    ...withShadow(),
  },
});

export default ReanimatedModal;
