import i18n from 'i18n-js';
import React, { useCallback, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import DraggableFlatList, {
  DragEndParams,
  RenderItemParams,
} from 'react-native-draggable-flatlist';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

import { TranslationsKeys } from '../../translations/keys';
import SubTask from '../components/SubTask';
import useDimensions from '../hooks/useDimensions';
import { ISubTask, IUser } from '../store';
import { COLORS, STYLESHEET, withShadow } from '../styles';

type ADD_SUBTASK = {
  type: 'ADD_SUBTASK';
  payload: Omit<ISubTask, 'id' | 'isCompleted'>;
};
type EDIT_SUBTASK = { type: 'EDIT_SUBTASK'; payload: ISubTask };
type SORT_SUBTASKS = { type: 'SORT_SUBTASKS'; payload: ISubTask[] };

type ACTION = ADD_SUBTASK | EDIT_SUBTASK | SORT_SUBTASKS;

interface Props {
  dispatch: React.Dispatch<ACTION>;
  subTasks: ISubTask[];
  user: IUser;
}

const SubTasksList: React.FC<Props> = ({ dispatch, subTasks, user }) => {
  const { containerWidth } = useDimensions();
  const [showSubTask, setShowSubTask] = useState(false);
  const [subTaskArguments, setSubTaskArguments] = useState<ISubTask | null>(
    null
  );
  const onItemsDragEnd = ({ data }: DragEndParams<ISubTask>) =>
    dispatch({ type: 'SORT_SUBTASKS', payload: data });
  const onSubTaskSubmit = (submittedTask: Omit<ISubTask, 'isCompleted'>) => {
    setShowSubTask(false);
    setSubTaskArguments(null);
    return dispatch({
      type: submittedTask.id === -1 ? 'ADD_SUBTASK' : 'EDIT_SUBTASK',
      payload: submittedTask,
    });
  };
  const renderItem = useCallback(
    ({ item, drag, isActive }: RenderItemParams<ISubTask>) => (
      <TouchableWithoutFeedback
        onLongPress={drag}
        onPress={() => {
          setSubTaskArguments(item);
          setShowSubTask(true);
        }}
        style={[styles.subTaskItem, isActive && styles.activeItem]}
      >
        <>
          <View style={styles.category}>
            <Text style={STYLESHEET.baseText}>{item.category}</Text>
          </View>
          <View>
            <Text style={[STYLESHEET.baseText, styles.subTaskName]}>
              Nazwa: {item.title}
            </Text>
            <Text style={STYLESHEET.baseText}>
              {item.assigneeId === user.id
                ? i18n.t(TranslationsKeys.SUBTASKSLIST__ASSIGNED_MYSELF)
                : `${i18n.t(TranslationsKeys.SUBTASKSLIST__ASSIGNED)}: ${
                    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                    user.friends.find(friend => friend.id === item.assigneeId)!
                      .name
                  }`}
            </Text>
          </View>
        </>
      </TouchableWithoutFeedback>
    ),
    [user]
  );
  return (
    <View style={[styles.subTasksContainer, { width: containerWidth - 20 }]}>
      <TouchableWithoutFeedback
        onPress={() => {
          setShowSubTask(prev => !prev);
        }}
        style={styles.addCancelSubTaskButton}
      >
        <Text style={[STYLESHEET.baseText, styles.addCancelSubTaskButtonText]}>
          {i18n.t(
            showSubTask
              ? TranslationsKeys.SUBTASKSLIST__CANCEL_SUBTASK
              : TranslationsKeys.SUBTASKSLIST__ADD_SUBTASK
          )}
        </Text>
      </TouchableWithoutFeedback>
      <DraggableFlatList
        data={subTasks}
        keyExtractor={item => item.id.toString()}
        nestedScrollEnabled={true}
        onDragEnd={onItemsDragEnd}
        renderItem={renderItem}
        style={STYLESHEET.flexOneContainer}
      />
      {showSubTask && (
        <SubTask
          assigneesList={[
            {
              id: user.id,
              name: i18n.t(TranslationsKeys.SUBTASKSLIST__ASSIGN_YOURSELF),
            },
            ...user.friends,
          ]}
          {...subTaskArguments}
          onSubmit={onSubTaskSubmit}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  addCancelSubTaskButton: {
    backgroundColor: COLORS.MEDIUMDARKBLUE,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomColor: COLORS.WHITE,
    borderBottomWidth: 1,
  },
  addCancelSubTaskButtonText: {
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  activeItem: {
    backgroundColor: COLORS.DARKBLUE,
  },
  category: {
    backgroundColor: COLORS.LIGHTBLUE,
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 5,
    position: 'absolute',
    right: -5,
    top: -5,
  },
  subTasksContainer: {
    backgroundColor: COLORS.MEDIUMBLUE,
    height: 200,
    marginHorizontal: 10,
    marginVertical: 5,
    padding: 5,
    ...withShadow(),
  },
  subTaskItem: {
    backgroundColor: COLORS.MEDIUMDARKBLUE,
    borderRadius: 5,
    margin: 5,
    marginTop: 10,
    opacity: 0.8,
    padding: 5,
  },
  subTaskName: {
    flex: 1,
    marginRight: 40,
  },
});

export default SubTasksList;
