import { useNavigation } from '@react-navigation/native';
import i18n from 'i18n-js';
import React from 'react';
import { StyleSheet, ScrollView, Text, View } from 'react-native';
import { useSafeArea } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useSelector } from 'react-redux';

import { TranslationsKeys } from '../../translations/keys';
import TouchableOpacity from '../animations/TouchableOpacity';
import {
  CreateTaskScreenNavigationProp,
  HomeScreenNavigationProp,
  SearchFriendsScreenNavigationProp,
} from '../navigation/types';
import { RootState, TokenProviders, UserState } from '../store';
import { COLORS, STYLESHEET } from '../styles';
import ChangeLocale from './ChangeLocale';

interface Props {
  closeDrawer: () => void;
  drawerPosition: 'left' | 'right';
}

const CustomDrawerContent: React.FC<Props> = ({
  closeDrawer,
  drawerPosition,
}) => {
  const navigation = useNavigation<
    | CreateTaskScreenNavigationProp
    | HomeScreenNavigationProp
    | SearchFriendsScreenNavigationProp
  >();
  const insets = useSafeArea();
  const userState = useSelector<RootState, UserState>(state => state.user);
  const onCloseDrawerButtonPress = () => {
    closeDrawer();
  };
  const onHomeButtonPress = () => {
    onCloseDrawerButtonPress();
    navigation.navigate('Home');
  };
  const onCreateTaskButtonPress = () => {
    onCloseDrawerButtonPress();
    navigation.navigate('CreateTask');
  };
  const onSearchFriendsButtonPress = () => {
    onCloseDrawerButtonPress();
    navigation.navigate('SearchFriends');
  };
  const onSettingsButtonPress = () => {
    onCloseDrawerButtonPress();
    navigation.navigate('Settings');
  };
  return (
    <ScrollView
      contentContainerStyle={{
        backgroundColor: COLORS.MEDIUMDARKBLUE,
        flex: 1,
        paddingTop: insets.top,
        paddingLeft: drawerPosition === 'left' ? insets.left : 0,
        paddingRight: drawerPosition === 'right' ? insets.right : 0,
      }}
    >
      <View
        style={[
          STYLESHEET.rowCenterSpaceBetween,
          styles.row,
          styles.drawerHeader,
        ]}
      >
        <TouchableOpacity
          onPress={onCloseDrawerButtonPress}
          style={styles.drawerButton}
        >
          <Icon color={COLORS.WHITE} name="close" size={40} />
        </TouchableOpacity>
        <Text style={[STYLESHEET.baseText, styles.nameText]}>
          {userState?.name ||
            i18n.t(
              TranslationsKeys.CUSTOM_DRAWER_CONTENT__SERVER_ERROR_MESSAGE
            )}
        </Text>
      </View>
      {userState?.accountType === TokenProviders.server && (
        <TouchableOpacity
          onPress={onSettingsButtonPress}
          style={[
            STYLESHEET.rowCenterSpaceBetween,
            styles.navigateButton,
            styles.row,
            styles.settingsButton,
          ]}
        >
          <View style={styles.settingsIcon}>
            <Icon color={COLORS.WHITE} name="settings" size={30} />
          </View>
          <Text style={[STYLESHEET.baseText, styles.settingsButtonText]}>
            {i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__SETTINGS)}
          </Text>
        </TouchableOpacity>
      )}
      <View style={styles.row}>
        <ChangeLocale
          onChangeToGBLocale={onCloseDrawerButtonPress}
          onChangeToPLLocale={onCloseDrawerButtonPress}
        />
      </View>
      <TouchableOpacity
        onPress={onHomeButtonPress}
        style={[
          STYLESHEET.rowCenterSpaceBetween,
          styles.navigateButton,
          styles.row,
        ]}
      >
        <Icon color={COLORS.WHITE} name="home" size={24} />
        <Text style={[STYLESHEET.baseText, styles.navigateButtonText]}>
          {i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__HOME_PAGE)}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={onCreateTaskButtonPress}
        style={[
          STYLESHEET.rowCenterSpaceBetween,
          styles.navigateButton,
          styles.row,
        ]}
      >
        <Icon color={COLORS.WHITE} name="plus-circle-outline" size={24} />
        <Text style={[STYLESHEET.baseText, styles.navigateButtonText]}>
          {i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__CREATE_TASK)}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={onSearchFriendsButtonPress}
        style={[
          STYLESHEET.rowCenterSpaceBetween,
          styles.navigateButton,
          styles.row,
        ]}
      >
        <Icon color={COLORS.WHITE} name="magnify" size={24} />
        <Text style={[STYLESHEET.baseText, styles.navigateButtonText]}>
          {i18n.t(TranslationsKeys.CUSTOM_DRAWER_CONTENT__SEARCH_FRIENDS)}
        </Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  drawerButton: {
    marginLeft: 10,
  },
  drawerHeader: {
    backgroundColor: COLORS.DARKBLUE,
  },
  nameText: {
    flex: 1,
    marginHorizontal: 10,
  },
  navigateButton: {
    paddingHorizontal: 16,
    paddingVertical: 10,
  },
  navigateButtonText: {
    flex: 1,
    fontWeight: 'bold',
    paddingLeft: 30,
  },
  row: {
    borderBottomColor: COLORS.MEDIUMBLUE,
    borderBottomWidth: 1,
    height: 50,
  },
  settingsButton: {
    justifyContent: 'center',
  },
  settingsButtonText: {
    fontWeight: 'bold',
    textAlign: 'left',
  },
  settingsIcon: {
    marginRight: 10,
  },
});

export default CustomDrawerContent;
