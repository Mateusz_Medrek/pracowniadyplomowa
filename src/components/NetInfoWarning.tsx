import i18n from 'i18n-js';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { TranslationsKeys } from '../../translations/keys';
import useDimensions from '../hooks/useDimensions';
import { COLORS, STYLESHEET, withShadow } from '../styles';

const NetInfoWarning = () => {
  const { containerWidth } = useDimensions();
  return (
    <View style={[styles.container, { width: containerWidth }]}>
      <Text style={STYLESHEET.baseText}>
        {i18n.t(TranslationsKeys.NET_INFO_WARNING__NO_INTERNET_CONNECTION)}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.RED,
    height: 40,
    padding: 2,
    position: 'absolute',
    ...withShadow(999),
  },
});

export default NetInfoWarning;
