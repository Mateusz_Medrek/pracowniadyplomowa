import i18n from 'i18n-js';
import React from 'react';
import { Image, View } from 'react-native';
import { useDispatch } from 'react-redux';

import TouchableOpacity from '../animations/TouchableOpacity';
import { Dispatch } from '../store';
import { STYLESHEET } from '../styles';

interface Props {
  onChangeToGBLocale?: () => void;
  onChangeToPLLocale?: () => void;
}

const ChangeLocale: React.FC<Props> = ({
  onChangeToGBLocale,
  onChangeToPLLocale,
}) => {
  const dispatch = useDispatch<Dispatch>();
  return (
    <View style={STYLESHEET.rowCenter}>
      <TouchableOpacity
        onPress={() => {
          dispatch.locale.changeLocale((i18n.locale = 'en-GB'));
          onChangeToGBLocale?.();
        }}
        style={STYLESHEET.flagButton}
      >
        <Image
          source={require('../../images/united-kingdom-flag-icon-32.png')}
          style={STYLESHEET.flagImage}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          dispatch.locale.changeLocale((i18n.locale = 'pl-PL'));
          onChangeToPLLocale?.();
        }}
        style={STYLESHEET.flagButton}
      >
        <Image
          source={require('../../images/poland-flag-icon-32.png')}
          style={STYLESHEET.flagImage}
        />
      </TouchableOpacity>
    </View>
  );
};

export default ChangeLocale;
