import i18n from 'i18n-js';
import React, { useState } from 'react';
import { Picker, StyleSheet, Text, TextInput, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { TranslationsKeys } from '../../translations/keys';
import TouchableOpacity from '../animations/TouchableOpacity';
import { IFriend, ISubTask } from '../store';
import { COLORS, CONSTS, STYLESHEET } from '../styles';

interface Props extends Partial<Omit<ISubTask, 'isCompleted'>> {
  assigneesList: IFriend[];
  onSubmit: (subTaskArguments: Omit<ISubTask, 'isCompleted'>) => void;
}

const SubTask: React.FC<Props> = ({
  assigneeId,
  assigneesList,
  onSubmit,
  category,
  id,
  title,
}) => {
  const [subTaskTitle, setSubTaskTitle] = useState(title ?? '');
  const [subTaskCategory, setSubTaskCategory] = useState(category ?? 1);
  const [subTaskAssignee, setSubTaskAssignee] = useState<IFriend | null>(
    assigneeId
      ? assigneesList.find(assgn => assgn.id === assigneeId) ?? null
      : null
  );
  const onAssigneePick = (pickedAssigneeId: string) => {
    setSubTaskAssignee(
      pickedAssigneeId
        ? assigneesList.find(assgn => assgn.id === pickedAssigneeId) ?? null
        : null
    );
  };
  const onSubTaskSubmit = () => {
    subTaskAssignee &&
      onSubmit({
        assigneeId: subTaskAssignee.id,
        category: subTaskCategory,
        id: id ?? -1,
        title: subTaskTitle,
      });
  };
  return (
    <View style={styles.container}>
      <View style={STYLESHEET.rowCenterSpaceBetween}>
        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          maxLength={40}
          placeholderTextColor={COLORS.WHITE}
          onChangeText={setSubTaskTitle}
          style={styles.titleInput}
          value={subTaskTitle}
        />
        <TouchableOpacity
          disabled={!subTaskTitle || !subTaskAssignee}
          onPress={onSubTaskSubmit}
          style={styles.submitButton}
        >
          <Icon color={COLORS.WHITE} name="check-circle-outline" size={30} />
        </TouchableOpacity>
      </View>
      <View style={STYLESHEET.rowCenterSpaceBetween}>
        <Text style={[STYLESHEET.baseText, styles.categoryPickerText]}>
          {i18n.t(TranslationsKeys.SUBTASK__CATEGORY)}
        </Text>
        <Picker
          onValueChange={setSubTaskCategory}
          selectedValue={subTaskCategory}
          style={STYLESHEET.flexOneContainer}
        >
          <Picker.Item color={COLORS.WHITE} label="1" value={1} />
          <Picker.Item color={COLORS.WHITE} label="2" value={2} />
          <Picker.Item color={COLORS.WHITE} label="3" value={3} />
          <Picker.Item color={COLORS.WHITE} label="4" value={4} />
          <Picker.Item color={COLORS.WHITE} label="5" value={5} />
        </Picker>
      </View>
      <View style={STYLESHEET.rowCenterSpaceBetween}>
        <Text style={[STYLESHEET.baseText, styles.assigneePickerText]}>
          {i18n.t(TranslationsKeys.SUBTASK__ASSIGNEE)}
        </Text>
        <Picker
          onValueChange={onAssigneePick}
          selectedValue={subTaskAssignee?.id ?? ''}
          style={STYLESHEET.flexOneContainer}
        >
          <Picker.Item
            color={COLORS.WHITE}
            label={i18n.t(TranslationsKeys.SUBTASK__NOT_ASSIGNED)}
            value={''}
          />
          {assigneesList.map(assignee => (
            <Picker.Item
              color={COLORS.WHITE}
              key={assignee.id}
              label={assignee.name}
              value={assignee.id}
            />
          ))}
        </Picker>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  assigneePickerText: {
    marginLeft: 10,
    marginRight: 15,
  },
  categoryPickerText: {
    marginLeft: 10,
    marginRight: 65,
  },
  container: {
    backgroundColor: COLORS.LIGHTBLUE,
    borderRadius: 5,
    margin: 5,
  },
  submitButton: {
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    height: 40,
    margin: 5,
    padding: 5,
    width: 40,
  },
  titleInput: {
    borderColor: COLORS.WHITE,
    borderBottomWidth: 2,
    color: COLORS.WHITE,
    flex: 1,
    fontSize: CONSTS.FONT_SIZE,
    marginLeft: 10,
  },
});

export default SubTask;
