import i18n from 'i18n-js';
import RNCDateTimePicker from '@react-native-community/datetimepicker';
import React, { useEffect, useRef, useState } from 'react';
import { StyleSheet, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import TouchableOpacity from '../animations/TouchableOpacity';
import { TranslationsKeys } from '../../translations/keys';
import { COLORS, CONSTS, withShadow, STYLESHEET } from '../styles';

interface Props {
  dateTime: Date;
  title?: string;
  setDateTime: (newDate: Date) => void;
}

const DateTimePicker: React.FC<Props> = ({ dateTime, title, setDateTime }) => {
  const dateRef = useRef(new Date());
  const timeRef = useRef(new Date());
  const [pickerMode, setPickerMode] = useState<
    'time' | 'date' | 'datetime' | undefined
  >('date');
  const [pickerVisible, setPickerVisible] = useState(false);
  const [shouldSubmitDateTimeValue, setShouldSubmitDateTimeValue] = useState(
    false
  );
  const onDateChange = (event: Event, newDate?: Date) => {
    if (newDate) {
      if (pickerMode === 'date') {
        dateRef.current = newDate;
        setPickerMode('time');
      } else {
        timeRef.current = newDate;
        setPickerMode('date');
        setPickerVisible(false);
        setShouldSubmitDateTimeValue(true);
      }
    }
    setPickerVisible(false);
    setPickerMode('date');
  };
  const onOpenPickerButtonPress = () => {
    setPickerVisible(true);
  };
  useEffect(() => {
    if (!shouldSubmitDateTimeValue) {
      return;
    }
    const newDateTime = new Date(
      dateRef.current.getFullYear(),
      dateRef.current.getMonth(),
      dateRef.current.getDate(),
      timeRef.current.getHours(),
      timeRef.current.getMinutes(),
      0
    );
    setDateTime(newDateTime);
    setShouldSubmitDateTimeValue(false);
  }, [shouldSubmitDateTimeValue]); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <TouchableOpacity
      onPress={onOpenPickerButtonPress}
      style={[styles.container, STYLESHEET.rowCenterSpaceBetween]}
    >
      <Text style={[STYLESHEET.baseText, styles.titleText]}>
        {title || i18n.t(TranslationsKeys.DATE_TIME_PICKER__SET_DATE)}
      </Text>
      <Icon
        color={COLORS.WHITE}
        name="calendar"
        size={24}
        style={styles.calendarButton}
      />
      {pickerVisible && (
        <RNCDateTimePicker
          mode={pickerMode}
          onChange={onDateChange}
          value={dateTime}
        />
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  calendarButton: {
    padding: 5,
  },
  container: {
    backgroundColor: COLORS.MEDIUMBLUE,
    borderColor: COLORS.MEDIUMBLUE,
    borderRadius: CONSTS.TILE_BORDERRADIUS,
    borderWidth: 1,
    paddingHorizontal: 8,
    paddingVertical: 3,
    width: 160,
    ...withShadow(),
  },
  titleText: {
    width: 110,
  },
});

export default DateTimePicker;
