import { GoogleSignin } from '@react-native-community/google-signin';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDispatch, useSelector } from 'react-redux';

import TouchableOpacity from '../animations/TouchableOpacity';
import { Dispatch, RootState, TokenProviders, TokenState } from '../store';
import { COLORS, STYLESHEET } from '../styles';

interface Props {
  onDrawerButtonPress?: () => void;
  title?: string;
}

const Header: React.FC<Props> = ({ onDrawerButtonPress, title }) => {
  const tokenState = useSelector<RootState, TokenState>(state => state.token);
  const dispatch = useDispatch<Dispatch>();
  const onLogoutButtonPress = async () => {
    try {
      switch (tokenState.tokenType) {
        case TokenProviders.google:
          await GoogleSignin.revokeAccess();
          await GoogleSignin.signOut();
          break;
        case TokenProviders.server:
          break;
      }
      dispatch.token.removeToken();
    } catch (err) {
      console.log(err.message);
    }
  };
  return (
    <View style={[STYLESHEET.rowCenterFlexStart, styles.headerContainer]}>
      <TouchableOpacity
        onPress={onDrawerButtonPress}
        style={styles.openDrawerButton}
      >
        <Icon color={COLORS.WHITE} name="menu" size={30} />
      </TouchableOpacity>
      <View
        style={[STYLESHEET.flexOneContainer, STYLESHEET.rowCenterSpaceBetween]}
      >
        <Text style={[STYLESHEET.baseText, styles.title]}>{title || ''}</Text>
        <TouchableOpacity
          onPress={onLogoutButtonPress}
          style={styles.logoutButton}
        >
          <Icon color={COLORS.WHITE} name="logout" size={36} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: COLORS.DARKBLUE,
    height: 50,
  },
  logoutButton: {
    margin: 5,
  },
  openDrawerButton: {
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 10,
  },
  title: {
    marginHorizontal: 10,
    paddingHorizontal: 5,
    textAlign: 'left',
  },
});

export default Header;
