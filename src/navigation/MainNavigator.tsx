import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import i18n from 'i18n-js';
import React from 'react';

import { TranslationsKeys } from '../../translations/keys';
import CreateTaskScreen from '../screens/CreateTaskScreen';
import CompletedTasksScreen from '../screens/CompletedTasksScreen';
import FriendsDetailScreen from '../screens/FriendsDetailScreen';
import HomeScreen from '../screens/HomeScreen';
import SearchFriendsScreen from '../screens/SearchFriendsScreen';
import SettingsScreen from '../screens/SettingsScreen';
import TaskDetailsScreen from '../screens/TaskDetailsScreen';
import { MainParamList } from './types';
import { COLORS } from '../styles';

const Main = createNativeStackNavigator<MainParamList>();

const MainNavigator = () => {
  return (
    <Main.Navigator>
      <Main.Screen
        component={HomeScreen}
        name="Home"
        options={{ headerShown: false }}
      />
      <Main.Screen
        component={CreateTaskScreen}
        name="EditTask"
        options={{
          headerStyle: { backgroundColor: COLORS.DARKBLUE },
          headerTitle: '',
        }}
      />
      <Main.Screen
        component={CompletedTasksScreen}
        name="CompletedTasks"
        options={{
          headerStyle: { backgroundColor: COLORS.DARKBLUE },
          headerTintColor: COLORS.WHITE,
          headerTitle: i18n.t(TranslationsKeys.COMPLETED_TASKS_SCREEN__HEADER),
        }}
      />
      <Main.Screen
        component={TaskDetailsScreen}
        name="TaskDetails"
        options={{
          headerStyle: { backgroundColor: COLORS.DARKBLUE },
          headerTitle: '',
        }}
      />
      <Main.Screen
        component={CreateTaskScreen}
        name="CreateTask"
        options={{ headerShown: false }}
      />
      <Main.Screen
        component={SearchFriendsScreen}
        name="SearchFriends"
        options={{ headerShown: false }}
      />
      <Main.Screen
        component={FriendsDetailScreen}
        name="FriendsDetail"
        options={{
          headerStyle: { backgroundColor: COLORS.DARKBLUE },
          headerTitle: '',
        }}
      />
      <Main.Screen
        component={SettingsScreen}
        name="Settings"
        options={{
          headerStyle: { backgroundColor: COLORS.DARKBLUE },
          headerTintColor: COLORS.WHITE,
          headerTitle: i18n.t(TranslationsKeys.SETTINGS_SCREEN__HEADER),
        }}
      />
    </Main.Navigator>
  );
};
export default MainNavigator;
