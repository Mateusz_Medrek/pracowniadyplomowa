import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import i18n from 'i18n-js';
import React from 'react';

import { TranslationsKeys } from '../../translations/keys';
import AuthScreen from '../screens/AuthScreen';
import RegistrationScreen from '../screens/RegistrationScreen';
import { AuthParamList } from './types';
import { COLORS } from '../styles';

const Auth = createNativeStackNavigator<AuthParamList>();

const AuthNavigator = () => {
  return (
    <Auth.Navigator>
      <Auth.Screen
        name="Auth"
        component={AuthScreen}
        options={{ headerShown: false }}
      />
      <Auth.Screen
        name="Registration"
        component={RegistrationScreen}
        options={{
          headerStyle: { backgroundColor: COLORS.DARKBLUE },
          headerTintColor: COLORS.WHITE,
          headerTitle: i18n.t(TranslationsKeys.REGISTRATION_SCREEN__HEADER),
        }}
      />
    </Auth.Navigator>
  );
};

export default AuthNavigator;
