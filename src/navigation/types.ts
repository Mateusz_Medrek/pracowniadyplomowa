import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from 'react-native-screens/native-stack';

import { ITask } from '../store';

export type AuthParamList = {
  Auth: undefined;
  Registration: undefined;
};

export type MainParamList = {
  Home: undefined;
  EditTask: { taskData: ITask & { id: string } };
  CompletedTasks: undefined;
  TaskDetails: { id: string };
  CreateTask: { taskData: ITask & { id: string } } | undefined;
  SearchFriends: undefined;
  FriendsDetail: { id: string };
  Settings: undefined;
};

export type AuthScreenNavigationProp = NativeStackNavigationProp<
  AuthParamList,
  'Auth'
>;

export type CompletedTasksScreenNavigationProp = NativeStackNavigationProp<
  MainParamList,
  'CompletedTasks'
>;

export type CreateTaskScreenNavigationProp = NativeStackNavigationProp<
  MainParamList,
  'CreateTask' | 'EditTask'
>;

export type FriendsDetailScreenNavigationProp = NativeStackNavigationProp<
  MainParamList,
  'FriendsDetail'
>;

export type HeaderNavigationProp = NativeStackNavigationProp<
  MainParamList,
  'CreateTask' | 'Home' | 'SearchFriends'
>;

export type HomeScreenNavigationProp = NativeStackNavigationProp<
  MainParamList,
  'Home'
>;

export type RegistrationScreenNavigationProp = NativeStackNavigationProp<
  AuthParamList,
  'Registration'
>;

export type SearchFriendsScreenNavigationProp = NativeStackNavigationProp<
  MainParamList,
  'SearchFriends'
>;

export type SettingsScreenNavigationProp = NativeStackNavigationProp<
  MainParamList,
  'Settings'
>;

export type TaskDetailsScreenNavigationProp = NativeStackNavigationProp<
  MainParamList,
  'TaskDetails'
>;

export type CreateTaskScreenRouteProp = RouteProp<
  MainParamList,
  'CreateTask' | 'EditTask'
>;

export type FriendsDetailScreenRouteProp = RouteProp<
  MainParamList,
  'FriendsDetail'
>;

export type TaskDetailsScreenRouteProp = RouteProp<
  MainParamList,
  'TaskDetails'
>;
