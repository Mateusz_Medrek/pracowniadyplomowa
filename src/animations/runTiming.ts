import Animated, { Easing } from 'react-native-reanimated';

const {
  Value,
  block,
  clockRunning,
  cond,
  defined,
  set,
  startClock,
  stopClock,
  timing,
} = Animated;

const runTiming = (
  clock: Animated.Clock,
  value: Animated.Adaptable<number>,
  dest: Animated.Adaptable<number>,
  offset: Animated.Value<number>,
  duration: Animated.Adaptable<number> = 500
) => {
  const state = {
    finished: new Value<number>(0),
    frameTime: new Value<number>(0),
    position: new Value<number>(0),
    time: new Value<number>(0),
  };
  const config = {
    duration,
    easing: Easing.inOut(Easing.ease),
    toValue: new Value<number>(0),
  };
  return block([
    // if clock is not running, we reset all parameters and start clock;
    cond(clockRunning(clock), 0, [
      set(state.finished, 0),
      set(state.time, 0),
      set(state.position, value),
      set(state.frameTime, 0),
      set(config.toValue, dest),
      startClock(clock),
    ]),
    timing(clock, state, config),
    cond(state.finished, [stopClock(clock), set(offset, state.position)]),
    state.position,
  ]);
};

export default runTiming;
