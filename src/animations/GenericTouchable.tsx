import React from 'react';
import {
  AccessibilityRole,
  AccessibilityStates,
  LayoutChangeEvent,
  Platform,
  StyleProp,
  ViewStyle,
} from 'react-native';
import {
  BaseButton,
  NativeViewGestureHandlerGestureEvent,
  NativeViewGestureHandlerStateChangeEvent,
  State,
} from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';

/**
 * Each touchable is a states' machine which preforms transitions.
 * On very beginning (and on the very end or recognition) touchable is
 * UNDETERMINED. Then it moves to BEGAN. If touchable recognizes that finger
 * travel outside it transits to special MOVED_OUTSIDE state. Gesture recognition
 * finishes in UNDETERMINED state.
 */
export enum TOUCHABLE_STATE {
  UNDETERMINED = 0,
  BEGAN = 1,
  MOVED_OUTSIDE = 2,
}

export interface PublicProps {
  accessible?: boolean;
  accessibilityLabel?: string;
  accessibilityHint?: string;
  accessibilityRole?: AccessibilityRole;
  accessibilityStates?: AccessibilityStates[];
  hitSlop?: {
    top: number;
    left: number;
    bottom: number;
    right: number;
  };
  disabled?: boolean;
  onPress?: () => void;
  onPressIn?: () => void;
  onPressOut?: () => void;
  onLayout?: (event: LayoutChangeEvent) => void;
  onLongPress?: () => void;
  nativeID?: string;
  testID?: string;
  delayPressIn?: number;
  delayPressOut?: number;
  delayLongPress?: number;
  shouldActivateOnStart?: boolean;
  disallowInterruption?: boolean;
  containerStyle?: StyleProp<ViewStyle>;
  style?: StyleProp<Animated.AnimateStyle<ViewStyle>>;
}

interface InternalProps {
  extraButtonProps?: Record<string, any>;
  onStateChange?: (
    oldState: TOUCHABLE_STATE,
    newState: TOUCHABLE_STATE
  ) => void;
}

/**
 * COPY FROM RNGH repo with Typescript types instead of prop-types
 *
 * {@link https://github.com/software-mansion/react-native-gesture-handler/blob/master/touchables/GenericTouchable.js}
 *
 * GenericTouchable is not intented to be used as it.
 * Should be treated as a source for the rest of touchables
 */

class GenericTouchable extends React.Component<PublicProps & InternalProps> {
  static defaultProps = {
    delayLongPress: 600,
    extraButtonProps: {
      rippleColor: 'transparent',
    },
  };

  // timeout handlers
  private pressInTimeout: NodeJS.Timeout | null = null;
  private pressOutTimeout: NodeJS.Timeout | null = null;
  private longPressTimeout: NodeJS.Timeout | null = null;

  /** This flag is required since recognition of longPress implies not-invoking onPress */
  private longPressDetected = false;

  private pointerInside = true;

  /** State of touchable */
  private STATE = TOUCHABLE_STATE.UNDETERMINED;

  /**
   * handlePressIn in called on first touch on traveling inside component.
   * Handles state transition with delay.
   */
  private handlePressIn() {
    if (this.props.delayPressIn) {
      this.pressInTimeout = setTimeout(() => {
        this.moveToState(TOUCHABLE_STATE.BEGAN);
        this.pressInTimeout = null;
      }, this.props.delayPressIn);
    } else {
      this.moveToState(TOUCHABLE_STATE.BEGAN);
    }
    if (this.props.onLongPress) {
      const time =
        (this.props.delayPressIn || 0) + (this.props.delayLongPress || 0);
      this.longPressTimeout = setTimeout(this.onLongPressDetected, time);
    }
  }

  /**
   * handleMoveOutside in called on traveling outside component.
   * Handles state transition with delay.
   */
  private handleMoveOutside() {
    if (this.props.delayPressOut) {
      this.pressOutTimeout =
        this.pressOutTimeout ||
        setTimeout(() => {
          this.moveToState(TOUCHABLE_STATE.MOVED_OUTSIDE);
          this.pressOutTimeout = null;
        }, this.props.delayPressOut);
    } else {
      this.moveToState(TOUCHABLE_STATE.MOVED_OUTSIDE);
    }
  }

  /** handleGoToUndetermined transits to UNDETERMINED state with proper delay */
  private handleGoToUndetermined() {
    this.pressOutTimeout && clearTimeout(this.pressOutTimeout);
    if (this.props.delayPressOut) {
      this.pressOutTimeout = setTimeout(() => {
        if (this.STATE === TOUCHABLE_STATE.UNDETERMINED) {
          this.moveToState(TOUCHABLE_STATE.BEGAN);
        }
        this.moveToState(TOUCHABLE_STATE.UNDETERMINED);
        this.pressOutTimeout = null;
      }, this.props.delayPressOut);
    } else {
      if (this.STATE === TOUCHABLE_STATE.UNDETERMINED) {
        this.moveToState(TOUCHABLE_STATE.BEGAN);
      }
      this.moveToState(TOUCHABLE_STATE.UNDETERMINED);
    }
  }

  componentDidMount() {
    this.reset();
  }

  /** reset timeout to prevent memory leaks. */
  private reset() {
    this.longPressDetected = false;
    this.pointerInside = true;
    this.pressInTimeout && clearTimeout(this.pressInTimeout);
    this.pressOutTimeout && clearTimeout(this.pressOutTimeout);
    this.longPressTimeout && clearTimeout(this.longPressTimeout);
    this.pressOutTimeout = null;
    this.longPressTimeout = null;
    this.pressInTimeout = null;
  }

  /** All states' transitions are defined here. */
  private moveToState(newState: TOUCHABLE_STATE) {
    if (newState === this.STATE) {
      // Ignore dummy transitions
      return;
    }
    if (newState === TOUCHABLE_STATE.BEGAN) {
      // First touch and moving inside
      this.props.onPressIn?.();
    } else if (newState === TOUCHABLE_STATE.MOVED_OUTSIDE) {
      // Moving outside
      this.props.onPressOut?.();
    } else if (newState === TOUCHABLE_STATE.UNDETERMINED) {
      // Need to reset each time on transition to UNDETERMINED
      this.reset();
      if (this.STATE === TOUCHABLE_STATE.BEGAN) {
        // ... and if it happens inside button.
        this.props.onPressOut?.();
      }
    }
    // Finally call lister (used by subclasses)
    this.props.onStateChange?.(this.STATE, newState);
    // ... and make transition.
    this.STATE = newState;
  }

  /** BaseButton onGestureEvent */
  private onGestureEvent = ({
    nativeEvent: { pointerInside },
  }: NativeViewGestureHandlerGestureEvent) => {
    if (this.pointerInside !== pointerInside) {
      if (pointerInside) {
        this.onMoveIn();
      } else {
        this.onMoveOut();
      }
    }
    this.pointerInside = pointerInside;
  };

  /**
   * BaseButton onHandlerStateChange
   *
   * When disabled prop is true, it does nothing;
   */
  private onHandlerStateChange = ({
    nativeEvent,
  }: NativeViewGestureHandlerStateChangeEvent) => {
    if (this.props.disabled) {
      return;
    }
    const { state } = nativeEvent;
    if (state === State.CANCELLED || state === State.FAILED) {
      // Need to handle case with external cancellation (e.g. by ScrollView)
      this.moveToState(TOUCHABLE_STATE.UNDETERMINED);
    } else if (
      // This platform check is an implication of slightly different behavior of handlers on different platform.
      // And Android "Active" state is achieving on first move of a finger, not on press in.
      // On iOS event on "Began" is not delivered.
      state === (Platform.OS === 'ios' ? State.ACTIVE : State.BEGAN) &&
      this.STATE === TOUCHABLE_STATE.UNDETERMINED
    ) {
      // Moving inside requires
      this.handlePressIn();
    } else if (state === State.END) {
      const shouldCallOnPress =
        !this.longPressDetected &&
        this.STATE !== TOUCHABLE_STATE.MOVED_OUTSIDE &&
        this.pressOutTimeout === null;
      this.handleGoToUndetermined();
      if (shouldCallOnPress) {
        // Calls only inside component whether no long press was called previously
        this.props.onPress && this.props.onPress();
      }
    }
  };

  private onLongPressDetected = () => {
    this.longPressDetected = true;
    this.props.onLongPress?.();
  };

  componentWillUnmount() {
    // to prevent memory leaks
    this.reset();
  }

  private onMoveIn() {
    if (this.STATE === TOUCHABLE_STATE.MOVED_OUTSIDE) {
      // This call is not throttled with delays (like in RN's implementation).
      this.moveToState(TOUCHABLE_STATE.BEGAN);
    }
  }

  private onMoveOut() {
    // long press should no longer be detected
    this.longPressTimeout && clearTimeout(this.longPressTimeout);
    this.longPressTimeout = null;
    if (this.STATE === TOUCHABLE_STATE.BEGAN) {
      this.handleMoveOutside();
    }
  }

  render() {
    const coreProps = {
      accessible: this.props.accessible !== false,
      accessibilityLabel: this.props.accessibilityLabel,
      accessibilityHint: this.props.accessibilityHint,
      accessibilityRole: this.props.accessibilityRole,
      accessibilityStates: this.props.accessibilityStates,
      nativeID: this.props.nativeID,
      testID: this.props.testID,
      onLayout: this.props.onLayout,
      hitSlop: this.props.hitSlop,
    };
    return (
      <BaseButton
        style={this.props.containerStyle}
        onHandlerStateChange={this.onHandlerStateChange}
        onGestureEvent={this.onGestureEvent}
        hitSlop={this.props.hitSlop}
        shouldActivateOnStart={this.props.shouldActivateOnStart}
        disallowInterruption={this.props.disallowInterruption}
        {...this.props.extraButtonProps}
      >
        <Animated.View {...coreProps} style={this.props.style}>
          {this.props.children}
        </Animated.View>
      </BaseButton>
    );
  }
}

export default GenericTouchable;
