import React from 'react';
import {
  I18nManager,
  LayoutChangeEvent,
  StyleSheet,
  View,
  ViewStyle,
} from 'react-native';
import {
  PanGestureHandler,
  TapGestureHandler,
  State,
  TapGestureHandlerStateChangeEvent,
  PanGestureHandlerStateChangeEvent,
  PanGestureHandlerGestureEvent,
} from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';

const DRAG_TOSS = 0.05;

const {
  Clock,
  Extrapolate,
  SpringUtils,
  Value,
  add,
  and,
  block,
  call,
  clockRunning,
  cond,
  divide,
  eq,
  event,
  greaterOrEq,
  greaterThan,
  interpolate,
  lessOrEq,
  lessThan,
  max,
  multiply,
  neq,
  onChange,
  proc,
  set,
  spring,
  startClock,
  stopClock,
  sub,
} = Animated;

/**
 * (dragX + DRAG_TOSS * velocityX) / friction
 *
 * @param {Animated.Adaptable<number>} dragX translationX
 * @param {Animated.Adaptable<number>} velocityX
 * @param {Animated.Adaptable<number>} friction
 */
const _handleReleaseTranslationX = proc((dragX, velocityX, friction) =>
  divide(add(dragX, multiply(DRAG_TOSS, velocityX)), friction)
);

/**
 * -1 * Math.max(0, rowWidth - rightOffset)
 *
 * @param {Animated.Adaptable<number>} rowWidth
 * @param {Animated.Adaptable<number>} rightOffset
 */
const onGestureEventRightWidth = proc((rowWidth, rightOffset) =>
  multiply(max(0, sub(rowWidth, rightOffset)), -1)
);

/**
 * overshootFriction > 1 ? 1 : 0
 *
 * @param {Animated.Adaptable<number>} overshootFriction
 */
const onGestureEventIfOvershootFrictionGreaterThanOne = proc(
  overshootFriction => cond(greaterThan(overshootFriction, 1), 1, 0)
);

/**
 * 0 - false;
 *
 * 1 - true;
 */
type Binary = 0 | 1;
/**
 * -1 - swiped to left;
 *
 *  0 - closed;
 *
 *  1 - swiped to right;
 */
type RowState = -1 | 0 | 1;

export interface SwipeableProps {
  // DEFAULT VALUES:
  friction: number;
  overshootFriction: number;
  // OPTIONAL:
  leftThreshold?: number;
  rightThreshold?: number;
  overshootLeft?: boolean;
  overshootRight?: boolean;
  onSwipeableLeftOpen?: () => void;
  onSwipeableRightOpen?: () => void;
  onSwipeableOpen?: () => void;
  onSwipeableClose?: () => void;
  onSwipeableLeftWillOpen?: () => void;
  onSwipeableRightWillOpen?: () => void;
  onSwipeableWillOpen?: () => void;
  onSwipeableWillClose?: () => void;
  /**
   *
   * This map describes the values to use as inputRange for extra interpolation:
   * AnimatedValue: [startValue, endValue]
   *
   * progressAnimatedValue: [0, 1]
   * dragAnimatedValue: [0, +]
   *
   * To support `rtl` flexbox layouts use `flexDirection` styling.
   * */
  renderLeftActions?: (
    progressAnimatedValue: Animated.Node<number>,
    dragAnimatedValue: Animated.Node<number>
  ) => React.ReactNode;
  /**
   *
   * This map describes the values to use as inputRange for extra interpolation:
   * AnimatedValue: [startValue, endValue]
   *
   * progressAnimatedValue: [0, 1]
   * dragAnimatedValue: [0, -]
   *
   * To support `rtl` flexbox layouts use `flexDirection` styling.
   * */
  renderRightActions?: (
    progressAnimatedValue: Animated.Node<number>,
    dragAnimatedValue: Animated.Node<number>
  ) => React.ReactNode;
  containerStyle?: ViewStyle;
  childrenContainerStyle?: ViewStyle;
}

interface SwipeableState {
  rowState: RowState;
}

/**
 * This component is based on RNGH Swipeable component
 *
 * {@link https://github.com/software-mansion/react-native-gesture-handler/blob/master/Swipeable.js}
 *
 * Difference is this component uses Animated module from react-native-reanimated
 * {@link https://github.com/software-mansion/react-native-reanimated}
 * instead of Animated module from RN's core
 */
class Swipeable extends React.PureComponent<SwipeableProps, SwipeableState> {
  static defaultProps = {
    friction: 1,
    overshootFriction: 1,
  };

  state: SwipeableState = {
    rowState: 0,
  };

  /**
   * Contains position value returned from spring node,
   * after finishing animation it sets final translationX value;
   */
  _animateRowSpringPosition: Animated.Value<number> = new Value(0);
  /**
   * Helper Animated Value - used in this._animateRow method, contains toValue argument
   * which is passed to call node;
   */
  _animateRowToValue: Animated.Value<number> = new Value(0);
  /**
   * Clock for animating Swipeable;
   */
  _clock: Animated.Clock = new Clock();
  /**
   * Contains oldState value from PanGestureHandler onHandlerStateChange event;
   */
  _oldPanGestureState: Animated.Value<State> = new Value(State.UNDETERMINED);
  /**
   * Contains final translationX value for Swipeable;
   */
  _transX: Animated.Value<number> = new Value(0);
  /**
   * Contains binary value whether show or not leftActions;
   */
  _showLeftAction: Animated.Value<Binary> = new Value(0);
  /**
   * Contains binary value whether show or not rightActions;
   */
  _showRightAction: Animated.Value<Binary> = new Value(0);
  /**
   * Contains translationX value for leftActions based on this._showLeftAction;
   */
  _leftActionTranslate: Animated.Value<number> = new Value(0);
  /**
   * Contains translationX value for rightActions based on this._showRightAction;
   */
  _rightActionTranslate: Animated.Value<number> = new Value(0);
  /**
   * Contains initial translationX value recorded from this._onGestureEvent;
   */
  _dragX: Animated.Value<number> = new Value(0);
  /**
   * When component is dragged it contains valueX before drag;
   *
   * When component is released it contains valueX from which spring animation starts;
   */
  _rowTranslation: Animated.Value<number> = new Value(0);

  // "Method" values;
  /**
   * Setting this value to current value multiplied by -1 triggers showing leftActions;
   *
   * This is only set in this.openLeft method;
   */
  _openLeftValue: Animated.Value<number> = new Value(1);
  /**
   * Setting this value to current value multiplied by -1 triggers showing rightActions;
   *
   * This is only set in this.openRight method;
   */
  _openRightValue: Animated.Value<number> = new Value(1);
  /**
   * Setting this value to current value multiplied by -1 triggers closing Swipeable;
   *
   * This is only set in this.close method and after "touching" TapGestureHandler;
   */
  _closeValue: Animated.Value<number> = new Value(1);

  // "State" values;
  /**
   * Contains current leftActions width value;
   */
  _leftWidth: Animated.Value<number> = new Value(0);
  /**
   * Contains current valueX of left side of rightActions' container;
   *
   * Current rightActions width value = this._rowWidth - this._rightOffset
   */
  _rightOffset: Animated.Value<number> = new Value(0);
  /**
   * Contains current value of Swipeable position:
   * -1 is swiped to left
   *  0 is closed
   *  1 is swiped to right
   */
  _rowState: Animated.Value<RowState> = new Value(0);
  /**
   * Contains previous value of Swipeable position:
   * -1 is swiped to left
   *  0 is closed
   *  1 is swiped to right
   */
  _prevRowState: Animated.Value<RowState> = new Value(0);
  /**
   * Contains current Swipeable container width value (container is child of PanGestureHandler);
   */
  _rowWidth: Animated.Value<number> = new Value(0);

  // Converting props to Animated Values;
  _friction: Animated.Value<number> = new Value(this.props.friction);
  _leftThreshold: Animated.Value<number> = new Value(
    !this.props.leftThreshold ? 0 : this.props.leftThreshold
  );
  _rightThreshold: Animated.Value<number> = new Value(
    !this.props.rightThreshold ? 0 : this.props.rightThreshold
  );
  _overshootLeft: Animated.Value<number> = new Value(
    !this.props.overshootLeft ? 0 : 1
  );
  _overshootRight: Animated.Value<number> = new Value(
    !this.props.overshootRight ? 0 : 1
  );
  _overshootFriction: Animated.Value<number> = new Value(
    this.props.overshootFriction
  );

  componentDidUpdate(prevProps: SwipeableProps) {
    if (
      this.props.friction !== prevProps.friction ||
      this.props.overshootLeft !== prevProps.overshootLeft ||
      this.props.overshootRight !== prevProps.overshootRight ||
      this.props.overshootFriction !== prevProps.overshootFriction
    ) {
      this._updateAnimatedEvent(this.props);
    }
  }

  /**
   * This method updates Animated Values based on props after updating component props
   * and after onLayouts updates;
   */
  private _updateAnimatedEvent = (
    props: SwipeableProps,
    stateValues?: {
      leftWidth?: number;
      rightOffset?: number;
      rowWidth?: number;
    }
  ) => {
    // After component props update we must manually update Animated Values with updated props;
    const leftWidth = stateValues?.leftWidth ?? 0;
    const rowWidth = stateValues?.rowWidth ?? 0;
    const rightOffset = stateValues?.rightOffset ?? rowWidth;
    const rightWidth = Math.max(0, rowWidth - rightOffset);
    this._friction.setValue(props.friction);
    this._leftThreshold.setValue(
      !props.leftThreshold ? leftWidth / 2 : props.leftThreshold
    );
    this._rightThreshold.setValue(
      !props.rightThreshold
        ? (rowWidth - rightOffset) / 2
        : props.rightThreshold
    );
    this._overshootLeft.setValue(
      !props.overshootLeft ? (leftWidth > 0 ? 1 : 0) : 1
    );
    this._overshootRight.setValue(
      !props.overshootRight ? (rightWidth > 0 ? 1 : 0) : 1
    );
    this._overshootFriction.setValue(props.overshootFriction);
  };

  /**
   * This method runs spring animation and after finish calls "onOpen" and "onClose" props methods;
   */
  private _spring = (
    fromValue: Animated.Adaptable<number>,
    toValue: Animated.Adaptable<number>,
    velocity: Animated.Adaptable<number>
  ) => {
    const state = {
      finished: new Value<number>(0),
      position: new Value<number>(0),
      time: new Value<number>(0),
      velocity: new Value<number>(0),
    };
    const config = {
      ...SpringUtils.makeConfigFromBouncinessAndSpeed({
        bounciness: 0,
        mass: 5,
        overshootClamping: false,
        restDisplacementThreshold: 0.4,
        restSpeedThreshold: 1.7,
        speed: 8,
        toValue: new Value<number>(0),
      }),
      // To make toValue of type Animated.Value instead of Animated.Adaptable;
      toValue: new Value<number>(0),
    };
    return block([
      cond(clockRunning(this._clock), 0, [
        set(state.finished, 0),
        set(state.time, 0),
        set(state.position, fromValue),
        set(state.velocity, velocity),
        set(config.toValue, toValue),
        startClock(this._clock),
      ]),
      spring(this._clock, state, config),
      cond(state.finished, [
        stopClock(this._clock),
        set(this._rowTranslation, state.position),
        set(this._prevRowState, this._rowState),
        call([config.toValue], ([toVal]) => {
          if (toVal > 0) {
            this.props.onSwipeableLeftOpen?.();
          } else if (toVal < 0) {
            this.props.onSwipeableRightOpen?.();
          }
          if (toVal === 0) {
            this.props.onSwipeableClose?.();
          } else {
            this.props.onSwipeableOpen?.();
          }
        }),
      ]),
      state.position,
    ]);
  };

  /**
   * This block runs spring animation and calls "onWillOpen" and "onWillClose" props methods;
   */
  private _animateRow = (
    fromValue: Animated.Adaptable<number>,
    toValue: Animated.Adaptable<number>,
    velocityX: Animated.Adaptable<number> = 5
  ) =>
    block([
      set(this._rowTranslation, fromValue),
      set(this._animateRowToValue, toValue),
      set(
        this._rowState,
        cond(
          and(greaterThan(toValue, 0), greaterOrEq(this._prevRowState, 0)),
          1,
          cond(
            and(lessThan(toValue, 0), lessOrEq(this._prevRowState, 0)),
            -1,
            0
          )
        )
      ),
      set(
        this._animateRowSpringPosition,
        this._spring(this._rowTranslation, this._animateRowToValue, velocityX)
      ),
      set(this._dragX, 0),
      // TODO: MAKE BELOW CALL NODE EVALUATE ONLY ONCE - IT CAUSES MANY CALLS TO JS WHICH CRASHES APP;
      // call([this._animateRowToValue, this._rowState], ([toVal, rowState]) => {
      //   if (toVal > 0) {
      //     this.props.onSwipeableLeftWillOpen?.();
      //   } else if (toVal < 0) {
      //     this.props.onSwipeableRightWillOpen?.();
      //   }
      //   if (toVal === 0) {
      //     this.props.onSwipeableWillClose?.();
      //   } else {
      //     this.props.onSwipeableWillOpen?.();
      //   }
      //   this.setState({ rowState: rowState as RowState });
      // }),
    ]);

  /**
   * This method returns Animated Value based on current Swipeable state;
   */
  private _currentOffset = cond(
    eq(this._rowState, 1),
    this._leftWidth,
    cond(
      eq(this._rowState, -1),
      multiply(sub(this._rowWidth, this._rightOffset), -1),
      0
    )
  );

  /**
   * This method returns spring animation toValue Animated Value;
   */
  private _handleReleaseToValue = (translationX: number, velocityX: number) => {
    return cond(
      eq(this._rowState, 0),
      cond(
        greaterThan(
          _handleReleaseTranslationX(translationX, velocityX, this._friction),
          this._leftThreshold
        ),
        this._leftWidth,
        cond(
          and(
            lessThan(
              _handleReleaseTranslationX(
                translationX,
                velocityX,
                this._friction
              ),
              multiply(this._rightThreshold, -1)
            ),
            neq(this._rightOffset, 0)
          ),
          multiply(sub(this._rowWidth, this._rightOffset), -1),
          0
        )
      ),
      cond(
        eq(this._rowState, 1),
        [
          // already swiped to left and gesture will be to right direction
          cond(
            greaterThan(
              _handleReleaseTranslationX(
                translationX,
                velocityX,
                this._friction
              ),
              multiply(this._leftThreshold, -1)
            ),
            this._leftWidth,
            0
          ),
        ],
        [
          // already swiped to right and gesture will be to left direction
          cond(
            lessThan(
              _handleReleaseTranslationX(
                translationX,
                velocityX,
                this._friction
              ),
              this._rightThreshold
            ),
            multiply(sub(this._rowWidth, this._rightOffset), -1),
            0
          ),
        ]
      )
    );
  };

  /**
   * This method returns block which triggers animating Swipeable;
   */
  private _handleRelease = (translationX: number, velocityX: number) => {
    return block([
      this._animateRow(
        add(this._currentOffset, divide(translationX, this._friction)),
        this._handleReleaseToValue(translationX, velocityX),
        divide(velocityX, this._friction)
      ),
    ]);
  };

  /**
   * This node set whether show leftActions or not;
   */
  private _setShowLeftAction = set(
    this._showLeftAction,
    cond(
      greaterThan(this._leftWidth, 0),
      interpolate(this._transX, {
        inputRange: [-1, 0, this._leftWidth],
        outputRange: [0, 0, 1],
      }),
      0
    )
  );

  /**
   * This node set whether show rightActions or not;
   */
  private _setShowRightAction = set(
    this._showRightAction,
    cond(
      greaterThan(
        multiply(
          onGestureEventRightWidth(this._rowWidth, this._rightOffset),
          -1
        ),
        0
      ),
      interpolate(this._transX, {
        inputRange: [
          onGestureEventRightWidth(this._rowWidth, this._rightOffset),
          0,
          1,
        ],
        outputRange: [1, 0, 0],
      }),
      0
    )
  );

  /**
   * This node set translationX of leftActions;
   */
  private _translateLeftAction = set(
    this._leftActionTranslate,
    interpolate(this._showLeftAction, {
      inputRange: [0, Number.MIN_VALUE],
      outputRange: [-10000, 0],
      extrapolate: Extrapolate.CLAMP,
    })
  );

  /**
   * This node set translationX of rightActions;
   */
  private _translateRightAction = set(
    this._rightActionTranslate,
    interpolate(this._showRightAction, {
      inputRange: [0, Number.MIN_VALUE],
      outputRange: [-10000, 0],
      extrapolate: Extrapolate.CLAMP,
    })
  );

  /**
   * This block runs setting:
   * -> final translationX value
   * -> showing and translatingX leftActions
   * -> showing and translatingX rightActions
   */
  private _translateX = block([
    cond(
      eq(this._oldPanGestureState, State.ACTIVE),
      set(this._transX, this._animateRowSpringPosition),
      set(
        this._transX,
        interpolate(
          add(
            this._rowTranslation,
            interpolate(this._dragX, {
              inputRange: [0, this._friction],
              outputRange: [0, 1],
            })
          ),
          {
            inputRange: [
              sub(
                onGestureEventRightWidth(this._rowWidth, this._rightOffset),
                cond(
                  eq(this._overshootRight, 1),
                  this._overshootRight,
                  this._overshootFriction
                )
              ),
              onGestureEventRightWidth(this._rowWidth, this._rightOffset),
              this._leftWidth,
              add(
                this._leftWidth,
                cond(
                  eq(this._overshootLeft, 1),
                  this._overshootLeft,
                  this._overshootFriction
                )
              ),
            ],
            outputRange: [
              sub(
                onGestureEventRightWidth(this._rowWidth, this._rightOffset),
                cond(
                  eq(this._overshootRight, 1),
                  this._overshootRight,
                  onGestureEventIfOvershootFrictionGreaterThanOne(
                    this._overshootFriction
                  )
                )
              ),
              onGestureEventRightWidth(this._rowWidth, this._rightOffset),
              this._leftWidth,
              add(
                this._leftWidth,
                cond(
                  eq(this._overshootLeft, 1),
                  this._overshootLeft,
                  onGestureEventIfOvershootFrictionGreaterThanOne(
                    this._overshootFriction
                  )
                )
              ),
            ],
          }
        )
      )
    ),
    this._setShowLeftAction,
    this._translateLeftAction,
    this._setShowRightAction,
    this._translateRightAction,
  ]);

  /**
   * Handler for PanGestureHandler onGestureEvent event;
   */
  private _onGestureEvent = event<PanGestureHandlerGestureEvent>([
    {
      nativeEvent: ({ translationX }) =>
        block([set(this._dragX, translationX)]),
    },
  ]);

  /**
   * Handler for TapGestureHandler onHandlerStateChange event;
   */
  private _onTapHandlerStateChange = event<TapGestureHandlerStateChangeEvent>([
    {
      nativeEvent: ({ oldState }) =>
        cond(
          eq(oldState, State.ACTIVE),
          // manually trigger close method;
          set(this._closeValue, multiply(this._closeValue, -1))
        ),
    },
  ]);

  /**
   * Handler for PanGestureHandler onHandlerStateChange event;
   */
  private _onHandlerStateChange = event<PanGestureHandlerStateChangeEvent>([
    {
      nativeEvent: ({ oldState, translationX, velocityX }) =>
        block([
          set(this._oldPanGestureState, oldState),
          cond(
            eq(oldState, State.ACTIVE),
            this._handleRelease(translationX, velocityX)
          ),
        ]),
    },
  ]);

  /**
   * This method runs after changing leftActions width;
   */
  private _onLeftLayout = ({ nativeEvent }: LayoutChangeEvent) => {
    this._leftWidth.setValue(nativeEvent.layout.x);
    this._updateAnimatedEvent(this.props, { leftWidth: nativeEvent.layout.x });
  };

  /**
   * This method runs after changing rightActions width;
   */
  private _onRightLayout = ({ nativeEvent }: LayoutChangeEvent) => {
    this._rightOffset.setValue(nativeEvent.layout.x);
    this._updateAnimatedEvent(this.props, {
      rightOffset: nativeEvent.layout.x,
    });
  };

  /**
   * This method runs after changing Swipeable container width;
   */
  private _onRowLayout = ({ nativeEvent }: LayoutChangeEvent) => {
    this._rowWidth.setValue(nativeEvent.layout.width);
    this._updateAnimatedEvent(this.props, {
      rowWidth: nativeEvent.layout.width,
    });
  };

  /**
   * This method enables manual closing;
   */
  close = () => {
    // temporarily disabled
    // this._closeValue.setValue(multiply(this._closeValue, -1));
  };

  /**
   * This method enables manual left swiping;
   */
  openLeft = () => {
    // temporarily disabled
    // this._openLeftValue.setValue(multiply(this._openLeftValue, -1));
  };

  /**
   * This method enables manual right swiping;
   */
  openRight = () => {
    // temporarily disabled
    // this._openRightValue.setValue(multiply(this._openRightValue, -1));
  };

  render() {
    const { children, renderLeftActions, renderRightActions } = this.props;

    const left = renderLeftActions && (
      <Animated.View
        style={[
          styles.leftActions,
          { transform: [{ translateX: this._leftActionTranslate }] },
        ]}
      >
        {renderLeftActions(this._showLeftAction, this._transX)}
        <View onLayout={this._onLeftLayout} />
      </Animated.View>
    );

    const right = renderRightActions && (
      <Animated.View
        style={[
          styles.rightActions,
          { transform: [{ translateX: this._rightActionTranslate }] },
        ]}
      >
        {renderRightActions(this._showRightAction, this._transX)}
        <View onLayout={this._onRightLayout} />
      </Animated.View>
    );

    return (
      <PanGestureHandler
        activeOffsetX={[-10, 10]}
        {...this.props}
        onGestureEvent={this._onGestureEvent}
        onHandlerStateChange={this._onHandlerStateChange}
      >
        <Animated.View
          onLayout={this._onRowLayout}
          style={[styles.container, this.props.containerStyle]}
        >
          {left}
          {right}
          <TapGestureHandler
            enabled={this.state.rowState !== 0}
            onHandlerStateChange={this._onTapHandlerStateChange}
          >
            <Animated.View
              pointerEvents={cond(eq(this._rowState, 0), 'auto', 'box-only')}
              style={[
                {
                  transform: [
                    {
                      translateX: block([
                        // debug('this._transX', this._transX),
                        this._transX,
                      ]),
                    },
                  ],
                },
                this.props.childrenContainerStyle,
              ]}
            >
              {children}
            </Animated.View>
          </TapGestureHandler>
          {/** FOR CLOSE METHOD - temporarily disabled */}
          {/* <Animated.Code
            exec={block([
              onChange(
                this._closeValue,
                cond(neq(this._rowState, 0), [
                  this._animateRow(this._currentOffset, 0),
                  set(this._transX, this._animateRowSpringPosition),
                ])
              ),
            ])}
          /> */}
          {/** FOR OPEN LEFT METHOD - temporarily disabled */}
          {/* <Animated.Code
            exec={block([
              onChange(
                this._openLeftValue,
                cond(neq(this._leftWidth, 0), [
                  this._animateRow(this._currentOffset, this._leftWidth),
                  set(this._transX, this._animateRowSpringPosition),
                ])
              ),
            ])}
          /> */}
          {/** FOR OPEN RIGHT METHOD - temporarily disabled */}
          {/* <Animated.Code
            exec={block([
              onChange(
                this._openRightValue,
                cond(neq(this._rightOffset, 0), [
                  this._animateRow(
                    this._currentOffset,
                    multiply(sub(this._rowWidth, this._rightOffset), -1)
                  ),
                  set(this._transX, this._animateRowSpringPosition),
                ])
              ),
            ])}
          /> */}
          {/** FOR SETTING this._transX when component is dragged (1. onChange) and released (2. onChange) */}
          <Animated.Code
            exec={block([
              onChange(this._dragX, this._translateX),
              onChange(this._animateRowSpringPosition, this._translateX),
            ])}
          />
        </Animated.View>
      </PanGestureHandler>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
  },
  leftActions: {
    ...StyleSheet.absoluteFillObject,
    flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
  },
  rightActions: {
    ...StyleSheet.absoluteFillObject,
    flexDirection: I18nManager.isRTL ? 'row' : 'row-reverse',
  },
});

export default Swipeable;
