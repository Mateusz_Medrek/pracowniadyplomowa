import React from 'react';
import { StyleProp, StyleSheet, ViewStyle } from 'react-native';
import Animated, { Easing } from 'react-native-reanimated';

import GenericTouchable, {
  PublicProps,
  TOUCHABLE_STATE,
} from './GenericTouchable';

const {
  Clock,
  Value,
  block,
  clockRunning,
  cond,
  eq,
  onChange,
  or,
  set,
  startClock,
  stopClock,
  timing,
} = Animated;

interface TouchableOpacityProps extends PublicProps {
  activeOpacity: number;
  style?: StyleProp<ViewStyle>;
}

/**
 * This component is based on the similar from RNGH repo
 * Difference is in typescript types instead of prop-types and
 * Animated module from react-native-reanimated instead of RN's core;
 *
 * {@link https://github.com/software-mansion/react-native-gesture-handler/blob/master/touchables/TouchableOpacity.js}
 *
 * TouchableOpacity bases on timing animation which has been used in RN's core
 */
class TouchableOpacity extends React.PureComponent<TouchableOpacityProps> {
  static defaultProps = {
    ...GenericTouchable.defaultProps,
    activeOpacity: 0.2,
  };

  private clock = new Clock();
  private triggerOpacityAnimation: Animated.Value<TOUCHABLE_STATE> = new Value(
    TOUCHABLE_STATE.UNDETERMINED
  );
  private activeOpacity: Animated.Value<number> = new Value(
    this.props.activeOpacity
  );
  /** opacity is 1 one by default but could be overwritten */
  private childStyleOpacity = StyleSheet.flatten(this.props.style).opacity ?? 1;

  componentDidUpdate(prevProps: TouchableOpacityProps) {
    if (this.props.activeOpacity !== prevProps.activeOpacity) {
      this.activeOpacity.setValue(this.props.activeOpacity);
    }
    if (
      StyleSheet.flatten(this.props.style).opacity !==
      StyleSheet.flatten(prevProps.style).opacity
    ) {
      this.childStyleOpacity =
        StyleSheet.flatten(this.props.style).opacity ?? 1;
    }
  }

  private finalOpacity: Animated.Value<number> = new Value(
    this.childStyleOpacity
  );
  private prevOpacity: Animated.Value<number> = new Value(
    this.childStyleOpacity
  );

  private timing = (
    value: Animated.Adaptable<number>,
    dest: Animated.Adaptable<number>,
    duration: Animated.Adaptable<number>
  ) => {
    const state = {
      finished: new Value<number>(0),
      frameTime: new Value<number>(0),
      position: new Value<number>(0),
      time: new Value<number>(0),
    };
    const config = {
      duration,
      easing: Easing.inOut(Easing.quad),
      toValue: new Value<number>(0),
    };
    return block([
      // if clock is not running, we reset all parameters and start clock;
      cond(clockRunning(this.clock), 0, [
        set(state.finished, 0),
        set(state.time, 0),
        set(state.position, value),
        set(state.frameTime, 0),
        set(config.toValue, dest),
        startClock(this.clock),
      ]),
      timing(this.clock, state, config),
      cond(state.finished, [stopClock(this.clock)]),
      state.position,
    ]);
  };

  private setOpacityTo = (
    toValue: Animated.Adaptable<number>,
    duration: Animated.Adaptable<number>
  ) => set(this.finalOpacity, this.timing(this.prevOpacity, toValue, duration));

  private onStateChange = (
    oldState: TOUCHABLE_STATE,
    newState: TOUCHABLE_STATE
  ) => {
    this.triggerOpacityAnimation.setValue(newState);
  };

  render() {
    const { style, ...rest } = this.props;
    return (
      <>
        <GenericTouchable
          {...rest}
          style={[StyleSheet.flatten(style), { opacity: this.finalOpacity }]}
          onStateChange={this.onStateChange}
        >
          {this.props.children}
        </GenericTouchable>
        <Animated.Code
          exec={block([
            onChange(
              this.triggerOpacityAnimation,
              cond(
                eq(this.triggerOpacityAnimation, TOUCHABLE_STATE.BEGAN),
                this.setOpacityTo(this.activeOpacity, 0),
                cond(
                  or(
                    eq(
                      this.triggerOpacityAnimation,
                      TOUCHABLE_STATE.MOVED_OUTSIDE
                    ),
                    eq(
                      this.triggerOpacityAnimation,
                      TOUCHABLE_STATE.UNDETERMINED
                    )
                  ),
                  this.setOpacityTo(this.childStyleOpacity, 150)
                )
              )
            ),
          ])}
        />
      </>
    );
  }
}

export default TouchableOpacity;
