import { useEffect, useState } from 'react';
import { Dimensions, ScaledSize } from 'react-native';

const useDimensions = () => {
  const [containerHeight, setContainerHeight] = useState(
    Dimensions.get('screen').height
  );
  const [containerWidth, setContainerWidth] = useState(
    Dimensions.get('screen').width
  );
  useEffect(() => {
    const handler = ({ screen }: { screen: ScaledSize }) => {
      setContainerHeight(screen.height);
      setContainerWidth(screen.width);
    };
    Dimensions.addEventListener('change', handler);
    return () => {
      Dimensions.removeEventListener('change', handler);
    };
  }, []);
  return { containerHeight, containerWidth };
};

export default useDimensions;
