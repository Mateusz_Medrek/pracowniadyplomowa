import { useCallback } from 'react';

import { GET_CURRENT_USER } from '../api/routes';
import server from '../api/server';
import { IUser, TokenState } from '../store';

const useCurrentUser = (tokenState: TokenState): [() => Promise<IUser>] => {
  const getCurrentUser = useCallback(async () => {
    try {
      const response = await server.get<IUser>(`/${GET_CURRENT_USER}`, {
        headers: {
          authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
        },
      });
      return response.data;
    } catch (err) {
      return Promise.reject(err.message);
    }
  }, [tokenState]);
  return [getCurrentUser];
};

export default useCurrentUser;
