import { useNavigation } from '@react-navigation/native';
import i18n from 'i18n-js';
import React, { useRef, useReducer } from 'react';
import { ScrollView, StyleSheet, Text, TextInput, View } from 'react-native';
import Animated from 'react-native-reanimated';
import { useDispatch, useSelector } from 'react-redux';

import { TranslationsKeys } from '../../translations/keys';
import {
  CHANGE_EMAIL,
  CHANGE_NAME,
  CHANGE_PASSWORD,
  GET_CURRENT_USER,
} from '../api/routes';
import server from '../api/server';
import TouchableOpacity from '../animations/TouchableOpacity';
import runTiming from '../animations/runTiming';
import ChangeUserDetailsForm from '../components/ChangeUserDetailsForm';
import ReanimatedModal from '../components/ReanimatedModal';
import useDimensions from '../hooks/useDimensions';
import settingsReducer, {
  INITIAL_STATE,
} from '../localReducers/settingsReducer';
import { SettingsScreenNavigationProp } from '../navigation/types';
import { Dispatch, IUser, RootState, TokenState, UserState } from '../store';
import { COLORS, STYLESHEET } from '../styles';

const regex = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);

const { Clock, Value, block, set, useCode } = Animated;

const SettingsScreen = () => {
  const navigation = useNavigation<SettingsScreenNavigationProp>();
  const { containerWidth } = useDimensions();
  const tokenState = useSelector<RootState, TokenState>(state => state.token);
  const userState = useSelector<RootState, UserState>(state => state.user);
  const dispatch = useDispatch<Dispatch>();
  const [state, localDispatch] = useReducer(settingsReducer, INITIAL_STATE);
  const clockRef = useRef<Animated.Clock>(new Clock());
  const emailClockRef = useRef<Animated.Clock>(new Clock());
  const nameClockRef = useRef<Animated.Clock>(new Clock());
  const passwordClockRef = useRef<Animated.Clock>(new Clock());
  const animateButtonsRef = useRef<Animated.Value<number>>(new Value(0));
  const animateButtonsOffsetRef = useRef<Animated.Value<number>>(new Value(0));
  const shouldAnimateButtonsUpOrDown = useRef<Animated.Value<number>>(
    new Value(0)
  );
  const shouldAnimateEmailFormInOrOut = useRef<Animated.Value<number>>(
    new Value(0)
  );
  const shouldAnimateNameFormInOrOut = useRef<Animated.Value<number>>(
    new Value(0)
  );
  const shouldAnimatePasswordFormInOrOut = useRef<Animated.Value<number>>(
    new Value(0)
  );
  const animateEmailFormRef = useRef<Animated.Value<number>>(new Value(0));
  const animateEmailFormOffsetRef = useRef<Animated.Value<number>>(
    new Value(0)
  );
  const animateNameFormRef = useRef<Animated.Value<number>>(new Value(0));
  const animateNameFormOffsetRef = useRef<Animated.Value<number>>(new Value(0));
  const animatePasswordFormRef = useRef<Animated.Value<number>>(new Value(0));
  const animatePasswordFormOffsetRef = useRef<Animated.Value<number>>(
    new Value(0)
  );
  const navigateBack = () => {
    navigation.goBack();
  };
  const hideModal = () => {
    localDispatch({ type: 'HIDE_MODAL' });
  };
  const emailCallback = async () => {
    localDispatch({ type: 'SHOW_EMAIL_INDICATOR' });
    if (!state.newEmail) {
      return localDispatch({
        type: 'SET_NEW_EMAIL_ERROR',
        payload: TranslationsKeys.CHANGE_EMAIL_FORM__NEW_EMAIL_NOT_PROVIDED,
      });
    }
    if (!state.password) {
      return localDispatch({
        type: 'SET_NEW_EMAIL_ERROR',
        payload: TranslationsKeys.CHANGE_EMAIL_FORM__PASSWORD_NOT_PROVIDED,
      });
    }
    if (!state.newEmail.match(regex)) {
      return localDispatch({
        type: 'SET_NEW_EMAIL_ERROR',
        payload: TranslationsKeys.CHANGE_EMAIL_FORM__EMAIL_HAS_INVALID_FORMAT,
      });
    }
    try {
      await server.post(
        `/${CHANGE_EMAIL}`,
        {
          email: state.newEmail,
          password: state.password,
        },
        {
          headers: {
            authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
          },
        }
      );
      const userResponse = await server.get<IUser>(`/${GET_CURRENT_USER}`, {
        headers: {
          authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
        },
      });
      dispatch.user.saveUser(userResponse.data);
      hideEmailForm();
      localDispatch({ type: 'EMAIL_CHANGE_COMPLETED' });
    } catch (err) {
      return localDispatch({
        type: 'SET_NEW_EMAIL_ERROR',
        payload: TranslationsKeys.CHANGE_EMAIL_FORM__UNKNOWN_ERROR,
      });
    }
  };
  const nameCallback = async () => {
    localDispatch({ type: 'SHOW_NAME_INDICATOR' });
    if (!state.newName) {
      return localDispatch({
        type: 'SET_NEW_NAME_ERROR',
        payload: TranslationsKeys.CHANGE_NAME_FORM__NEW_NAME_NOT_PROVIDED,
      });
    }
    try {
      await server.post(
        `/${CHANGE_NAME}`,
        { name: state.newName },
        {
          headers: {
            authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
          },
        }
      );
      const userResponse = await server.get<IUser>(`/${GET_CURRENT_USER}`, {
        headers: {
          authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
        },
      });
      dispatch.user.saveUser(userResponse.data);
      hideNameForm();
      localDispatch({ type: 'NAME_CHANGE_COMPLETED' });
    } catch (err) {
      return localDispatch({
        type: 'SET_NEW_NAME_ERROR',
        payload: TranslationsKeys.CHANGE_NAME_FORM__UNKNOWN_ERROR,
      });
    }
  };
  const passwordCallback = async () => {
    localDispatch({ type: 'SHOW_PASSWORD_INDICATOR' });
    if (!state.oldPassword) {
      return localDispatch({
        type: 'SET_NEW_PASSWORD_ERROR',
        payload:
          TranslationsKeys.CHANGE_PASSWORD_FORM__OLD_PASSWORD_NOT_PROVIDED,
      });
    }
    if (!state.newPassword) {
      return localDispatch({
        type: 'SET_NEW_PASSWORD_ERROR',
        payload:
          TranslationsKeys.CHANGE_PASSWORD_FORM__NEW_PASSWORD_NOT_PROVIDED,
      });
    }
    try {
      await server.post(
        `/${CHANGE_PASSWORD}`,
        {
          oldPassword: state.oldPassword,
          newPassword: state.newPassword,
        },
        {
          headers: {
            authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
          },
        }
      );
      hidePasswordForm();
      localDispatch({ type: 'PASSWORD_CHANGE_COMPLETED' });
    } catch (err) {
      return localDispatch({
        type: 'SET_NEW_PASSWORD_ERROR',
        payload: TranslationsKeys.CHANGE_PASSWORD_FORM__UNKNOWN_ERROR,
      });
    }
  };
  const BOX_WIDTH = containerWidth - 20;
  const STARTING_POSITION = 800;
  const moveButtonsUp = () => {
    shouldAnimateButtonsUpOrDown.current.setValue(-100);
  };
  const moveButtonsDown = () => {
    shouldAnimateButtonsUpOrDown.current.setValue(0);
  };
  const showEmailForm = () => {
    moveButtonsUp();
    shouldAnimateEmailFormInOrOut.current.setValue(STARTING_POSITION);
    shouldAnimateNameFormInOrOut.current.setValue(0);
    shouldAnimatePasswordFormInOrOut.current.setValue(0);
  };
  const hideEmailForm = () => {
    moveButtonsDown();
    shouldAnimateEmailFormInOrOut.current.setValue(0);
    localDispatch({ type: 'CLEAR_EMAIL_FORM' });
  };
  const showNameForm = () => {
    moveButtonsUp();
    shouldAnimateEmailFormInOrOut.current.setValue(0);
    shouldAnimateNameFormInOrOut.current.setValue(-STARTING_POSITION);
    shouldAnimatePasswordFormInOrOut.current.setValue(0);
  };
  const hideNameForm = () => {
    moveButtonsDown();
    shouldAnimateNameFormInOrOut.current.setValue(0);
    localDispatch({ type: 'CLEAR_NAME_FORM' });
  };
  const showPasswordForm = () => {
    moveButtonsUp();
    shouldAnimateEmailFormInOrOut.current.setValue(0);
    shouldAnimateNameFormInOrOut.current.setValue(0);
    shouldAnimatePasswordFormInOrOut.current.setValue(-STARTING_POSITION);
  };
  const hidePasswordForm = () => {
    moveButtonsDown();
    shouldAnimatePasswordFormInOrOut.current.setValue(0);
    localDispatch({ type: 'CLEAR_PASSWORD_FORM' });
  };
  // ANIMATING BUTTONS;
  useCode(
    () =>
      block([
        set(
          animateButtonsRef.current,
          runTiming(
            clockRef.current,
            animateButtonsOffsetRef.current,
            shouldAnimateButtonsUpOrDown.current,
            animateButtonsOffsetRef.current,
            300
          )
        ),
      ]),
    [shouldAnimateButtonsUpOrDown.current, animateButtonsRef.current]
  );
  // ANIMATING EMAIL FORM;
  useCode(
    () =>
      block([
        set(
          animateEmailFormRef.current,
          runTiming(
            emailClockRef.current,
            animateEmailFormOffsetRef.current,
            shouldAnimateEmailFormInOrOut.current,
            animateEmailFormOffsetRef.current,
            300
          )
        ),
      ]),
    [shouldAnimateEmailFormInOrOut.current, animateEmailFormRef.current]
  );
  // ANIMATING NAME FORM;
  useCode(
    () =>
      block([
        set(
          animateNameFormRef.current,
          runTiming(
            nameClockRef.current,
            animateNameFormOffsetRef.current,
            shouldAnimateNameFormInOrOut.current,
            animateNameFormOffsetRef.current,
            300
          )
        ),
      ]),
    [shouldAnimateNameFormInOrOut.current, animateNameFormRef.current]
  );
  // ANIMATING PASSWORD FORM;
  useCode(
    () =>
      block([
        set(
          animatePasswordFormRef.current,
          runTiming(
            passwordClockRef.current,
            animatePasswordFormOffsetRef.current,
            shouldAnimatePasswordFormInOrOut.current,
            animatePasswordFormOffsetRef.current,
            300
          )
        ),
      ]),
    [shouldAnimatePasswordFormInOrOut.current, animatePasswordFormRef.current]
  );
  if (!userState) {
    return null;
  }
  return (
    <View style={STYLESHEET.screenContainer}>
      <ScrollView nestedScrollEnabled={true}>
        <View style={STYLESHEET.screenContainer}>
          <View style={styles.settingsContainer}>
            <Animated.View
              style={[
                styles.buttonsContainer,
                { transform: [{ translateY: animateButtonsRef.current }] },
              ]}
            >
              <TouchableOpacity
                onPress={showEmailForm}
                style={[STYLESHEET.baseButton, { width: containerWidth - 20 }]}
              >
                <Text style={[STYLESHEET.baseText, styles.selectButtonText]}>
                  {i18n.t(
                    TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_EMAIL
                  )}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={showNameForm}
                style={[STYLESHEET.baseButton, { width: containerWidth - 20 }]}
              >
                <Text style={[STYLESHEET.baseText, styles.selectButtonText]}>
                  {i18n.t(
                    TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_NAME
                  )}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={showPasswordForm}
                style={[STYLESHEET.baseButton, { width: containerWidth - 20 }]}
              >
                <Text style={[STYLESHEET.baseText, styles.selectButtonText]}>
                  {i18n.t(
                    TranslationsKeys.SETTINGS_SCREEN__WANT_TO_CHANGE_PASSWORD
                  )}
                </Text>
              </TouchableOpacity>
            </Animated.View>
            <View style={[styles.formsContainer, { width: BOX_WIDTH }]}>
              <Animated.View
                style={[
                  styles.formContainer,
                  {
                    left: -STARTING_POSITION,
                    transform: [{ translateX: animateEmailFormRef.current }],
                    width: BOX_WIDTH,
                  },
                ]}
              >
                <ChangeUserDetailsForm
                  headerDetails={[
                    i18n.t(TranslationsKeys.CHANGE_EMAIL_FORM__CURRENT_EMAIL),
                    userState.email,
                  ]}
                  hideForm={hideEmailForm}
                  onSubmit={emailCallback}
                  showIndicator={state.shouldShowEmailIndicator}
                  submitButtonText={i18n.t(
                    TranslationsKeys.CHANGE_EMAIL_FORM__CHANGE_EMAIL
                  )}
                >
                  <>
                    <TextInput
                      autoCapitalize="none"
                      autoCorrect={false}
                      onChangeText={payload =>
                        localDispatch({ type: 'SET_NEW_EMAIL', payload })
                      }
                      placeholder={i18n.t(
                        TranslationsKeys.CHANGE_EMAIL_FORM__NEW_EMAIL_INPUT_PLACEHOLDER
                      )}
                      placeholderTextColor={COLORS.WHITE}
                      style={[
                        STYLESHEET.baseInput,
                        { width: containerWidth - 20 },
                      ]}
                      value={state.newEmail}
                    />
                    <TextInput
                      autoCapitalize="none"
                      autoCorrect={false}
                      onChangeText={payload =>
                        localDispatch({ type: 'SET_PASSWORD', payload })
                      }
                      placeholder={i18n.t(
                        TranslationsKeys.CHANGE_EMAIL_FORM__PASSWORD_INPUT_PLACEHOLDER
                      )}
                      placeholderTextColor={COLORS.WHITE}
                      secureTextEntry={true}
                      style={[
                        STYLESHEET.baseInput,
                        { width: containerWidth - 20 },
                      ]}
                      value={state.password}
                    />
                    {!!state.newEmailError && (
                      <Text style={STYLESHEET.errorText}>
                        {state.newEmailError}
                      </Text>
                    )}
                  </>
                </ChangeUserDetailsForm>
              </Animated.View>
              <Animated.View
                style={[
                  styles.formContainer,
                  {
                    bottom: -STARTING_POSITION,
                    transform: [{ translateY: animateNameFormRef.current }],
                    width: BOX_WIDTH,
                  },
                ]}
              >
                <ChangeUserDetailsForm
                  headerDetails={[
                    i18n.t(TranslationsKeys.CHANGE_NAME_FORM__CURRENT_NAME),
                    userState.name,
                  ]}
                  hideForm={hideNameForm}
                  onSubmit={nameCallback}
                  showIndicator={state.shouldShowNameIndicator}
                  submitButtonText={i18n.t(
                    TranslationsKeys.CHANGE_NAME_FORM__CHANGE_NAME
                  )}
                >
                  <>
                    <TextInput
                      autoCapitalize="none"
                      autoCorrect={false}
                      onChangeText={payload =>
                        localDispatch({ type: 'SET_NEW_NAME', payload })
                      }
                      placeholder={i18n.t(
                        TranslationsKeys.CHANGE_NAME_FORM__NEW_NAME_INPUT_PLACEHOLDER
                      )}
                      placeholderTextColor={COLORS.WHITE}
                      style={[
                        STYLESHEET.baseInput,
                        { width: containerWidth - 20 },
                      ]}
                      value={state.newName}
                    />
                    {!!state.newNameError && (
                      <Text style={STYLESHEET.errorText}>
                        {state.newNameError}
                      </Text>
                    )}
                  </>
                </ChangeUserDetailsForm>
              </Animated.View>
              <Animated.View
                style={[
                  styles.formContainer,
                  {
                    right: -STARTING_POSITION,
                    transform: [{ translateX: animatePasswordFormRef.current }],
                    width: BOX_WIDTH,
                  },
                ]}
              >
                <ChangeUserDetailsForm
                  hideForm={hidePasswordForm}
                  onSubmit={passwordCallback}
                  showIndicator={state.shouldShowPasswordIndicator}
                  submitButtonText={i18n.t(
                    TranslationsKeys.CHANGE_PASSWORD_FORM__CHANGE_PASSWORD
                  )}
                >
                  <>
                    <TextInput
                      autoCapitalize="none"
                      autoCorrect={false}
                      onChangeText={payload =>
                        localDispatch({ type: 'SET_OLD_PASSWORD', payload })
                      }
                      placeholder={i18n.t(
                        TranslationsKeys.CHANGE_PASSWORD_FORM__OLD_PASSWORD_INPUT_PLACEHOLDER
                      )}
                      placeholderTextColor={COLORS.WHITE}
                      secureTextEntry={true}
                      style={[
                        STYLESHEET.baseInput,
                        { width: containerWidth - 20 },
                      ]}
                      value={state.oldPassword}
                    />
                    <TextInput
                      autoCapitalize="none"
                      autoCorrect={false}
                      onChangeText={payload =>
                        localDispatch({ type: 'SET_NEW_PASSWORD', payload })
                      }
                      placeholder={i18n.t(
                        TranslationsKeys.CHANGE_PASSWORD_FORM__NEW_PASSWORD_INPUT_PLACEHOLDER
                      )}
                      placeholderTextColor={COLORS.WHITE}
                      secureTextEntry={true}
                      style={[
                        STYLESHEET.baseInput,
                        { width: containerWidth - 20 },
                      ]}
                      value={state.newPassword}
                    />
                    {!!state.newPasswordError && (
                      <Text style={STYLESHEET.errorText}>
                        {state.newPasswordError}
                      </Text>
                    )}
                  </>
                </ChangeUserDetailsForm>
              </Animated.View>
            </View>
          </View>
          <View style={styles.backButtonContainer}>
            <TouchableOpacity
              onPress={navigateBack}
              style={[
                STYLESHEET.baseButton,
                STYLESHEET.mediumBlueButton,
                { width: containerWidth - 20 },
              ]}
            >
              <Text style={[STYLESHEET.baseText, STYLESHEET.saveButtonText]}>
                {i18n.t(TranslationsKeys.SETTINGS_SCREEN__RETURN)}
              </Text>
            </TouchableOpacity>
          </View>
          <ReanimatedModal
            isVisible={state.shouldShowModal}
            modalButtonText="OK"
            modalText={state.modalText}
            onModalClose={hideModal}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  backButtonContainer: {
    justifyContent: 'flex-end',
    marginTop: 255,
  },
  buttonsContainer: {
    marginTop: 100,
  },
  formContainer: {
    alignItems: 'center',
    height: 300,
    justifyContent: 'center',
    position: 'absolute',
  },
  formsContainer: {
    height: 300,
    marginTop: 150,
    position: 'absolute',
    zIndex: -1,
  },
  selectButtonText: {
    paddingHorizontal: 5,
  },
  settingsContainer: {
    alignItems: 'center',
    flex: 1,
    paddingTop: 10,
  },
});

export default SettingsScreen;
