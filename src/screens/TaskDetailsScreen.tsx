import { useRoute } from '@react-navigation/native';
import i18n from 'i18n-js';
import React, { useCallback, useEffect, useState } from 'react';
import {
  FlatList,
  ListRenderItem,
  ListRenderItemInfo,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useSelector } from 'react-redux';

import { TranslationsKeys } from '../../translations/keys';
import TouchableOpacity from '../animations/TouchableOpacity';
import { CHECK_SUBTASK, GET_TASK, UNCHECK_SUBTASK } from '../api/routes';
import server from '../api/server';
import useDimensions from '../hooks/useDimensions';
import { TaskDetailsScreenRouteProp } from '../navigation/types';
import { ISubTask, ITask, RootState, TokenState, UserState } from '../store';
import { COLORS, CONSTS, STYLESHEET, withShadow } from '../styles';

const TaskDetailsScreen = () => {
  const route = useRoute<TaskDetailsScreenRouteProp>();
  const tokenState = useSelector<RootState, TokenState>(state => state.token);
  const userState = useSelector<RootState, UserState>(state => state.user);
  const { containerWidth } = useDimensions();
  const [taskDetails, setTaskDetails] = useState<ITask | null>(null);
  const [refresh, setRefresh] = useState(false); // Any time it changes value, FlatList re-renders itself;
  const getTaskDetails = async () => {
    const id = route.params.id;
    try {
      const response = await server.get<ITask>(`/${GET_TASK}/${id}`, {
        headers: {
          authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
        },
      });
      setTaskDetails({
        title: response.data.title,
        description: response.data.description,
        startingTime: new Date(response.data.startingTime),
        finishingTime: new Date(response.data.finishingTime),
        subTasks: response.data.subTasks,
      });
    } catch (err) {
      console.log(err.message);
    }
  };
  const checkingTask = (subTaskId: number, isCompleted?: boolean) => {
    setTaskDetails(prevTaskDetails => {
      prevTaskDetails &&
        (prevTaskDetails.subTasks = prevTaskDetails.subTasks.map(subtask => {
          subtask.id === subTaskId && (subtask.isCompleted = !isCompleted);
          return subtask;
        }));
      return prevTaskDetails;
    });
    setRefresh(prev => !prev);
  };
  const onSubtaskChecking = async (
    subTaskId: number,
    isCompleted?: boolean
  ) => {
    const id = route.params.id;
    try {
      await server.post(
        `/${!isCompleted ? CHECK_SUBTASK : UNCHECK_SUBTASK}`,
        { id, subTaskId },
        {
          headers: {
            authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
          },
        }
      );
      checkingTask(subTaskId, isCompleted);
    } catch (err) {
      console.log(err.message);
    }
  };
  const renderItem: ListRenderItem<ISubTask> = useCallback(
    ({ item }: ListRenderItemInfo<ISubTask>) => {
      const executor =
        item.assigneeId === userState?.id
          ? i18n.t(TranslationsKeys.TASK_DETAILS_SCREEN__ME)
          : // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            userState?.friends.find(friend => friend.id === item.assigneeId)!
              .name;
      return (
        <View
          key={item.id}
          style={[
            styles.subTasksFlatlistItemContainer,
            item.isCompleted ? styles.checkedItem : styles.uncheckedItem,
          ]}
        >
          <Text style={styles.category}>{item.category}</Text>
          <View style={[STYLESHEET.rowCenterSpaceBetween, styles.row]}>
            <View style={styles.executorAndName}>
              <Text
                style={[STYLESHEET.baseText, styles.subTasksFlatListItemText]}
              >
                {item.title}
              </Text>
              <Text
                style={[STYLESHEET.baseText, styles.subTasksFlatListItemText]}
              >
                {i18n.t(TranslationsKeys.TASK_DETAILS_SCREEN__EXECUTOR) +
                  executor}
              </Text>
            </View>
            {item.isCompleted && (
              <Icon
                color={COLORS.GREEN}
                name="check-circle"
                size={30}
                style={styles.checkIcon}
              />
            )}
            <TouchableOpacity
              onPress={() => onSubtaskChecking(item.id, item.isCompleted)}
              style={styles.checkIcon}
            >
              {item.isCompleted ? (
                <Icon color={COLORS.BLACK} name="restore" size={30} />
              ) : (
                <Icon
                  color={COLORS.BLACK}
                  name="checkbox-blank-circle-outline"
                  size={30}
                />
              )}
            </TouchableOpacity>
          </View>
        </View>
      );
    },
    [userState] // eslint-disable-line react-hooks/exhaustive-deps
  );
  useEffect(() => {
    getTaskDetails();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  if (!taskDetails) {
    return null;
  }
  return (
    <View style={STYLESHEET.screenContainer}>
      <ScrollView nestedScrollEnabled={true}>
        <View
          style={[
            STYLESHEET.rowCenterFlexStart,
            styles.row,
            { width: containerWidth - 20 },
          ]}
        >
          <Text style={STYLESHEET.baseText}>
            {i18n.t(TranslationsKeys.TASK_DETAILS_SCREEN__TITLE_HEADER)}
          </Text>
          <Text style={[STYLESHEET.baseText, styles.rowText]}>
            {taskDetails.title}
          </Text>
        </View>
        <View
          style={[
            STYLESHEET.rowCenterFlexStart,
            styles.row,
            { width: containerWidth - 20 },
          ]}
        >
          <Text style={STYLESHEET.baseText}>
            {i18n.t(TranslationsKeys.TASK_DETAILS_SCREEN__DESCRIPTION_HEADER)}
          </Text>
          <Text
            style={[
              STYLESHEET.baseText,
              styles.rowText,
              styles.descriptionText,
            ]}
          >
            {taskDetails.description}
          </Text>
        </View>
        <View
          style={[
            STYLESHEET.rowCenterSpaceBetween,
            styles.row,
            { width: containerWidth - 20 },
          ]}
        >
          <Text style={STYLESHEET.baseText}>
            {i18n.t(
              TranslationsKeys.TASK_DETAILS_SCREEN__STARTING_DATETIME_HEADER
            )}
          </Text>
          <Text style={[STYLESHEET.baseText, styles.rowText]}>
            {`${taskDetails.startingTime.toLocaleDateString(
              i18n.locale
            )} ${taskDetails.startingTime.toLocaleTimeString(i18n.locale)}`}
          </Text>
        </View>
        <View
          style={[
            STYLESHEET.rowCenterSpaceBetween,
            styles.row,
            { width: containerWidth - 20 },
          ]}
        >
          <Text style={STYLESHEET.baseText}>
            {i18n.t(
              TranslationsKeys.TASK_DETAILS_SCREEN__FINISHING_DATETIME_HEADER
            )}
          </Text>
          <Text style={[STYLESHEET.baseText, styles.rowText]}>
            {`${taskDetails.finishingTime.toLocaleDateString(
              i18n.locale
            )} ${taskDetails.finishingTime.toLocaleTimeString(i18n.locale)}`}
          </Text>
        </View>
        <FlatList
          contentContainerStyle={[
            STYLESHEET.baseFlatList,
            { width: containerWidth - 20 },
          ]}
          data={taskDetails.subTasks}
          extraData={refresh}
          keyExtractor={item => item.id.toString()}
          renderItem={renderItem}
        />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  category: {
    backgroundColor: COLORS.LIGHTBLUE,
    borderRadius: 10,
    color: COLORS.WHITE,
    fontSize: CONSTS.FONT_SIZE,
    paddingHorizontal: 20,
    paddingVertical: 5,
    position: 'absolute',
    right: -5,
    top: -5,
  },
  checkIcon: {
    padding: 5,
    marginTop: 15,
  },
  checkedItem: {
    backgroundColor: COLORS.LIGHTGRAY,
    opacity: 0.4,
  },
  descriptionText: {
    flex: 1,
    paddingRight: 10,
  },
  executorAndName: {
    flex: 1,
    marginRight: 40,
  },
  row: {
    marginHorizontal: 10,
    marginVertical: 5,
  },
  rowText: {
    opacity: 0.6,
    paddingHorizontal: 10,
    paddingVertical: 5,
    textAlign: 'left',
  },
  subTasksFlatlistItemContainer: {
    marginTop: 10,
    opacity: 0.8,
    padding: 5,
    ...withShadow(),
  },
  subTasksFlatListItemText: {
    textAlign: 'left',
  },
  uncheckedItem: {
    backgroundColor: COLORS.MEDIUMLIGHTBLUE,
  },
});

export default TaskDetailsScreen;
