import { useNavigation, useRoute } from '@react-navigation/native';
import i18n from 'i18n-js';
import React, { useEffect, useRef, useReducer } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { TranslationsKeys } from '../../translations/keys';
import TouchableOpacity from '../animations/TouchableOpacity';
import { CREATE_TASK, EDIT_TASK } from '../api/routes';
import server from '../api/server';
import DateTimePicker from '../components/DateTimePicker';
import ReanimatedDrawerLayout from '../components/ReanimatedDrawerLayout';
import Header from '../components/Header';
import ReanimatedModal from '../components/ReanimatedModal';
import SubTasksList from '../components/SubTasksList';
import useCurrentUser from '../hooks/useCurrentUser';
import useDimensions from '../hooks/useDimensions';
import createTaskReducer, {
  initState,
} from '../localReducers/createTaskReducer';
import {
  CreateTaskScreenNavigationProp,
  CreateTaskScreenRouteProp,
} from '../navigation/types';
import { Dispatch, ITask, RootState, TokenState, UserState } from '../store';
import { COLORS, CONSTS, STYLESHEET, withShadow } from '../styles';

const CreateTaskScreen = () => {
  const navigation = useNavigation<CreateTaskScreenNavigationProp>();
  const route = useRoute<CreateTaskScreenRouteProp>();
  const taskData = route.params && route.params.taskData;
  useSelector<RootState, string>(state => state.locale);
  const tokenState = useSelector<RootState, TokenState>(state => state.token);
  const userState = useSelector<RootState, UserState>(state => state.user);
  const dispatch = useDispatch<Dispatch>();
  const [getCurrentUser] = useCurrentUser(tokenState);
  const { containerWidth } = useDimensions();
  const drawerLayoutRef = useRef<ReanimatedDrawerLayout>(null);
  const [state, localDispatch] = useReducer(
    createTaskReducer,
    taskData,
    initState
  );
  const hideModal = () => {
    localDispatch({ type: 'HIDE_MODAL' });
    if (!taskData) {
      navigation.navigate('Home');
    } else {
      navigation.goBack();
    }
  };
  const onCreateSubmit = async () => {
    try {
      const addTaskBody: ITask = {
        title: state.taskTitle,
        description: state.taskDescription,
        startingTime: state.startingDatetime,
        finishingTime: state.finishingDatetime,
        subTasks: state.subTasks,
      };
      await server.post(`/${CREATE_TASK}`, addTaskBody, {
        headers: {
          authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
        },
      });
      dispatch.user.saveUser(await getCurrentUser());
      return localDispatch({ type: 'CREATE_EDIT_TASK_COMPLETED' });
    } catch (err) {
      localDispatch({ type: 'CREATE_EDIT_TASK_FAILED', payload: err.message });
    }
  };
  const onEditSubmit = async () => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const data = taskData!;
    try {
      const editTaskBody: Partial<ITask> & { id?: string } = {
        id: data.id,
      };
      if (data.title !== state.taskTitle) {
        editTaskBody.title = state.taskTitle;
      }
      if (data.description !== state.taskDescription) {
        editTaskBody.description = state.taskDescription;
      }
      if (data.startingTime !== state.startingDatetime) {
        editTaskBody.startingTime = state.startingDatetime;
      }
      if (data.finishingTime !== state.finishingDatetime) {
        editTaskBody.finishingTime = state.finishingDatetime;
      }
      if (data.subTasks.length !== state.subTasks.length) {
        editTaskBody.subTasks = state.subTasks;
      } else {
        for (let i = 0; i < data.subTasks.length; i++) {
          if (
            data.subTasks[i].title !== state.subTasks[i].title ||
            data.subTasks[i].category !== state.subTasks[i].category ||
            data.subTasks[i].id !== state.subTasks[i].id ||
            data.subTasks[i].assigneeId !== state.subTasks[i].assigneeId
          ) {
            editTaskBody.subTasks = state.subTasks;
            break;
          }
        }
      }
      await server.post(`/${EDIT_TASK}`, editTaskBody, {
        headers: {
          authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
        },
      });
      dispatch.user.saveUser(await getCurrentUser());
      return localDispatch({ type: 'CREATE_EDIT_TASK_COMPLETED' });
    } catch (err) {
      localDispatch({ type: 'CREATE_EDIT_TASK_FAILED', payload: err.message });
    }
  };
  const onSubmit = () => {
    if (!state.taskTitle) {
      return localDispatch({
        type: 'SET_TASK_TITLE_ERROR',
        payload: TranslationsKeys.CREATE_TASK_SCREEN__TITLE_NOT_PROVIDED,
      });
    }
    if (!state.taskDescription) {
      return localDispatch({
        type: 'SET_TASK_DESCRIPTION_ERROR',
        payload: TranslationsKeys.CREATE_TASK_SCREEN__DESCRIPTION_NOT_PROVIDED,
      });
    }
    localDispatch({ type: 'SHOW_INDICATOR' });
    if (!taskData) {
      return onCreateSubmit();
    }
    return onEditSubmit();
  };
  const setFinishingLaterThanStarting = (startingDatetime: Date) => {
    startingDatetime.setSeconds(0);
    localDispatch({
      type: 'SET_FINISHING_DATETIME',
      payload: startingDatetime,
    });
  };
  useEffect(() => {
    if (state.finishingDatetime > state.startingDatetime) {
      return;
    }
    setFinishingLaterThanStarting(state.startingDatetime);
  }, [state.startingDatetime, state.finishingDatetime]);
  if (!userState) {
    return null;
  }
  const renderContent = () => (
    <View style={STYLESHEET.screenContainer}>
      {!taskData && (
        <Header
          onDrawerButtonPress={drawerLayoutRef.current?.openDrawer}
          title={i18n.t(TranslationsKeys.CREATE_TASK_SCREEN__HEADER)}
        />
      )}
      <View style={STYLESHEET.baseContainer}>
        <View
          style={[styles.titleInputContainer, { width: containerWidth - 20 }]}
        >
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            onChangeText={payload =>
              localDispatch({ type: 'SET_TASK_TITLE', payload })
            }
            placeholder={i18n.t(
              TranslationsKeys.CREATE_TASK_SCREEN__TITLE_INPUT_PLACEHOLDER
            )}
            placeholderTextColor={COLORS.WHITE}
            style={STYLESHEET.baseInput}
            value={state.taskTitle}
          />
          {!!state.taskTitleError && (
            <Text style={STYLESHEET.errorText}>{state.taskTitleError}</Text>
          )}
        </View>
        <View
          style={[styles.descriptionContainer, { width: containerWidth - 20 }]}
        >
          <TextInput
            autoCapitalize="sentences"
            autoCorrect={true}
            multiline={true}
            numberOfLines={10}
            onChangeText={payload =>
              localDispatch({ type: 'SET_TASK_DESCRIPTION', payload })
            }
            placeholder={i18n.t(
              TranslationsKeys.CREATE_TASK_SCREEN__DESCRIPTION_INPUT_PLACEHOLDER
            )}
            placeholderTextColor={COLORS.WHITE}
            style={[STYLESHEET.baseText, styles.descriptionInput]}
            value={state.taskDescription}
          />
          {!!state.taskDescriptionError && (
            <Text style={STYLESHEET.errorText}>
              {state.taskDescriptionError}
            </Text>
          )}
        </View>
        {userState && (
          <SubTasksList
            dispatch={localDispatch}
            subTasks={state.subTasks}
            user={userState}
          />
        )}
        <View
          style={[
            STYLESHEET.rowCenterSpaceBetween,
            styles.datetimeContainer,
            { width: containerWidth - 20 },
          ]}
        >
          <DateTimePicker
            dateTime={state.startingDatetime}
            title={i18n.t(
              TranslationsKeys.CREATE_TASK_SCREEN__SET_STARTING_DATE
            )}
            setDateTime={payload =>
              localDispatch({ type: 'SET_STARTING_DATETIME', payload })
            }
          />
          <Text style={[STYLESHEET.baseText, styles.datetimeText]}>
            {`${state.startingDatetime.toLocaleDateString(
              i18n.locale
            )} ${state.startingDatetime.toLocaleTimeString(i18n.locale)}`}
          </Text>
        </View>
        <View
          style={[
            STYLESHEET.rowCenterSpaceBetween,
            styles.datetimeContainer,
            { width: containerWidth - 20 },
          ]}
        >
          <DateTimePicker
            dateTime={state.finishingDatetime}
            title={i18n.t(
              TranslationsKeys.CREATE_TASK_SCREEN__SET_FINISHING_DATE
            )}
            setDateTime={payload =>
              localDispatch({ type: 'SET_FINISHING_DATETIME', payload })
            }
          />
          <Text style={[STYLESHEET.baseText, styles.datetimeText]}>
            {`${state.finishingDatetime.toLocaleDateString(
              i18n.locale
            )} ${state.finishingDatetime.toLocaleTimeString(i18n.locale)}`}
          </Text>
        </View>
        <TouchableOpacity
          onPress={onSubmit}
          style={[
            STYLESHEET.baseButton,
            styles.saveTaskButton,
            { width: containerWidth - 20 },
          ]}
        >
          {!state.shouldShowIndicator ? (
            <Text style={STYLESHEET.baseText}>
              {i18n.t(TranslationsKeys.CREATE_TASK_SCREEN__SAVE_TASK)}
            </Text>
          ) : (
            <ActivityIndicator color={COLORS.WHITE} />
          )}
        </TouchableOpacity>
      </View>
      <ReanimatedModal
        isVisible={state.shouldShowModal}
        modalButtonText="OK"
        modalText={i18n.t(TranslationsKeys.CREATE_TASK_SCREEN__MODAL_MESSAGE)}
        onModalClose={hideModal}
      />
    </View>
  );
  if (!taskData) {
    return (
      <ReanimatedDrawerLayout ref={drawerLayoutRef}>
        {renderContent()}
      </ReanimatedDrawerLayout>
    );
  }
  return renderContent();
};

const styles = StyleSheet.create({
  datetimeContainer: {
    marginVertical: 3,
  },
  datetimeText: {
    paddingRight: 5,
  },
  descriptionContainer: {
    backgroundColor: COLORS.MEDIUMBLUE,
    borderRadius: CONSTS.TILE_BORDERRADIUS,
    color: COLORS.WHITE,
    height: 100,
    marginHorizontal: 10,
    marginVertical: 5,
    paddingHorizontal: 5,
    ...withShadow(),
  },
  descriptionInput: {
    flex: 1,
    opacity: 0.6,
    textAlignVertical: 'top',
  },
  saveTaskButton: {
    borderRadius: CONSTS.TILE_BORDERRADIUS,
  },
  titleInputContainer: {
    backgroundColor: COLORS.MEDIUMBLUE,
    borderRadius: CONSTS.TILE_BORDERRADIUS,
    marginVertical: 5,
    ...withShadow(),
  },
});

export default CreateTaskScreen;
