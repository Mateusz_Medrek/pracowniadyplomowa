import { useRoute } from '@react-navigation/native';
import i18n from 'i18n-js';
import React, { useEffect, useState } from 'react';
import {
  FlatList,
  ListRenderItem,
  ListRenderItemInfo,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { useSelector } from 'react-redux';

import { TranslationsKeys } from '../../translations/keys';
import { GET_USER } from '../api/routes';
import server from '../api/server';
import useDimensions from '../hooks/useDimensions';
import { FriendsDetailScreenRouteProp } from '../navigation/types';
import { IFriend, IUser, RootState, TokenState } from '../store';
import { COLORS, CONSTS, STYLESHEET, withShadow } from '../styles';

export const FlatListEmptyComponent = () => {
  return (
    <Text style={styles.friendsFlatListEmpty}>
      {i18n.t(TranslationsKeys.FRIENDS_DETAIL_SCREEN__NO_FRIENDS)}
    </Text>
  );
};

export const FlatListRenderItemComponent: ListRenderItem<IFriend> = ({
  item,
}: ListRenderItemInfo<IFriend>) => {
  return (
    <Text key={item.id} style={styles.friendsFlatListItem}>
      {item.name}
    </Text>
  );
};

const FriendsDetailScreen = () => {
  const route = useRoute<FriendsDetailScreenRouteProp>();
  const { containerWidth } = useDimensions();
  const tokenState = useSelector<RootState, TokenState>(state => state.token);
  const [userDetails, setUserDetails] = useState<IUser | null>(null);
  const getUserDetails = async () => {
    const id = route.params.id;
    const response = await server.get<IUser>(`/${GET_USER}/${id}`, {
      headers: {
        authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
      },
    });
    setUserDetails(response.data);
  };
  useEffect(() => {
    getUserDetails();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  if (!userDetails) {
    return null;
  }
  return (
    <View style={STYLESHEET.screenContainer}>
      <View
        style={[
          STYLESHEET.rowCenterFlexStart,
          styles.row,
          { width: containerWidth - 20 },
        ]}
      >
        <Text style={STYLESHEET.baseText}>
          {i18n.t(TranslationsKeys.FRIENDS_DETAIL_SCREEN__USER_NAME_HEADER)}
        </Text>
        <Text style={[STYLESHEET.baseText, styles.rowText]}>
          {userDetails.name}
        </Text>
      </View>
      <View
        style={[
          STYLESHEET.rowCenterFlexStart,
          styles.row,
          { width: containerWidth - 20 },
        ]}
      >
        <Text style={STYLESHEET.baseText}>
          {i18n.t(TranslationsKeys.FRIENDS_DETAIL_SCREEN__EMAIL_HEADER)}
        </Text>
        <Text style={[STYLESHEET.baseText, styles.rowText]}>
          {userDetails.email}
        </Text>
      </View>
      <View
        style={[
          STYLESHEET.rowCenterFlexStart,
          styles.row,
          { width: containerWidth - 20 },
        ]}
      >
        <Text style={[STYLESHEET.baseText, styles.headerText]}>
          {i18n.t(TranslationsKeys.FRIENDS_DETAIL_SCREEN__FRIENDS_HEADER)}
        </Text>
      </View>
      <FlatList
        contentContainerStyle={[
          STYLESHEET.baseFlatList,
          { width: containerWidth - 20 },
        ]}
        data={userDetails.friends}
        keyExtractor={item => item.id}
        ListEmptyComponent={FlatListEmptyComponent}
        renderItem={FlatListRenderItemComponent}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  friendsFlatListEmpty: {
    color: COLORS.WHITE,
    fontSize: CONSTS.LARGE_FONT_SIZE,
  },
  friendsFlatListItem: {
    backgroundColor: COLORS.MEDIUMLIGHTBLUE,
    borderBottomColor: COLORS.MEDIUMBLUE,
    borderBottomWidth: 1,
    color: COLORS.WHITE,
    fontSize: CONSTS.FONT_SIZE,
    marginBottom: 5,
    paddingHorizontal: 15,
    paddingVertical: 5,
    textAlignVertical: 'center',
    ...withShadow(),
  },
  headerText: {
    paddingTop: 5,
  },
  row: {
    marginHorizontal: 10,
    marginVertical: 5,
  },
  rowText: {
    opacity: 0.6,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
});

export default FriendsDetailScreen;
