import { useNavigation } from '@react-navigation/native';
import i18n from 'i18n-js';
import React, { useRef, useState } from 'react';
import {
  FlatList,
  ListRenderItem,
  ListRenderItemInfo,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDispatch, useSelector } from 'react-redux';

import { TranslationsKeys } from '../../translations/keys';
import TouchableOpacity from '../animations/TouchableOpacity';
import { GET_USER_BY_NAME } from '../api/routes';
import server from '../api/server';
import ReanimatedDrawerLayout from '../components/ReanimatedDrawerLayout';
import Header from '../components/Header';
import useCurrentUser from '../hooks/useCurrentUser';
import useDimensions from '../hooks/useDimensions';
import { SearchFriendsScreenNavigationProp } from '../navigation/types';
import { Dispatch, IFriend, RootState, TokenState, UserState } from '../store';
import { COLORS, CONSTS, STYLESHEET, withShadow } from '../styles';

export const FlatListEmptyComponent = () => {
  return (
    <View style={STYLESHEET.baseContainer}>
      <Text style={styles.friendsFlatListEmptyText}>
        {i18n.t(TranslationsKeys.SEARCH_FRIENDS_SCREEN__INSTRUCTION)}
      </Text>
    </View>
  );
};

const SearchFriendsScreen = () => {
  const navigation = useNavigation<SearchFriendsScreenNavigationProp>();
  useSelector<RootState, string>(state => state.locale);
  const tokenState = useSelector<RootState, TokenState>(state => state.token);
  const userState = useSelector<RootState, UserState>(state => state.user);
  const dispatch = useDispatch<Dispatch>();
  const [getCurrentUser] = useCurrentUser(tokenState);
  const { containerWidth } = useDimensions();
  const drawerLayoutRef = useRef<ReanimatedDrawerLayout>(null);
  const [refresh, setRefresh] = useState(false); // Any time it changes value, FlatList re-renders itself;
  const [searchValue, setSearchValue] = useState('');
  const [data, setData] = useState<IFriend[]>([]);
  if (!userState) {
    return null;
  }
  const onSearch = async () => {
    const response = await server.get<{ users: IFriend[] }>(
      `${GET_USER_BY_NAME}`,
      {
        headers: {
          authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
        },
        params: {
          name: searchValue,
        },
      }
    );
    setData(response.data.users.filter(user => user.id !== userState?.id));
  };
  const renderItem: ListRenderItem<IFriend> = ({
    item,
  }: ListRenderItemInfo<IFriend>) => {
    const isFriend = userState?.friends.find(friend => friend.id === item.id);
    const addOrRemoveFriend = async () => {
      try {
        if (isFriend) {
          dispatch.user.removeFriend(
            item,
            'Bearer ' + tokenState.tokenType + tokenState.token
          );
        } else {
          dispatch.user.addFriend(
            item,
            'Bearer ' + tokenState.tokenType + tokenState.token
          );
        }
        dispatch.user.saveUser(await getCurrentUser());
        setRefresh(prev => !prev);
      } catch (err) {
        console.log(err.message);
      }
    };
    return (
      <TouchableOpacity
        delayLongPress={2000}
        key={item.id}
        onLongPress={addOrRemoveFriend}
        onPress={() => navigation.navigate('FriendsDetail', { id: item.id })}
        style={styles.friendsFlatListItem}
      >
        <Text style={STYLESHEET.baseText}>{item.name}</Text>
        {!!isFriend && <Icon color={COLORS.HEART} name="heart" size={16} />}
      </TouchableOpacity>
    );
  };
  return (
    <ReanimatedDrawerLayout ref={drawerLayoutRef}>
      <View style={[STYLESHEET.baseContainer, STYLESHEET.screenContainer]}>
        <Header
          onDrawerButtonPress={drawerLayoutRef.current?.openDrawer}
          title={i18n.t(
            TranslationsKeys.SEARCH_FRIENDS_SCREEN__SEARCH_FRIENDS_HEADER
          )}
        />
        <View
          style={[styles.searchInputContainer, { width: containerWidth - 20 }]}
        >
          <TextInput
            onChangeText={setSearchValue}
            placeholder={i18n.t(
              TranslationsKeys.SEARCH_FRIENDS_SCREEN__NAME_INPUT_PLACEHOLDER
            )}
            placeholderTextColor={COLORS.WHITE}
            style={[STYLESHEET.baseInput, styles.searchInput]}
            value={searchValue}
          />
        </View>
        <TouchableOpacity
          disabled={!searchValue}
          onPress={onSearch}
          style={[
            STYLESHEET.baseButton,
            styles.searchButton,
            { width: containerWidth - 20 },
          ]}
        >
          <Icon
            color={COLORS.WHITE}
            name="magnify"
            size={24}
            style={[!searchValue && { opacity: 0.5 }]}
          />
          <Text
            style={[
              STYLESHEET.baseText,
              styles.searchButtonText,
              !searchValue && { opacity: 0.5 },
            ]}
          >
            {i18n.t(TranslationsKeys.SEARCH_FRIENDS_SCREEN__SEARCH_BUTTON)}
          </Text>
        </TouchableOpacity>
        <FlatList
          contentContainerStyle={[
            STYLESHEET.baseFlatList,
            { width: containerWidth - 20 },
          ]}
          data={data}
          extraData={refresh}
          keyExtractor={item => item.id}
          ListEmptyComponent={FlatListEmptyComponent}
          renderItem={renderItem}
        />
      </View>
    </ReanimatedDrawerLayout>
  );
};

const styles = StyleSheet.create({
  friendsFlatListEmptyText: {
    color: COLORS.WHITE,
    fontSize: CONSTS.LARGE_FONT_SIZE,
    margin: 5,
    padding: 5,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  friendsFlatListItem: {
    alignItems: 'center',
    backgroundColor: COLORS.MEDIUMLIGHTBLUE,
    borderRadius: CONSTS.TILE_BORDERRADIUS,
    flexDirection: 'row',
    height: 40,
    justifyContent: 'space-around',
    margin: 5,
    padding: 5,
    ...withShadow(),
  },
  searchButton: {
    borderRadius: CONSTS.TILE_BORDERRADIUS,
  },
  searchButtonText: {
    marginLeft: 10,
  },
  searchInput: {
    marginVertical: 10,
  },
  searchInputContainer: {
    backgroundColor: COLORS.MEDIUMBLUE,
    borderRadius: CONSTS.TILE_BORDERRADIUS,
    marginVertical: 5,
    ...withShadow(),
  },
});

export default SearchFriendsScreen;
