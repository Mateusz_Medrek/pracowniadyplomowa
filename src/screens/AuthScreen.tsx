import { GoogleSignin } from '@react-native-community/google-signin';
import { useNavigation } from '@react-navigation/native';
import i18n from 'i18n-js';
import React, { useReducer } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TextInput,
  ScrollView,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDispatch, useSelector } from 'react-redux';

import { TranslationsKeys } from '../../translations/keys';
import TouchableOpacity from '../animations/TouchableOpacity';
import { SIGN_IN, SIGN_UP_GOOGLE } from '../api/routes';
import server from '../api/server';
import ChangeLocale from '../components/ChangeLocale';
import useDimensions from '../hooks/useDimensions';
import authReducer, { INITIAL_STATE } from '../localReducers/authReducer';
import { AuthScreenNavigationProp } from '../navigation/types';
import { Dispatch, RootState, TokenProviders } from '../store';
import { COLORS, STYLESHEET } from '../styles';

const regex = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);

const AuthScreen = () => {
  const navigation = useNavigation<AuthScreenNavigationProp>();
  const { containerWidth } = useDimensions();
  useSelector<RootState, string>(state => state.locale);
  const dispatch = useDispatch<Dispatch>();
  const [state, localDispatch] = useReducer(authReducer, INITIAL_STATE);
  const onFormSubmitButtonPress = async () => {
    if (!state.email) {
      return localDispatch({
        type: 'SET_EMAIL_ERROR',
        payload: TranslationsKeys.AUTH_SCREEN__EMAIL_NOT_PROVIDED,
      });
    }
    if (!state.email.match(regex)) {
      return localDispatch({
        type: 'SET_EMAIL_ERROR',
        payload: TranslationsKeys.AUTH_SCREEN__EMAIL_HAS_INVALID_FORMAT,
      });
    }
    if (!state.password) {
      return localDispatch({
        type: 'SET_PASSWORD_ERROR',
        payload: TranslationsKeys.AUTH_SCREEN__PASSWORD_NOT_PROVIDED,
      });
    }
    localDispatch({ type: 'SHOW_FORM_INDICATOR' });
    try {
      const response = await server.post<string>(`/${SIGN_IN}`, {
        email: state.email,
        password: state.password,
      });
      dispatch.token.addToken({
        tokenValue: response.data,
        tokenType: TokenProviders.server,
      });
      localDispatch({ type: 'FORM_AUTH_COMPLETED' });
    } catch (err) {
      if (err.response.data.error === 'Invalid email') {
        return localDispatch({
          type: 'SET_EMAIL_ERROR',
          payload: TranslationsKeys.AUTH_SCREEN__SERVER_INVALID_EMAIL,
        });
      }
      if (err.response.data.error === 'Invalid password') {
        return localDispatch({
          type: 'SET_PASSWORD_ERROR',
          payload: TranslationsKeys.AUTH_SCREEN__SERVER_INVALID_PASSWORD,
        });
      }
      return localDispatch({ type: 'FORM_AUTH_FAILED', payload: err.message });
    }
  };
  const onGoogleButtonPress = async () => {
    try {
      localDispatch({ type: 'SHOW_GOOGLE_INDICATOR' });
      const user = await GoogleSignin.signIn();
      if (!user || !user.idToken) {
        throw new Error(
          i18n.t(TranslationsKeys.AUTH_SCREEN__NOT_AUTHENTICATED)
        );
      }
      const response = await server.post<string>(
        `/${SIGN_UP_GOOGLE}`,
        {},
        {
          headers: {
            authorization: 'Bearer ' + user.idToken,
          },
        }
      );
      if (response.data !== user.idToken) {
        throw new Error(
          i18n.t(TranslationsKeys.AUTH_SCREEN__NOT_AUTHENTICATED)
        );
      }
      dispatch.token.addToken({
        tokenValue: user.idToken,
        tokenType: TokenProviders.google,
      });
      localDispatch({ type: 'GOOGLE_AUTH_COMPLETED' });
    } catch (err) {
      localDispatch({ type: 'GOOGLE_AUTH_FAILED', payload: err.message });
    }
  };
  const navigateToRegistration = () => {
    localDispatch({ type: 'RESET' });
    navigation.navigate('Registration');
  };
  return (
    <View style={STYLESHEET.screenContainer}>
      <ScrollView nestedScrollEnabled={true}>
        <View style={[STYLESHEET.baseContainer, styles.container]}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            onChangeText={payload =>
              localDispatch({ type: 'SET_EMAIL', payload })
            }
            placeholder={i18n.t(
              TranslationsKeys.AUTH_SCREEN__EMAIL_PLACEHOLDER
            )}
            placeholderTextColor={COLORS.WHITE}
            style={[STYLESHEET.baseInput, { width: containerWidth - 20 }]}
            value={state.email}
          />
          {!!state.emailError && (
            <Text style={STYLESHEET.errorText}>{state.emailError}</Text>
          )}
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            onChangeText={payload =>
              localDispatch({ type: 'SET_PASSWORD', payload })
            }
            placeholder={i18n.t(
              TranslationsKeys.AUTH_SCREEN__PASSWORD_PLACEHOLDER
            )}
            placeholderTextColor={COLORS.WHITE}
            secureTextEntry={true}
            style={[STYLESHEET.baseInput, { width: containerWidth - 20 }]}
            value={state.password}
          />
          {!!state.passwordError && (
            <Text style={STYLESHEET.errorText}>{state.passwordError}</Text>
          )}
          <TouchableOpacity
            onPress={onFormSubmitButtonPress}
            style={[
              STYLESHEET.baseButton,
              STYLESHEET.mediumBlueButton,
              styles.button,
              { width: containerWidth - 20 },
            ]}
          >
            {!state.shouldShowFormIndicator ? (
              <Text style={STYLESHEET.baseText}>
                {i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_IN)}
              </Text>
            ) : (
              <ActivityIndicator color={COLORS.WHITE} />
            )}
          </TouchableOpacity>
          <Text style={[STYLESHEET.baseText, styles.orText]}>
            {i18n.t(TranslationsKeys.AUTH_SCREEN__OR)}
          </Text>
          <TouchableOpacity
            onPress={navigateToRegistration}
            style={[
              STYLESHEET.baseButton,
              STYLESHEET.mediumBlueButton,
              styles.button,
              { width: containerWidth - 20 },
            ]}
          >
            <Text style={STYLESHEET.baseText}>
              {i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_UP)}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={onGoogleButtonPress}
            style={[
              STYLESHEET.baseButton,
              styles.button,
              styles.fbGoogleButton,
              { width: containerWidth - 20 },
            ]}
          >
            {!state.shouldShowGoogleIndicator ? (
              <View style={STYLESHEET.rowCenter}>
                <Text
                  accessibilityLabel={i18n.t(
                    TranslationsKeys.AUTH_SCREEN__SIGN_IN_GOOGLE_A11Y
                  )}
                  style={[STYLESHEET.baseText, styles.googleButtonText]}
                >
                  {i18n.t(TranslationsKeys.AUTH_SCREEN__SIGN_IN_GOOGLE)}
                </Text>
                <View style={styles.icon}>
                  <Icon name="google" size={36} />
                </View>
              </View>
            ) : (
              <ActivityIndicator color={COLORS.RED} />
            )}
          </TouchableOpacity>
        </View>
      </ScrollView>
      <ChangeLocale />
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    height: 36,
  },
  container: {
    marginTop: 150,
  },
  fbGoogleButton: {
    backgroundColor: COLORS.WHITE,
  },
  fbButtonText: {
    color: COLORS.LIGHTBLUE,
  },
  googleButtonText: {
    color: COLORS.RED,
  },
  icon: {
    marginLeft: 10,
  },
  orText: {
    padding: 10,
  },
});

export default AuthScreen;
