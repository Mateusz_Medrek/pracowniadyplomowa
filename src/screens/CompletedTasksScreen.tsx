import { useNavigation } from '@react-navigation/native';
import i18n from 'i18n-js';
import React, { useCallback, useEffect, useState } from 'react';
import {
  FlatList,
  ListRenderItem,
  ListRenderItemInfo,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useSelector } from 'react-redux';

import { TranslationsKeys } from '../../translations/keys';
import TouchableOpacity from '../animations/TouchableOpacity';
import { GET_ALL_COMPLETED_TASKS, UNCHECK_TASK } from '../api/routes';
import server from '../api/server';
import useDimensions from '../hooks/useDimensions';
import { CompletedTasksScreenNavigationProp } from '../navigation/types';
import { ITaskTitle, RootState, TokenState } from '../store';
import { COLORS, CONSTS, STYLESHEET, withShadow } from '../styles';

export const FlatListEmptyComponent = () => {
  return (
    <View style={STYLESHEET.baseContainer}>
      <Text style={styles.tasksFlatListEmptyText}>
        {i18n.t(TranslationsKeys.COMPLETED_TASKS_SCREEN__NO_TASKS)}
      </Text>
    </View>
  );
};

export const FlatListHeaderComponent = () => {
  return (
    <Text style={styles.tasksFlatListHeader}>
      {i18n.t(TranslationsKeys.COMPLETED_TASKS_SCREEN__COMPLETED_TASKS)}
    </Text>
  );
};

const CompletedTasksScreen = () => {
  const navigation = useNavigation<CompletedTasksScreenNavigationProp>();
  const { containerWidth } = useDimensions();
  const tokenState = useSelector<RootState, TokenState>(state => state.token);
  const [completedTasks, setCompletedTasks] = useState<ITaskTitle[] | null>(
    null
  );
  const getAllCompletedTasks = async () => {
    try {
      const tasksResponse = await server.get<{ tasks: ITaskTitle[] }>(
        `/${GET_ALL_COMPLETED_TASKS}`,
        {
          headers: {
            authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
          },
        }
      );
      setCompletedTasks(tasksResponse.data.tasks);
    } catch (err) {
      console.log(err.message);
    }
  };
  const navigateBack = () => {
    navigation.goBack();
  };
  const uncheckTask = async (id: string) => {
    try {
      await server.post(
        `/${UNCHECK_TASK}`,
        { id },
        {
          headers: {
            authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
          },
        }
      );
      setCompletedTasks(
        prevCompletedTasks =>
          prevCompletedTasks &&
          prevCompletedTasks.filter(prevTask => prevTask.id !== id)
      );
    } catch (err) {
      console.log(err.message);
    }
  };
  const renderItem: ListRenderItem<ITaskTitle> = useCallback(
    ({ item }: ListRenderItemInfo<ITaskTitle>) => {
      return (
        <View
          key={item.id}
          style={[STYLESHEET.rowCenterSpaceBetween, styles.tasksFlatListItem]}
        >
          <Text style={[STYLESHEET.baseText, styles.tasksFlatListItemText]}>
            {item.title}
          </Text>
          <View style={STYLESHEET.rowCenterSpaceBetween}>
            <TouchableOpacity
              onPress={() => uncheckTask(item.id)}
              style={[STYLESHEET.baseButton, styles.uncompletedButton]}
            >
              <View style={styles.restoreIcon}>
                <Icon color={COLORS.BLACK} name="restore" size={30} />
              </View>
              <View>
                <Text style={STYLESHEET.baseText}>
                  {i18n.t(
                    TranslationsKeys.COMPLETED_TASKS_SCREEN__BRING_TASK__BRING
                  )}
                </Text>
                <Text style={STYLESHEET.baseText}>
                  {i18n.t(
                    TranslationsKeys.COMPLETED_TASKS_SCREEN__BRING_TASK__TASK
                  )}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      );
    },
    [] // eslint-disable-line react-hooks/exhaustive-deps
  );
  useEffect(() => {
    getAllCompletedTasks();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  if (!completedTasks) {
    return null;
  }
  return (
    <View style={STYLESHEET.screenContainer}>
      <FlatList
        contentContainerStyle={[
          STYLESHEET.baseFlatList,
          { width: containerWidth - 20 },
        ]}
        data={completedTasks}
        keyExtractor={item => item.id}
        ListEmptyComponent={FlatListEmptyComponent}
        ListHeaderComponent={FlatListHeaderComponent}
        renderItem={renderItem}
      />
      <TouchableOpacity
        onPress={navigateBack}
        style={[
          STYLESHEET.baseButton,
          styles.returnButton,
          { width: containerWidth - 20 },
        ]}
      >
        <Text style={STYLESHEET.baseText}>
          {i18n.t(TranslationsKeys.COMPLETED_TASKS_SCREEN__RETURN)}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  restoreIcon: {
    marginRight: 10,
  },
  returnButton: {
    backgroundColor: COLORS.MEDIUMLIGHTBLUE,
    borderRadius: CONSTS.TILE_BORDERRADIUS,
  },
  tasksFlatListEmptyText: {
    color: COLORS.WHITE,
    fontSize: CONSTS.LARGE_FONT_SIZE,
  },
  tasksFlatListHeader: {
    borderBottomColor: COLORS.MEDIUMDARKBLUE,
    borderBottomWidth: 1,
    color: COLORS.WHITE,
    fontSize: CONSTS.FONT_SIZE,
    paddingBottom: 5,
    paddingLeft: 15,
    marginBottom: 1,
  },
  tasksFlatListItem: {
    backgroundColor: COLORS.MEDIUMLIGHTBLUE,
    borderBottomColor: COLORS.MEDIUMBLUE,
    borderBottomWidth: 1,
    marginHorizontal: 10,
    marginVertical: 5,
    ...withShadow(),
  },
  tasksFlatListItemText: {
    flex: 1,
    textAlign: 'center',
  },
  uncompletedButton: {
    backgroundColor: COLORS.MEDIUMLIGHTBLUE,
    borderRadius: CONSTS.TILE_BORDERRADIUS,
    justifyContent: 'space-between',
  },
});

export default CompletedTasksScreen;
