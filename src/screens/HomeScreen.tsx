import { useIsFocused, useNavigation } from '@react-navigation/native';
import i18n from 'i18n-js';
import React, { useEffect, useRef, useState } from 'react';
import {
  FlatList,
  ListRenderItem,
  ListRenderItemInfo,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDispatch, useSelector } from 'react-redux';

import { TranslationsKeys } from '../../translations/keys';
import TouchableOpacity from '../animations/TouchableOpacity';
import {
  CHECK_TASK,
  DELETE_TASK,
  GET_ALL_TASKS,
  GET_ALL_TODAY_TASKS,
  GET_TASK,
} from '../api/routes';
import server from '../api/server';
import ReanimatedDrawerLayout from '../components/ReanimatedDrawerLayout';
import Header from '../components/Header';
import ReanimatedSwipeable from '../components/ReanimatedSwipeable';
import useCurrentUser from '../hooks/useCurrentUser';
import useDimensions from '../hooks/useDimensions';
import { HomeScreenNavigationProp } from '../navigation/types';
import {
  Dispatch,
  ITask,
  ITaskTitle,
  RootState,
  TokenState,
  UserState,
} from '../store';
import { COLORS, CONSTS, STYLESHEET, withShadow } from '../styles';

export const FlatListEmptyComponent = () => {
  const userState = useSelector<RootState, UserState>(state => state.user);
  if (!userState || userState.tasksIds.length) {
    return null;
  }
  return (
    <Text style={styles.tasksFlatListEmpty}>
      {i18n.t(TranslationsKeys.HOME_SCREEN__NO_TASKS)}
    </Text>
  );
};

export const FlatListHeaderComponent = () => {
  return (
    <Text style={styles.tasksFlatListHeader}>
      {i18n.t(TranslationsKeys.HOME_SCREEN__YOUR_TASKS)}
    </Text>
  );
};

const HomeScreen = () => {
  const focused = useIsFocused();
  const navigation = useNavigation<HomeScreenNavigationProp>();
  useSelector<RootState, string>(state => state.locale);
  const tokenState = useSelector<RootState, TokenState>(state => state.token);
  const userState = useSelector<RootState, UserState>(state => state.user);
  const dispatch = useDispatch<Dispatch>();
  const [getCurrentUser] = useCurrentUser(tokenState);
  const { containerWidth } = useDimensions();
  const drawerLayoutRef = useRef<ReanimatedDrawerLayout>(null);
  const [showAllTasks, setShowAllTasks] = useState(true);
  const [tasksList, setTasksList] = useState<ITaskTitle[]>([]);
  const [todayTasksList, setTodayTasksList] = useState<ITaskTitle[]>([]);
  const getAllTasks = async () => {
    try {
      const response = await server.get<{ tasks: ITaskTitle[] }>(
        `/${GET_ALL_TASKS}`,
        {
          headers: {
            authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
          },
        }
      );
      setTasksList(response.data.tasks);
    } catch (err) {
      console.log(err.message);
    }
  };
  const getAllTodayTasks = async () => {
    try {
      const response = await server.get<{ tasks: ITaskTitle[] }>(
        `/${GET_ALL_TODAY_TASKS}`,
        {
          headers: {
            authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
          },
        }
      );
      setTodayTasksList(response.data.tasks);
    } catch (err) {
      console.log(err.message);
    }
  };
  const getUserAndTasks = async () => {
    try {
      await getAllTasks();
      dispatch.user.saveUser(await getCurrentUser());
    } catch (err) {
      console.log(err.message);
    }
  };
  const onSwitchTasksButtonPress = async () => {
    if (!showAllTasks) {
      await getAllTasks();
    } else {
      await getAllTodayTasks();
    }
    setShowAllTasks(prev => !prev);
  };
  const onTaskCheck = async (id: string) => {
    try {
      await server.post(
        `/${CHECK_TASK}`,
        { id },
        {
          headers: {
            authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
          },
        }
      );
      setTodayTasksList(prevTodayTaskList =>
        prevTodayTaskList.filter(prevTask => prevTask.id !== id)
      );
      setTasksList(prevTaskList =>
        prevTaskList.filter(prevTask => prevTask.id !== id)
      );
    } catch (err) {
      console.log(err.message);
    }
  };
  const onTaskDelete = async (id: string) => {
    try {
      await server.post(
        `/${DELETE_TASK}`,
        { id },
        {
          headers: {
            authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
          },
        }
      );
      setTodayTasksList(prevTodayTaskList =>
        prevTodayTaskList.filter(prevTask => prevTask.id !== id)
      );
      setTasksList(prevTaskList =>
        prevTaskList.filter(prevTask => prevTask.id !== id)
      );
    } catch (err) {
      console.log(err.message);
    }
  };
  const onTaskEdit = async (id: string) => {
    try {
      const response = await server.get<ITask>(`/${GET_TASK}/${id}`, {
        headers: {
          authorization: 'Bearer ' + tokenState.tokenType + tokenState.token,
        },
      });
      navigation.navigate('EditTask', {
        taskData: {
          id,
          ...response.data,
        },
      });
    } catch (err) {
      console.log(err.message);
    }
  };
  const renderItem: ListRenderItem<ITaskTitle> = ({
    item,
  }: ListRenderItemInfo<ITaskTitle>) => {
    const renderLeftActions = () => {
      return (
        <View
          style={[
            STYLESHEET.rowCenterSpaceBetween,
            styles.tasksFlatListItemContainer,
          ]}
        >
          <TouchableOpacity
            onPress={() => onTaskCheck(item.id)}
            style={styles.icon}
          >
            <Icon color={COLORS.GREEN} name="check-circle" size={30} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => onTaskEdit(item.id)}
            style={styles.icon}
          >
            <Icon color={COLORS.BLACK} name="circle-edit-outline" size={30} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => onTaskDelete(item.id)}
            style={styles.icon}
          >
            <Icon color={COLORS.GRAY} name="trash-can" size={30} />
          </TouchableOpacity>
        </View>
      );
    };
    return (
      <ReanimatedSwipeable
        renderLeftActions={renderLeftActions}
        renderRightActions={renderLeftActions}
      >
        <RectButton
          key={item.id}
          onPress={() => {
            navigation.navigate('TaskDetails', { id: item.id });
          }}
          style={[
            STYLESHEET.rowCenterSpaceBetween,
            styles.tasksFlatListItemContainer,
          ]}
        >
          <Text style={styles.tasksFlatListItem}>{item.title}</Text>
        </RectButton>
      </ReanimatedSwipeable>
    );
  };
  useEffect(() => {
    getUserAndTasks();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    userState && getAllTasks();
  }, [userState]); // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    if (!focused) {
      return;
    }
    if (showAllTasks) {
      getAllTasks();
    } else {
      getAllTodayTasks();
    }
  }, [focused]); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <ReanimatedDrawerLayout ref={drawerLayoutRef}>
      <View style={STYLESHEET.screenContainer}>
        <Header
          onDrawerButtonPress={drawerLayoutRef.current?.openDrawer}
          title={i18n.t(TranslationsKeys.HOME_SCREEN__HOME_HEADER)}
        />
        <TouchableOpacity
          onPress={onSwitchTasksButtonPress}
          style={[
            STYLESHEET.baseButton,
            STYLESHEET.mediumBlueButton,
            styles.showTasksButton,
            { width: containerWidth - 20 },
          ]}
        >
          <Text style={STYLESHEET.baseText}>
            {i18n.t(
              showAllTasks
                ? TranslationsKeys.HOME_SCREEN__SHOW_TODAY_TASKS
                : TranslationsKeys.HOME_SCREEN__SHOW_ALL_TASKS
            )}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('CompletedTasks')}
          style={[
            STYLESHEET.baseButton,
            STYLESHEET.mediumBlueButton,
            styles.showTasksButton,
            { width: containerWidth - 20 },
          ]}
        >
          <Text style={STYLESHEET.baseText}>
            {i18n.t(TranslationsKeys.HOME_SCREEN__SHOW_COMPLETED_TASKS)}
          </Text>
        </TouchableOpacity>
        <FlatList
          contentContainerStyle={[
            STYLESHEET.baseFlatList,
            { width: containerWidth - 20 },
          ]}
          data={showAllTasks ? tasksList : todayTasksList}
          keyExtractor={item => item.id}
          ListEmptyComponent={FlatListEmptyComponent}
          ListHeaderComponent={FlatListHeaderComponent}
          renderItem={renderItem}
        />
      </View>
    </ReanimatedDrawerLayout>
  );
};

const styles = StyleSheet.create({
  icon: {
    padding: 5,
  },
  showTasksButton: {
    borderRadius: CONSTS.TILE_BORDERRADIUS,
  },
  tasksFlatListEmpty: {
    color: COLORS.WHITE,
    fontSize: CONSTS.LARGE_FONT_SIZE,
  },
  tasksFlatListHeader: {
    borderBottomColor: COLORS.MEDIUMDARKBLUE,
    borderBottomWidth: 1,
    color: COLORS.WHITE,
    fontSize: CONSTS.FONT_SIZE,
    paddingBottom: 5,
    paddingLeft: 15,
    marginBottom: 1,
  },
  tasksFlatListItem: {
    backgroundColor: COLORS.MEDIUMLIGHTBLUE,
    borderBottomColor: COLORS.MEDIUMBLUE,
    borderBottomWidth: 1,
    color: COLORS.WHITE,
    flex: 1,
    fontSize: CONSTS.FONT_SIZE,
    paddingHorizontal: 15,
    paddingVertical: 5,
    textAlignVertical: 'center',
    ...withShadow(),
  },
  tasksFlatListItemContainer: {
    borderRadius: CONSTS.TILE_BORDERRADIUS,
    height: 40,
  },
});

export default HomeScreen;
