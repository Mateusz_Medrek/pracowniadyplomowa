import i18n from 'i18n-js';
import React, { useReducer } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { TranslationsKeys } from '../../translations/keys';
import TouchableOpacity from '../animations/TouchableOpacity';
import { SIGN_UP_EMAIL } from '../api/routes';
import server from '../api/server';
import ChangeLocale from '../components/ChangeLocale';
import useDimensions from '../hooks/useDimensions';
import registrationReducer, {
  INITIAL_STATE,
} from '../localReducers/registrationReducer';
import { Dispatch, RootState, TokenProviders } from '../store';
import { COLORS, STYLESHEET } from '../styles';

const regex = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);

const RegistrationScreen = () => {
  const { containerWidth } = useDimensions();
  useSelector<RootState, string>(state => state.locale);
  const dispatch = useDispatch<Dispatch>();
  const [state, localDispatch] = useReducer(registrationReducer, INITIAL_STATE);
  const onFormSubmit = async () => {
    if (!state.email) {
      return localDispatch({
        type: 'SET_EMAIL_ERROR',
        payload: TranslationsKeys.REGISTRATION_SCREEN__EMAIL_NOT_PROVIDED,
      });
    }
    if (!state.email.match(regex)) {
      return localDispatch({
        type: 'SET_EMAIL_ERROR',
        payload: TranslationsKeys.REGISTRATION_SCREEN__EMAIL_HAS_INVALID_FORMAT,
      });
    }
    if (!state.name) {
      return localDispatch({
        type: 'SET_NAME_ERROR',
        payload: TranslationsKeys.REGISTRATION_SCREEN__NAME_NOT_PROVIDED,
      });
    }
    if (!state.password) {
      return localDispatch({
        type: 'SET_PASSWORD_ERROR',
        payload: TranslationsKeys.REGISTRATION_SCREEN__PASSWORD_NOT_PROVIDED,
      });
    }
    localDispatch({ type: 'SHOW_INDICATOR' });
    try {
      const response = await server.post<string>(`/${SIGN_UP_EMAIL}`, {
        email: state.email,
        name: state.name,
        password: state.password,
      });
      dispatch.token.addToken({
        tokenValue: response.data,
        tokenType: TokenProviders.server,
      });
      localDispatch({ type: 'REGISTRATION_COMPLETED' });
    } catch (err) {
      if (
        err.response.data.error === 'There is already account with this email'
      ) {
        return localDispatch({
          type: 'SET_EMAIL_ERROR',
          payload: TranslationsKeys.REGISTRATION_SCREEN__SERVER_EMAIL_ERROR,
        });
      }
      return localDispatch({
        type: 'REGISTRATION_FAILED',
        payload: err.message,
      });
    }
  };
  return (
    <View style={STYLESHEET.screenContainer}>
      <View style={STYLESHEET.baseContainer}>
        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          onChangeText={payload =>
            localDispatch({ type: 'SET_EMAIL', payload })
          }
          placeholder={i18n.t(
            TranslationsKeys.REGISTRATION_SCREEN__EMAIL_INPUT_PLACEHOLDER
          )}
          placeholderTextColor={COLORS.WHITE}
          style={[STYLESHEET.baseInput, { width: containerWidth - 20 }]}
          value={state.email}
        />
        {!!state.emailError && (
          <Text style={STYLESHEET.errorText}>{state.emailError}</Text>
        )}
        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          onChangeText={payload => localDispatch({ type: 'SET_NAME', payload })}
          placeholder={i18n.t(
            TranslationsKeys.REGISTRATION_SCREEN__NAME_INPUT_PLACEHOLDER
          )}
          placeholderTextColor={COLORS.WHITE}
          style={[STYLESHEET.baseInput, { width: containerWidth - 20 }]}
          value={state.name}
        />
        {!!state.nameError && (
          <Text style={STYLESHEET.errorText}>{state.nameError}</Text>
        )}
        <TextInput
          autoCapitalize="none"
          autoCorrect={false}
          onChangeText={payload =>
            localDispatch({ type: 'SET_PASSWORD', payload })
          }
          placeholder={i18n.t(
            TranslationsKeys.REGISTRATION_SCREEN__PASSWORD_INPUT_PLACEHOLDER
          )}
          placeholderTextColor={COLORS.WHITE}
          secureTextEntry={true}
          style={[STYLESHEET.baseInput, { width: containerWidth - 20 }]}
          value={state.password}
        />
        {!!state.passwordError && (
          <Text style={STYLESHEET.errorText}>{state.passwordError}</Text>
        )}
        <TouchableOpacity
          onPress={onFormSubmit}
          style={[
            STYLESHEET.baseButton,
            STYLESHEET.mediumBlueButton,
            styles.loginRegistrationButton,
            { width: containerWidth - 20 },
          ]}
        >
          {!state.shouldShowIndicator ? (
            <Text style={STYLESHEET.baseText}>
              {i18n.t(TranslationsKeys.REGISTRATION_SCREEN__CREATE_ACCOUNT)}
            </Text>
          ) : (
            <ActivityIndicator color={COLORS.WHITE} />
          )}
        </TouchableOpacity>
      </View>
      <ChangeLocale />
    </View>
  );
};

const styles = StyleSheet.create({
  loginRegistrationButton: {
    height: 36,
  },
});

export default RegistrationScreen;
