module.exports = {
  preset: 'react-native',
  transform: {
    '^.+\\.js$': '<rootDir>/node_modules/react-native/jest/preprocessor.js',
    '\\.(ts|tsx)$': 'ts-jest',
  },
  transformIgnorePatterns: [
    'node_modules/(?!((jest-)?react-native|react-native-reanimated|react-navigation|react-native-screens/native-stack/*|@react-navigation/.*|@react-native-community/datetimepicker|react-native-draggable-flatlist))',
  ],
  globals: {
    'ts-jest': {
      babelConfig: true,
      tsConfig: 'tsconfig.jest.json',
    },
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  setupFilesAfterEnv: [
    '<rootDir>/tests/setup.js',
    './node_modules/react-native-gesture-handler/jestSetup.js',
  ],
  collectCoverageFrom: [
    'src/**/*.{js,jsx,ts,tsx}',
    '!src/animations/*',
    '!src/hooks/*',
    '!src/styles/*',
  ],
};
